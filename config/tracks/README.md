
# WARNING File handling disclaimer

The GleisGUI has a very disturbing way of handling its tracks files. It just does not work with an archive, as files are renamed around all the time.

It silently renames or removes files after exiting the editor. Whatever you plan to do, have a backup in place and watch addition/removal from git.

Also, a "recent" file-pair has to exist for the "playmode" to work. If any file of the pair is missing, open the desired track in the editor first, then the png file will be created and the xml file is renamed to recent.xml.
