#ifndef INFOPANEL_H
#define INFOPANEL_H

#include <QGraphicsObject>
#include <QPainter>
#include <QTimerEvent>

class InfoPanel : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit InfoPanel(QRectF size, QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);
protected:
    void timerEvent(QTimerEvent *event);

signals:
    void showMsg();
    void hideMsg();

public slots:
    void printMsg(const QString& msg);

private:
    QRectF m_bounds;
    QGraphicsTextItem* m_msg;
    int m_timerID;
};

#endif // INFOPANEL_H
