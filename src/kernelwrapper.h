#ifndef KERNELWRAPPER_H
#define KERNELWRAPPER_H

/**** Eine Klasse zur Kapselung der Rail-Kernel Funktionalität. Stellt Pub. Funktionen für die wichtigsten Einstellungen zur Verfügung.
Mit Set_switch wird ein MonoFlop gesetzt. ****/

#include <QObject>
#include <QMap>

#include "rail_kernel/media.h"
#include "rail_kernel/ipc.h"
#include "rail_kernel/rail-tools.h"

class KernelWrapper : public QObject
{
    Q_OBJECT
public:
    KernelWrapper();

public slots:
    void setSwitch(QString board, int port);
    void changeTime(QString board, int port, int new_time);
    void setMultiSwitch(QString board, QList<int> ports);
    void initBoard(QString board, QString media, QString interface);
    void stopBoard(QString board);
    bool probeBoard(QString board);
    QMultiMap<QString, char *> getInterfaceList();

signals:
    void getReply();
    void sendMsg(QString);

private:
    int m_retry;

};

#endif // KERNELWRAPPER_H
