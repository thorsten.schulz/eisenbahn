#include "trackplan.h"
#include <QGraphicsObject>
#include <QGraphicsItem>

TrackPlan::TrackPlan( QGraphicsItem *parent )
: QGraphicsObject( parent ), m_mode( BuildMode ),
	m_protlayer( 0 ), m_selected( 0 ), m_activenode( 0 )
{
    setFlag( QGraphicsItem::ItemHasNoContents );
    setAcceptHoverEvents( false );
    m_geoplan = new GeoPlan();
    m_boardcontrol = 0;
}

#include "railway.h"		// AktiveTrack
#include <QAbstractAnimation>
#include <QPropertyAnimation>
#include <QDebug>

// ein Gleisstück hinzufügen
void TrackPlan::addTrack( Railtrack* track )
{
	// nur was machen, wenn es der richtige Modus ist
	if ( m_mode != BuildMode )
		return;

        m_railtracks << track;
        QRectF scenerect = static_cast<Scene*>(scene())->sceneRect().adjusted(200,200,-200,-200);

	// IF NO PART WAS SELECTED BEFORE, FIND A COLLISION-FREE POSITION FOR THE NEW PART
        if( !m_selected )
	{
            qreal movex = childrenBoundingRect().right()+150;
            qreal movey = childrenBoundingRect().center().y();
            if(int(movex)%50) movex -=25;
            if(int(movey)%50) movey -=25;

            track->moveBy(movex, movey);
            qDebug() << track->pos() << movex << movey << childrenBoundingRect();
            QPropertyAnimation* smoothshift = new QPropertyAnimation(this, "pos");
            smoothshift->setDuration(100);
            smoothshift->setEndValue(QPointF(-movex*static_cast<Scene*>(scene())->raster()/50, -movey*static_cast<Scene*>(scene())->raster()/50));
            smoothshift->start( QAbstractAnimation::DeleteWhenStopped );
        }
	else
		if( !scenerect.contains(mapToScene(track->pos())) )
		{
			QPropertyAnimation* smoothshift = new QPropertyAnimation(this, "pos");
			smoothshift->setDuration(100);
			smoothshift->setEndValue(mapToScene(m_selected->pos()-track->pos()));
			smoothshift->start(QAbstractAnimation::DeleteWhenStopped);
		}

	track->setParentItem( this );
	selectionChanged( track );
	if( track->isActivePart() )
	{
		connect( static_cast<ActiveTrack*>(track), SIGNAL(activatePin(int)), m_boardcontrol, SLOT(activatePin(int)) );
		connect( static_cast<ActiveTrack*>(track), SIGNAL(changeActivationTime(int,int)), m_boardcontrol, SLOT(changeActivationTime(int,int)) );
		showTrackConfig();
	}
}

Railtrack *TrackPlan::checkNextTrack( int type, ConType *correctConn, TrackNode *node, int nodeNumberOfNextTrack )
{
	// potentielles nächstes Gleisstück
	Railtrack *track = Railtrack::fromType(type);

	//SET ORIENTATION
	track->setDirection(correctConn->transform(), false );
	track->setDirection(m_selected->transform(), true );

	//SET POSITION
	// position von der ausgewählten node von m_selected
	QPointF selTrackNode  = node->pos();
	// position der passenden node an nextTrack
	QPointF nextTrackNode  = track->nodes()[nodeNumberOfNextTrack]->pos();
	QPointF selNodeMapped  = m_selected->mapToParent( selTrackNode );
	QPointF nextNodeMapped = track->mapToParent( nextTrackNode );
	track->setPos( selNodeMapped - nextNodeMapped );

	//CHECK FOR COLLISIONS - DO NOT LIST IF COLLIDE
	QList<QGraphicsItem*> list = static_cast<Scene*>(scene())->items(mapToScene(track->mapToParent(track->shape())));
	int lc = list.count();
	for ( int n = 0; n < lc; ++n )
		if ( list[n]->parentItem() == this )
		{
			delete track;
			return 0;
		}
	return track;
}

int TrackPlan::checkNextConn( TrackNode *sel, TrackNode *next, int type, int connIndex, QList<Railtrack*> &newtracks )
{
	QList<ConType*> conn = sel->connType();
	// über alle Verbindungen zu Anschluss connIndex von Gleisstücktyp type iterieren
	int m = 0;
	for ( ; m < conn.count(); ++m )
	{
		// Eine Verbindung für den Anschluss connIndex von Gleisstücktyp type
		ConType *c = conn[m];
		// warum das hier? was bedeutet das?
		if( c->nodeType() != next->nodeType() )
			continue;
		Railtrack *track = checkNextTrack( type, c, sel, connIndex );
		// everything's ok, add track to list
		if( track )
			newtracks << track;
	}
	return m;
}

#include "global.h"	// Num_RailTracks

/* passt irgendwo noch was ran?
 */
void TrackPlan::findOpenNode()
{
	QList<Railtrack*> newtracks;
	int itcnt = 0;	// profiling
	// der erste Anschluss des ausgewählten Gleisstückes
	QList<TrackNode*> selNodes = m_selected->nodes();

	// über alle Anschlüsse iterieren
	TrackNode* node = selNodes.first();
	// n ist überflüssig
	for ( int n = 0;
		n < m_selected->nodes().count();
		++n,	node->hide(),
			node = m_selected->nextNode() )	// das ist grütze
	{
		// über alle Gleisstücktypen iterieren
	        for ( int i = 0; i < Num_RailTracks; ++i )
		{
			// alle Anschlüsse von Gleisstücktyp i
			QList<TrackNode*> nodes = Railtrack::nodesFromType( i );
			// über alle Anschlüsse von Gleisstücktyp i iterieren
			for ( int k = 0; k < nodes.count(); ++k )
				itcnt += checkNextConn( node, nodes[k], i, k, newtracks );
		}
		// an diesen Anschluss passen keine weiteren Gleisstücke
		if( ! newtracks.isEmpty() )
		{
			//found open node
			m_activenode = node;
			m_activenode->show();
			qDebug() << "findOpenNode - " << itcnt << " iterations needed";
			emit updateRailtrackList( newtracks );
			return;
		}
	}
	// an keinen Anschluss passen weitere Gleisstücke
	m_activenode = 0;
	emit showDefaultList( false );
}

bool TrackPlan::checkForCollisions()
{
	QList<QGraphicsItem*> list = m_selected->collidingItems();
	int lc = list.count();
	for(int i=0; i<lc; ++i)
		if(list[i]->parentItem() == this)
			return true;
	return false;
}

void TrackPlan::updateTrackControls()
{
	if ( !m_selected )
	{
		emit setGeom(false, false, false, false);
		emit setNextNode(false);
		emit setDelete(false);
		emit setTrackConfig(false);
		return;
	}

	// machen und prüfen? und danach entscheiden?
    m_selected->rotateLeft();
    bool islrot = !checkForCollisions();
    m_selected->rotateRight();

    m_selected->rotateRight();
    bool isrrot = !checkForCollisions();
    m_selected->rotateLeft();

    m_selected->mirrorHorizontal();
    bool ishmir = !checkForCollisions();
    m_selected->mirrorHorizontal();

    m_selected->mirrorVertical();
    bool isvmir = !checkForCollisions();
    m_selected->mirrorVertical();

    emit setGeom(islrot, isrrot, ishmir, isvmir);
    emit setDelete(true);
    emit setNextNode(m_activenode);
    emit setTrackConfig(m_selected->isActivePart());
}

void TrackPlan::saveTrackRetMain()
{
	saveTrack();
	emit showMainMenu();
}

void TrackPlan::saveTrackRetLoad()
{
	saveTrack();
	emit showTrackLoad();
}

void TrackPlan::deselect()
{
	if ( m_activenode )
	{
		m_activenode->hide();
		m_activenode = 0;
	}
	if ( m_selected )
	{
		m_selected->setHighlighted( false );
		m_selected = 0;
	}
}

void TrackPlan::select( Railtrack *track )
{
	m_selected = track;
	m_selected->setHighlighted( true );
	findOpenNode();
	updateTrackControls();
}

void TrackPlan::selectionChanged(Railtrack* newSelection)
{
	if ( m_selected == newSelection )
		return;
	deselect();
	if ( newSelection == 0 )
	{
		emit showDefaultList( true );
		updateTrackControls();
		return;
	}
	else
		select( newSelection );
}

void TrackPlan::createPng( QString filepath )
{

	// create png file
	setPos(0,0);
	QPixmap pixmap(200,200);
	pixmap.fill( Qt::white );
	QPainter imagePainter( &pixmap );
	imagePainter.setRenderHints( QPainter::SmoothPixmapTransform );
	imagePainter.setBackgroundMode( Qt::TransparentMode );

	QRectF source = mapRectToScene( childrenBoundingRect() );
	qreal max = qMax( source.width(),source.height() );
	// was macht das?
	QRectF expand( -max/2.0, -max/2.0, max, max );
	expand.moveCenter( source.center() );
	source |= expand;

	static_cast<Scene*>(scene())->render( &imagePainter, QRectF(0,0,200,200), source, Qt::KeepAspectRatio );
	pixmap.save( filepath );
}

void TrackPlan::createXml( QString filepath )
{
	QFile ausgabe( filepath );
	if( !ausgabe.open( QIODevice::WriteOnly ) )
		return;
	{
		QXmlStreamWriter stream( &ausgabe );

		// write to xml stream
		stream.setAutoFormatting( true );
		stream.writeStartDocument();
		stream.writeStartElement( "TrackPlan" );

		int tc = m_railtracks.count();
		for( int i = 0; i < tc; ++i )
		{
			stream.writeStartElement("Track");
			stream.writeTextElement("Type", QString::number(m_railtracks[i]->trackType()));
			stream.writeTextElement("Direction", QString::number(m_railtracks[i]->direction()));
			stream.writeTextElement("X", QString::number(m_railtracks[i]->pos().x()));
			stream.writeTextElement("Y", QString::number(m_railtracks[i]->pos().y()));
			if( m_railtracks[i]->isActivePart() )
			{
				QList<int> pins = static_cast<ActiveTrack*>(m_railtracks[i])->pins();
				int pc = pins.count();
				for( int i = 0; i < pc; ++i )
					stream.writeTextElement("Pin"+QString::number(i), QString::number(pins[i]));
			}
			stream.writeEndElement(); //Track
		}
		stream.writeEndElement(); //TrackPlan
		stream.writeEndDocument();
	}
	ausgabe.close();
}

#include <QDateTime>

void TrackPlan::saveTrack()
{
	// choose file name for the track to save
	// Qt 4.6, 32bit-Wert geht 2031? nicht mehr:
	QString timestr = QString::number( QDateTime::currentDateTime().toTime_t() );
	// ab Qt 4.7
	//QString timestr = QString::number(QDateTime::currentMSecsSinceEpoch());

	/* dieses Konzept ist Grütze.
	 * wenn ich eine Datei erstelle, bekommt sie einen eindeutigen Namen.
         * danach wird sie nur noch geändert, der Name bleibt aber gleich.
         * eine datei namens recent kann den Namen der zuletzt geöffneten Dateien enthalten.
         * das ist aber ein anderes Konzept, und optional
	 */

	QString dated_filename = QString(TRACK_DIR)+timestr;
	QString recent_filename = QString(TRACK_DIR)+"recent";
	QString	recent_png = recent_filename + ".png";
	QString recent_xml = recent_filename + ".xml";
	QString	dated_png = dated_filename + ".png";
	QString dated_xml = dated_filename + ".xml";

	// wenn m_filename nicht recent_xml ist
	if ( QString::compare( m_filename, recent_xml ) )
	{
		// recent_xml nach dated_xml verschieben
		QFile::rename( recent_png, dated_png );
		QFile::rename( recent_xml, dated_xml );
	}

	emit hideMenus();
	deselect();

	// create png file
	createPng( recent_png );

	// write xml file
	createXml( recent_xml );

	// wenn m_filename nicht recent_xml ist
	if( !m_filename.isEmpty() &&
		QString::compare(m_filename, recent_xml) )
	{
		// m_filename löschen
		QFile::remove(m_filename);
		m_filename.replace(m_filename.length()-3,3,"png");
		QFile::remove(m_filename);
		m_filename = QString();
	}
}

// transform selected track
void TrackPlan::rotateLeft()
{
	if ( !m_selected )
		return;
        m_selected->rotateLeft();
        findOpenNode();
	updateTrackControls();
}

void TrackPlan::rotateRight()
{
	if ( !m_selected )
		return;
        m_selected->rotateRight();
        findOpenNode();
	updateTrackControls();
}

void TrackPlan::mirrorHorrizontal()
{
	if ( !m_selected )
		return;
        m_selected->mirrorHorizontal();
        findOpenNode();
	updateTrackControls();
}

void TrackPlan::mirrorVertical()
{
	if ( !m_selected )
		return;
        m_selected->mirrorVertical();
        findOpenNode();
	updateTrackControls();
}
// end transform selected track

void TrackPlan::nextNode()
{
	if ( !m_selected )
		return;
	if ( m_activenode )
		m_activenode->hide();
	m_selected->nextNode();
	findOpenNode();
	updateTrackControls();
}

void TrackPlan::deleteTrack()
{
	if ( !m_selected )
		return;

	// ???
        int at = m_railtracks.indexOf( m_selected );

        m_railtracks.removeOne( m_selected );
        delete m_selected;
        m_activenode = 0;

	// das war das letzte Gleisstück
        if ( m_railtracks.isEmpty() )
	{
		m_selected = 0;
		emit showDefaultList(true);
		updateTrackControls();
		return;
        }

	// vorheriges Gleisstück auswählen, wenn at nicht 0 ist.
	if ( at )
		at--;
	select( m_railtracks[at] );
}

void TrackPlan::showTrackConfig()
{
	if ( !m_selected )
		return;
	if ( !m_selected->isActivePart() )
		return;
	TrackConfig* tc = new TrackConfig(static_cast<ActiveTrack*>(m_selected));
	connect(tc, SIGNAL(close()),      tc,   SLOT(deleteLater()));
	connect(tc, SIGNAL(close()),      this, SLOT(hideProtLayer()));
	connect(tc, SIGNAL(deleteItem()), this, SLOT(deleteTrack()));
	connect(tc, SIGNAL(deleteItem()), tc,   SLOT(deleteLater()));
	connect(tc, SIGNAL(deleteItem()), this, SLOT(hideProtLayer()));
	showProtLayer();
	scene()->addItem(tc);
}

void TrackPlan::showProtLayer()
{
	if ( m_protlayer )
		return;
	m_protlayer = new ProtLayer();
	m_protlayer->setBrush(QColor(0xFF,0xFf,0xFF,0x80));
	scene()->addItem(m_protlayer);
}

void TrackPlan::hideProtLayer()
{
	if ( !m_protlayer )
		return;
	delete m_protlayer;
	m_protlayer = 0;
}

void TrackPlan::clearTrack() {
    int tc = m_railtracks.count();
    for(int i = 0; i < tc; ++i)
        delete m_railtracks[i];
    m_railtracks.clear();
    setPos(0,0);

    m_selected = 0;
    m_activenode = 0;
    m_mode = BuildMode;
}


int TrackPlan::mode() const
{ return m_mode; }

void TrackPlan::setMode(int mode)
{ m_mode = mode; }

void TrackPlan::rotateTrack() {

    centerToView();
    QTransform rot;
    rot.rotate(-90);
    int tc = m_railtracks.count();
    for(int i=0; i<tc; ++i) {
        m_railtracks[i]->setTransform(m_railtracks[i]->transform()*rot);
        m_railtracks[i]->setPos(rot.map(m_railtracks[i]->pos()));
    }
}

void TrackPlan::parseXml( QXmlStreamReader &stream )
{
	if ( stream.readNextStartElement() )
		if ( stream.name() != "TrackPlan" )
			return;

	while ( stream.readNextStartElement() )
	{
		if ( stream.name() != "Track" )
			return;

		stream.readNextStartElement();
		if (stream.name() != "Type")
			return;
		int type=stream.readElementText().toInt();

		stream.readNextStartElement();
		if (stream.name() != "Direction")
			return;
		int direction=stream.readElementText().toInt();

		stream.readNextStartElement();
		if (stream.name() != "X")
			return;
// würg around
//		int x=stream.readElementText().toInt();
		double x=stream.readElementText().toDouble();

		stream.readNextStartElement();
		if (stream.name() != "Y")
			return;
//		int y=stream.readElementText().toInt();
		double y=stream.readElementText().toDouble();

		Railtrack* track = Railtrack::fromType( type, this );
		m_railtracks << track;
		track->setDirection(direction);
		track->setPos(x,y);

		if ( track->isActivePart() ) {
			ActiveTrack *aTrack = static_cast<ActiveTrack*>(track);

			connect(aTrack, SIGNAL(activatePin(int)), m_boardcontrol, SLOT(activatePin(int)));
			connect(aTrack, SIGNAL(activatePin(int)), m_geoplan, SLOT(sensePin(int)));
			connect(aTrack, SIGNAL(changeActivationTime(int,int)), m_boardcontrol, SLOT(changeActivationTime(int,int)));

			QList<int> pins = static_cast<ActiveTrack*>(track)->pins();
			for ( int i = 0; i < pins.count(); ++i )
			{
				stream.readNextStartElement();
				if ( stream.name() != "Pin"+QString::number(i) )
					return;
				pins[i] = stream.readElementText().toInt();
			}
			aTrack->setPins(pins);
			m_geoplan->link(aTrack);
		}

		stream.readNextStartElement();
		if( !stream.isEndElement() )
			return;
	}
}

void TrackPlan::readXml( const QString &filepath )
{
	QFile eingabe( filepath );
	if( !eingabe.open( QIODevice::ReadOnly ) )
		return;
	clearTrack();
	QXmlStreamReader stream( &eingabe );
	parseXml( stream );
	eingabe.close();
}

void TrackPlan::loadTrackFromFile( const QString &filename )
{
	m_filename = filename;

	// das ist totale Grütze. wo wird das aufgerufen?
	if ( m_filename.isEmpty() )
	{
		clearTrack();
		return;
	}

	// xml datei lesen
	readXml( m_filename );

	//RESET ZOOMLEVEL
	static_cast<Scene*>(scene())->setRaster(20);
	setScale(0.4);
	//CENTER TO VIEW
	centerToView();
	updateTrackControls();
}

void TrackPlan::centerToView()
{
	setPos(0,0);
	QPointF point = -childrenBoundingRect().center();
	// was soll das?
	if ( point.toPoint().x() % 50 )
		point += QPointF( 25, 0 );
	if ( point.toPoint().y() % 50 )
		point += QPointF( 0, 25 );
	for ( int i = 0; i < m_railtracks.count(); ++i )
		m_railtracks[i]->moveBy(point.x(),point.y());
}

void TrackPlan::setBoardControl(BoardControl* boardcontrol)
{ m_boardcontrol = boardcontrol; }

void TrackPlan::initActiveTracks()
{
	if ( m_mode != PlayMode ) return;
	for ( int i = 0; i < m_railtracks.count(); ++i )
		// grusel!
		if ( m_railtracks[i]->isActivePart() )
			static_cast<ActiveTrack*>( m_railtracks[i] )->init();

	m_geoplan->updatePlan(m_railtracks); // this is too early, ActiveTracks not registered yet
}

QRectF TrackPlan::boundingRect() const
{ return QRectF(); }

void TrackPlan::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

void TrackPlan::mousePressEvent(QGraphicsSceneMouseEvent *event)
{ Q_UNUSED(event); }

void TrackPlan::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{ Q_UNUSED(event); }
