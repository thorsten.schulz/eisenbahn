#ifndef TRAIN_H
#define TRAIN_H

#include <QGraphicsObject>
#include "button.h"
#include "buttons/pixmapbutton.h"
#include "buttons/textbutton.h"

struct TrainData
{
        quint32 id;
        quint32 user;
        quint32 vmax;
        quint32 TrainLength;
        QString TrainName;
        QString TrainPicURL;
        QImage* img;
        quint8 img_request_pending;
};
class Train : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit Train( TrainData data, QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);

    int getTrainID() const;
    void updateTrainID(int id);

    int getUser() const;
    void updateUser(int user);

    int getVmax() const;
    void updateVmax(int vmax);

    int getTrainLength() const;
    void updateTrainLength(int length);

    QString getTrainName() const;
    void updateTrainName(QString name);

    QString getTrainPicURL() const;
    void updateTrainPicURL(QString url);

    QImage* getImg() const;
    void updatePixmap(const QImage& img);
    void updatePixmap(const QPixmap& pixmap);

    int imgRequestPending() const;
    void updateImgRequest(bool pending);

    void released();

signals:
    void PowerOff(int trainid);
    void PowerOn(int trainid);
    void speedChanged(int tranid, int speed);
    void fullStop(int trainid);

private slots:
    void accelerate();
    void decelerate();
    void power();

public slots:
    void stop();

private:
    QRectF m_bounds;

    PixmapButton* b_train;
    Button* b_power;
    Button* b_light;
    Button* b_horn;
    Button* b_stop;
    Button* b_accel;
    Button* b_decel;
    TextButton* b_speed;
    TrainData m_data;

    bool m_poweron;
    int m_speed;

signals:

public slots:

private slots:
    //void accelerate();
    //void decelerate();

};

#endif // TRAIN_H
