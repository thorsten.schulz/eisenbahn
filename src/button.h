#ifndef Button_H
#define Button_H

#include <QGraphicsObject>
#include <QPainter>
#include <QLinearGradient>
#include <QGraphicsSceneMouseEvent>

#include <QDebug>

class Button : public QGraphicsObject
{
    Q_OBJECT

public:
    enum ButtonType {
        NoType, ZoomIn, ZoomOut, Config, AddTrack, RotateLeft, RotateRight,
        MirrorVertical, MirrorHorizontal, Delete, BoardConfig, TrainConfig,
        TrackConfig, Pixmap, Track, Character, SwitchNode, Refresh, SaveTrack, LoadTrack,
        ScrollUp, ScrollDown, ScrollLeft, ScrollRight, Back, Text, Ok, AddBoard,
        Add, Exit, Stop, SOS, TrainLight, TrainHorn, PowerOn, RotateTrack
    };

    Button(ButtonType type, qreal wigth = 100, qreal height = 100, QGraphicsItem *parent = 0);
    Button(ButtonType type, qreal size = 100, QGraphicsItem *parent = 0);

    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    //TYPE
    ButtonType buttonType() const;
    //void setButtonType(const ButtonType type);
    //COLOR
    QColor color() const;
    void setColor(const QColor& color);

    QColor highlightColor() const;
    void setHighlightColor(const QColor& color);

    QLinearGradient linearGradient() const;
    void setLinearGradient(const QLinearGradient& gradient);

    void setSelected(bool selected);
    bool isSelected() const;

protected:
    void timerEvent(QTimerEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

    //TYPE
    ButtonType m_buttontype;
    //SIZE
    qreal m_size;
    QRectF m_bounds;

signals:
    void pressed();

private:
    //COLOR
    QColor m_color;
    QColor m_hcolor;
    QLinearGradient m_gradient;
    //STATE
    bool m_pressed;
    bool m_selected;
};

#endif // Button_H
