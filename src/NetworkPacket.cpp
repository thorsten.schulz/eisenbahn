#include "NetworkPacket.h"


CNetworkPacket::CNetworkPacket(QObject *parent) : QObject(parent)
{
	*((quint16*)&(data[0x0]))=htons(0x1); //Version
	*((quint32*)&(data[0x2]))=htonl(0xFFFFFFFF); //Receiver-ID
	*((quint32*)&(data[0x6]))=htonl(0x0); //Sender-ID
	data[0xA] = 0; //Response-Flag
	data[0xB] = 0; //Fragment-Flag
	*((quint32*)&(data[0xC]))=htonl(0x0); //Seg-No
	*((quint32*)&(data[0x10]))=htonl(0x0); //Seg-Count
	*((quint32*)&(data[0x14]))=htonl(0x0); //Length
	*((quint32*)&(data[0x18]))=htonl(0x0); //Packet-ID
	*((quint32*)&(data[0x1C]))=htonl(0x0); //Train-ID
	memset(&(data[0x20]), 0x0, 0x100);
}

CNetworkPacket::CNetworkPacket(const CNetworkPacket& original) : QObject()
{
	memcpy(this->data, original.data, sizeof(data));
}

CNetworkPacket& CNetworkPacket::operator=(const CNetworkPacket& c)
{
	if (this == &c)
		return *this;
	memcpy(this->data, c.data, sizeof(data));
	return *this;
}

CNetworkPacket::~CNetworkPacket(void)
{
}

void CNetworkPacket::GetData(quint8 buffer[], const quint8 offset, const quint16 length) const
{
	if ((quint16)length + offset <= 0xFF) //Paketlänge darf nicht länger als 0xFF werden, da ansonsten Pufferüberlauf!
	{
#ifdef WIN32
		memcpy_s(buffer, length, &(data[offset + 0x20]), length);
#else
		memcpy(buffer, &(data[offset + 0x20]), length); 
#endif
	}
}

void CNetworkPacket::GetData(quint8& buffer, const quint8 offset) const
{
	buffer = data[offset + 0x20];
}

void CNetworkPacket::GetData(quint16& buffer, const quint8 offset) const
{
	buffer = ntohs(*((quint16*)&(data[offset + 0x20])));
}

void CNetworkPacket::GetData(quint32& buffer, const quint8 offset) const
{
	buffer = ntohl(*((quint32*)&(data[offset + 0x20])));
}


quint8 CNetworkPacket::GetFragmentFlag(void) const
{
	return data[0xA];
}

quint32 CNetworkPacket::GetFragmentNumber(void) const
{
	return ntohl(*((quint32*)&(data[0xC])));
}

quint32 CNetworkPacket::GetLength(void) const
{
	return ntohl(*((quint32*)&(data[0x14])));
}

quint32 CNetworkPacket::GetPacketID(void) const
{
	return ntohl(*((quint32*)&(data[0x18])));
}

quint32 CNetworkPacket::GetReceiver(void) const
{
	return ntohl(*((quint32*)&(data[0x2])));
}

quint8 CNetworkPacket::GetResponseFlag(void) const
{
	return data[0xA];
}

quint32 CNetworkPacket::GetSegmentCount(void) const
{
	return ntohl(*((quint32*)&(data[0x10])));
}

quint32 CNetworkPacket::GetSender(void) const
{
	return ntohl(*((quint32*)&(data[0x6])));
}

quint32 CNetworkPacket::GetTrainID(void) const
{
	return ntohl(*((quint32*)&(data[0x1C])));
}

quint16 CNetworkPacket::GetVersion(void) const
{
	return ntohs(*((quint16*)&(data[0x0])));
}

void CNetworkPacket::SetData(const quint8 buffer[], const quint8 offset, const quint16 length)
{
	if ((quint16)offset + length <= 0xFF)
#ifdef WIN32
		memcpy_s(&(data[0x20 + offset]), 0x100 - offset, buffer, length);
#else
		memcpy(&(data[0x20 + offset]), buffer, length);
#endif
}

void CNetworkPacket::SetData(const quint8 buffer, const quint8 offset)
{
	data[0x20 + offset] = buffer;
}

void CNetworkPacket::SetData(const quint16 buffer, const quint8 offset)
{
	*((quint16*)&(data[0x20 + offset])) = htons(buffer);
}

void CNetworkPacket::SetData(const quint32 buffer, const quint8 offset)
{
	*((quint32*)&(data[0x20 + offset])) = htonl(buffer);
}

void CNetworkPacket::SetFragmentFlag(const quint8 fragment)
{
	data[0xB] = fragment;
}

void CNetworkPacket::SetFragmentNumber(const quint32 number)
{
	*((quint32*)&(data[0xC])) = htonl(number);
}

void CNetworkPacket::SetLength(const quint32 length)
{
	*((quint32*)&(data[0x14])) = htonl(length);
}

void CNetworkPacket::SetPacketID(const quint32 packetid)
{
	*((quint32*)&(data[0x18])) = htonl(packetid);
}

void CNetworkPacket::SetReceiver(const quint32 receiverid)
{
	*((quint32*)&(data[0x2])) = htonl(receiverid);
}

void CNetworkPacket::SetResponseFlag(const quint8 respflag)
{
	data[0xA] = respflag;
}

void CNetworkPacket::SetSegmentCount(const quint32 segcount)
{
	*((quint32*)&(data[0x10])) = htonl(segcount);
}

void CNetworkPacket::SetSender(const quint32 senderid)
{
	*((quint32*)&(data[0x6])) = htonl(senderid);
}

void CNetworkPacket::SetTrainID(const quint32 trainid)
{
	*((quint32*)&(data[0x1C])) = htonl(trainid);
}

void CNetworkPacket::SendPacket(QTcpSocket *socket)	const
{
	socket->write((char*)data, 288);
}

void CNetworkPacket::RecvPacket(QTcpSocket *socket)
{
	socket->read((char*)data, 288);
}

void CNetworkPacket::SetVersion(const quint16 version)
{
	*((quint16*)&(data[0])) = htons(version);
}

