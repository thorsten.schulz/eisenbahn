#include "board.h"

#include <QDebug>

Board::Board(QString &boardID, QList<QString> media, QMap<int, int> &pinport, bool active) {
    m_boardid = boardID;
    m_media   = media;
    m_pinport = pinport;
    m_active  = active;
}

int Board::mapPinToPort(int pin) const {
    return m_pinport.value(pin, -1);
}

int Board::mapPortToPin(int port) const {
    return m_pinport.key(port, -1);
}

void Board::setActive(bool active) {
    m_active = active;
}

bool Board::isActive() const {
    return m_active;
}

QString Board::activeMedium() const {
    return m_activemedium;
}

void Board::setActiveMedium(QString medium) {
    m_activemedium = medium;
}

QString Board::activeInterface() const {
    return m_activeintfc;
}

void Board::setActiveInterface(QString interface) {
    m_activeintfc = interface;
}

QList<QString> Board::media() const {
    return m_media;
}

QString Board::boardID() const {
    return m_boardid;
}
