#ifndef MAINMENU_H
#define MAINMENU_H

#include <QGraphicsObject>
#include <QPainter>
#include <QFile>
#include "button.h"
#include "buttons/pixmapbutton.h"

#include "config.h"

class MainMenu : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit MainMenu(QGraphicsItem* parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

signals:
    void showBoardConfig();
    void showTrackLoad();
    void close();
    void startPlay();

public slots:

private:
    Button* b_boardconfig;
    Button* b_trackload;
//    Button* b_trainconfig;
    Button* b_exit;
    PixmapButton* b_play;
};

#endif // MAINMENU_H
