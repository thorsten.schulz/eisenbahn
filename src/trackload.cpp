#include "trackload.h"

TrackLoad::TrackLoad(QGraphicsItem* parent)
    : QGraphicsObject(parent) {

    m_bounds = QRectF(-385,-235,640,470);
    m_scroll = 0;

    m_title = QString("Tracks");
    QGraphicsTextItem* titleItem = new QGraphicsTextItem(m_title, this);
    titleItem->setDefaultTextColor(Qt::white);
    titleItem->setFont(QFont("Arial", 20, QFont::Bold));
    titleItem->setPos(m_bounds.topLeft());

    m_dir = TRACK_DIR;

    b_menu          = new PixmapButton(LOGO,          90, this);
    b_deletetrack   = new Button(Button::Delete,      90, this);
    b_scrollup      = new Button(Button::ScrollUp,    90, this);
    b_scrolldown    = new Button(Button::ScrollDown,  90, this);
    b_load          = new Button(Button::Ok,          90, this);

    b_menu->setPos(305,-190);
    b_deletetrack->setPos(305,-95);
    b_deletetrack->setEnabled(false);
    b_scrollup->setPos(305,0);
    b_scrolldown->setPos(305,95);
    b_load->setPos(305,190);
    b_load->setEnabled(false);

    connect(b_menu,         SIGNAL(pressed()), this, SIGNAL(showMainMenu()));
    connect(b_deletetrack,  SIGNAL(pressed()), this, SLOT(deleteTrack()));
    connect(b_scrollup,     SIGNAL(pressed()), this, SLOT(scrollUp())   );
    connect(b_scrolldown,   SIGNAL(pressed()), this, SLOT(scrollDown()) );
    connect(b_load,         SIGNAL(pressed()), this, SLOT(loadTrack())  );

    m_mapper = new QSignalMapper(this);
    connect(m_mapper, SIGNAL(mapped(int)), this, SLOT(selectTrack(int)));
    setList();

    setAcceptHoverEvents(false);
}

void TrackLoad::deleteTrack()
{
	if ( m_filename.isEmpty() )
		return;
	if (QDir(m_dir).exists())
	{
		QFile::remove( m_filename );
		m_filename.replace(m_filename.length()-3,3,"png");
		QFile::remove( m_filename );
		setList();
	}
}

void TrackLoad::loadTrack() {
    m_scroll = 0;
    emit loadTrack(m_filename);
}

void TrackLoad::setList()
{
	for( int i = 0; i < m_tracks.count(); ++i )
		delete m_tracks[i];
	m_tracks.clear();
	m_files.clear();
	m_selected = 0;

	if ( !QDir(m_dir).exists() )
	{
		QDir( QDir::currentPath()).mkdir(QDir::cleanPath(TRACK_DIR));
		// TODO: Textbutton new dazupappen???
		return;
	}

	QStringList png("*.png");
	QStringList xml("*.xml");
	QFileInfoList files = QDir(m_dir).entryInfoList(png, QDir::Files, QDir::Time);

	int fc = files.count();

	if ( m_scroll <= 0 )
	{	
		m_scroll = 0;
		b_scrollup->setEnabled( false );
		b_scrolldown->setEnabled( fc > 5 );
	}
	else if(m_scroll >= fc-3)
	{
		b_scrollup->setEnabled(true);
		b_scrolldown->setEnabled(false);
	}
	else
	{
		b_scrollup->setEnabled(true);
		b_scrolldown->setEnabled(true);
	}

	TextButton* b_new = new TextButton("NEW", 200, this);
	b_new->setFontSize(50);
	b_new->setPos(m_bounds.topLeft()+QPoint(110,150));

	connect(b_new, SIGNAL(pressed()), m_mapper, SLOT(map()));
	m_mapper->setMapping(b_new, -1);

	for( int i=m_scroll; i<fc && i<5+m_scroll; ++i )
	{
            QString file = m_dir+files.at(i).fileName();
            PixmapButton* b_track = new PixmapButton(file,200, this);
            b_track->setPos(m_bounds.topLeft()+QPoint(110,150)+QPointF(((i-m_scroll+1)%3)*210, ((int)(i-m_scroll+1)/3)*210));
            connect(b_track, SIGNAL(pressed()), m_mapper, SLOT(map()));
            m_mapper->setMapping(b_track, i-m_scroll);
            file = file.replace(file.length()-3,3,"xml");
            m_tracks << b_track;
            m_files << file;
        }
        m_filename.clear();
        b_deletetrack->setEnabled(false);
        b_load->setEnabled(false);
}

QRectF TrackLoad::boundingRect() const
{ return m_bounds; }

void TrackLoad::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setRenderHint(QPainter::Antialiasing);

    QRectF shadowrect(m_bounds.translated(2,2));
    QLinearGradient shadowgrad(shadowrect.topLeft(), shadowrect.bottomRight());
    shadowgrad.setColorAt(0, QColor(0x0,0x0,0x0,0x40));
    shadowgrad.setColorAt(1, QColor(0x0,0x0,0x0,0x40));
    painter->setPen(Qt::NoPen);
    painter->setBrush(shadowgrad);
    painter->drawRoundedRect(shadowrect, 3, 3, Qt::AbsoluteSize);

    QLinearGradient grad(m_bounds.topLeft(),m_bounds.bottomLeft());
    grad.setColorAt(0, QColor(0x60,0x60,0x60));
    grad.setColorAt(1, QColor(0x10,0x10,0x10));
    painter->setPen(Qt::NoPen);
    painter->setBrush(grad);
    painter->drawRoundedRect(m_bounds, 3, 3, Qt::AbsoluteSize);
}

void TrackLoad::show()
{
    setList();
    QGraphicsItem::show();
}

void TrackLoad::selectTrack(int selected)
{
	if ( m_selected )
		m_selected->setSelected( false );
	// ???
	if ( selected == -1 )
	{
		m_filename = QString();
		loadTrack();
		return;
	}
	m_selected = m_tracks[selected];
	m_selected->setSelected( true );
	m_filename = m_files[selected];
	b_deletetrack->setEnabled( true );
	b_load->setEnabled( true );
}
