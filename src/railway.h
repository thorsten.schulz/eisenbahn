#ifndef RAILWAY_H
#define RAILWAY_H

#include <QGraphicsView>
#include <QResizeEvent>
#include <QWheelEvent>
#include <QMouseEvent>
#include "scene.h"
#include "trackplan.h"
#include "mainmenu.h"
#include "trackload.h"
#include "boardcontrol.h"
#include "railtracklist.h"
#include "controls.h"
#include "trackcontrols.h"
#include "infopanel.h"
#include "trackconfig.h"

class Railway : public QGraphicsView
{
    Q_OBJECT

public:
    Railway(QWidget* parent = 0);

protected:
    void resizeEvent(QResizeEvent* event);
    void wheelEvent(QWheelEvent* event);
    void mouseDoubleClickEvent(QMouseEvent* event);
    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);

private:
	void hideAll();
private slots:
    void showBoardConfig();
    void showTrackLoad();
    void showMainMenu();
    void showTrackPlan();
    void zoomIn();
    void zoomOut();
    void hideMenus();
    void startPlay();
    void showConnectionStatus(bool connected, QString& msg);

private:

    Scene* m_scene;
    TrackPlan* m_trackplan;
    MainMenu* m_mainmenu;
    TrackLoad* m_trackload;
    RailtrackList* m_railtracklist;
    Controls* m_controls;
    TrackControls* m_trackcontrols;
    InfoPanel* m_infopanel;
    BoardControl* m_boardcontrol;
//    PlayControls* m_playcontrols;

    bool m_movetrack;
    bool m_trackmoved;
    QPoint m_oldpos;
    QString m_baseTitle;

    void scaleView(int curScale);
};

#endif // RAILWAY_H
