#ifndef TRACKCONTROLS_H
#define TRACKCONTROLS_H

#include <QGraphicsObject>
#include "button.h"
#include <QPropertyAnimation>
#include <QSequentialAnimationGroup>

class TrackControls : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit TrackControls(QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

signals:
    void rotateLeft();
    void rotateRight();
    void hMirror();
    void vMirror();
    void nextNode();
    void deleteTrack();
    void showTrackConfig();

public slots:
    void show();
    void hide();
    void setGeom(bool islrot, bool isrrot, bool ishmir, bool isvmir);
    void setDelete(bool enable);
    void setNextNode(bool enable);
    void setTrackConfig(bool enable);

private:
    Button* b_rotleft;
    Button* b_rotright;
    Button* b_hmirror;
    Button* b_vmirror;
    Button* b_nextnode;
    Button* b_delete;
    Button* b_trackconf;

//    QSequentialAnimationGroup* m_ani;

};

#endif // TRACKCONTROLS_H
