#ifndef TRACKPLAN_H
#define TRACKPLAN_H

#include "railtrack.h"		// Railtrack?, TrackNode?
//#include "trackconfig.h"	// vielleicht in trackplan.cpp
//#include <QFile>
//#include <QXmlStreamReader>
//#include <QMessageBox>
//#include <QDateTime>
#include "boardcontrol.h"	// BoardControl
#include "geoplan.h"

//#include <QPropertyAnimation>

//#include "config.h"

#include <QGraphicsRectItem>
// warum ist das überhaupt da?
class ProtLayer : public QGraphicsRectItem
{
public:
	// size soll über das gesamte Fenster reichen. TODO: richtig machen
	ProtLayer( QRectF size = QRectF(-2500,-2500,5000,5000), QGraphicsItem* parent = 0 )
        : QGraphicsRectItem(size, parent) { }
	void mousePressEvent(QGraphicsSceneMouseEvent *event)
	{ event->accept(); }
	void mouseMoveEvent(QGraphicsSceneMouseEvent *event)
	{ event->accept(); }
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
	{ event->accept(); }
	void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
	{ event->accept(); }
};

#include <QGraphicsObject>
class TrackPlan : public QGraphicsObject
{
    Q_OBJECT
public:
    enum Mode {
        BuildMode,
        PlayMode
    };
    TrackPlan(QGraphicsItem* parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

    int mode() const;
    void setMode(int mode);

    void selectionChanged(Railtrack* newSelection);
    void findOpenNode();
    void centerToView();

    void setBoardControl(BoardControl* boardcontrol);
    void initActiveTracks();
    QObject *getGeo() const { return m_geoplan; }

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

signals:
    void showDefaultList(bool);
    void updateRailtrackList( QList<Railtrack*> );
    void setGeom(bool, bool, bool, bool);
    void setDelete(bool);
    void setNextNode(bool);
    void setTrackConfig(bool);
    void hideMenus();
    void showMainMenu();
    void showTrackLoad();

public slots:
    void rotateTrack();
    void loadTrackFromFile(const QString& filename);
    void addTrack(Railtrack* track);
    void saveTrackRetMain();
    void saveTrackRetLoad();
    void rotateLeft();
    void rotateRight();
    void mirrorHorrizontal();
    void mirrorVertical();
    void nextNode();
    void deleteTrack();
    void showTrackConfig();
    void clearTrack();

private:
    QList<Railtrack*> m_railtracks;
    int m_mode;

    QString m_filename;
    ProtLayer* m_protlayer;

    Railtrack* m_selected;
    TrackNode* m_activenode;
    BoardControl* m_boardcontrol;
    GeoPlan *m_geoplan;

    bool checkForCollisions();
    void updateTrackControls();
    void saveTrack();
	Railtrack *checkNextTrack( int type, ConType *correctConn, TrackNode *node, int nodeNumberOfNextTrack );
	int checkNextConn( TrackNode *sel, TrackNode *next, int type, int connIndex, QList<Railtrack*> &newtracks );

	// track clicking
	void deselect();
	void select( Railtrack *track );
	// file operations
	void createPng( QString );
	void createXml( QString );
	void readXml( const QString& );
	void parseXml( QXmlStreamReader& );

private slots:
    void showProtLayer();
    void hideProtLayer();
};

#endif // TRACKPLAN_H
