#ifndef TRACKCONFIG_H
#define TRACKCONFIG_H

#include <QGraphicsObject>
#include <QSvgRenderer>
#include "railtracks/activetrack.h"
#include <QPainter>
#include <QSignalMapper>
#include "button.h"
#include "buttons/textbutton.h"

#include "config.h"

class TrackConfig : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit TrackConfig(ActiveTrack* track, QGraphicsItem *parent = 0);

    QRectF boundingRect() const;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget);


signals:
    void close();
    void deleteItem();

private:
    QRectF m_bounds;
    ActiveTrack* m_track;
    QString m_title;
    QList<int> m_pins;

    QSignalMapper* m_mapcnt;
    QSignalMapper* m_mapper;
    int m_incdec;

    Button* b_delete;
    Button* b_ok;
    Button* b_cancel;

    Button* b_scrollup0;
    Button* b_scrollup1;
    TextButton* b_state0;
    TextButton* b_state1;
    Button* b_scrolldown0;
    Button* b_scrolldown1;

    QList<TextButton*> m_states;

    void checkConflicts(int last);


private slots:
    void setIncDec(int incdec);
    void setPin(int pin);
    void apply();

};

#endif // TRACKCONFIG_H
