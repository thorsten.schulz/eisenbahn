#include "mainmenu.h"

MainMenu::MainMenu(QGraphicsItem* parent)
: QGraphicsObject(parent)
{
	int raster = 160;
	int smallsize = 150;
	int largesize = 310;
	int offset = raster/2;

    b_boardconfig = new Button(Button::BoardConfig, smallsize, this);
    b_trackload   = new Button(Button::AddTrack,    smallsize, this);
    b_exit        = new Button(Button::Exit,        smallsize, this);
// TODO: Pfad ändern
    b_play        = new PixmapButton(LOGO,          largesize, this);

	// TODO: layout
    b_boardconfig->moveBy(offset,-offset);
    b_trackload->moveBy(3*offset,-offset);
    b_exit->moveBy(3*offset,offset);
    b_play->moveBy(-2*offset,0);
    if(!QFile::exists(QString(TRACK_DIR)+"recent.xml")) {
        b_play->setEnabled(false);
    }

    connect(b_boardconfig, SIGNAL(pressed()), this, SIGNAL(showBoardConfig()));
    connect(b_trackload,   SIGNAL(pressed()), this, SIGNAL(showTrackLoad()));
    connect(b_exit,        SIGNAL(pressed()), this, SIGNAL(close()));
    connect(b_play,        SIGNAL(pressed()), this, SIGNAL(startPlay()));

    setFlag(QGraphicsItem::ItemHasNoContents);
    setAcceptHoverEvents(false);
}

QRectF MainMenu::boundingRect() const {
    return QRectF();
}

void MainMenu::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
    return;
}
