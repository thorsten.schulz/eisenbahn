#include "controls.h"

Controls::Controls(QGraphicsItem *parent)
    : QGraphicsObject(parent) {

    b_menu       = new PixmapButton(LOGO,           70, this);
    b_zoomin     = new Button(Button::ZoomIn,       70, this);
    b_zoomout    = new Button(Button::ZoomOut,      70, this);
    b_rotatetrack = new Button(Button::RotateTrack, 70, this);
    b_loadtrack  = new Button(Button::AddTrack,     70, this);
    //b_savetrack  = new Button(Button::SaveTrack,    70, this);

    b_zoomin->moveBy(0,73);
    b_zoomout->moveBy(0,146);
    b_rotatetrack->moveBy(0,219);
    b_loadtrack->moveBy(0,292);
    //b_savetrack->moveBy(0,292);

    connect(b_menu,        SIGNAL(pressed()), this, SIGNAL(showMainMenu()) );
    connect(b_zoomin,      SIGNAL(pressed()), this, SIGNAL(zoomIn())       );
    connect(b_zoomout,     SIGNAL(pressed()), this, SIGNAL(zoomOut())      );
    connect(b_rotatetrack, SIGNAL(pressed()), this, SIGNAL(rotateTrack())  );
    connect(b_loadtrack,   SIGNAL(pressed()), this, SIGNAL(showTrackLoad()));
    //connect(b_savetrack, SIGNAL(pressed()), this, SIGNAL(saveTrack())      );

    setFlag(QGraphicsItem::ItemHasNoContents);
    setAcceptHoverEvents(false);
}

void Controls::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}
