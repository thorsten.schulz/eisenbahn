#ifndef BOARD_H
#define BOARD_H

#include <QMap>
#include <QString>
#include <QStringList>

class Board
{
public:
    Board(QString &boardID, QList<QString> media, QMap<int, int> &pinport, bool active);

    int mapPinToPort(int pin) const;
    int mapPortToPin(int port) const;

    void setActive(bool active);
    inline void activate()   { setActive(true);  }
    inline void deactivate() { setActive(false); }
    bool isActive() const;

    QList<QString> media() const;
    QString boardID() const;

    QString activeMedium() const;
    void setActiveMedium(QString medium);

    QString activeInterface() const;
    void setActiveInterface(QString interface);

private:
    QString m_filename;
    QString m_boardid;
    QList<QString> m_media;
    QString m_activemedium;
    QString m_activeintfc;
    QMap<int, int> m_pinport;
    bool m_active;
};

#endif // BOARD_H
