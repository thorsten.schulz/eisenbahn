/*
 * geoplan.cpp
 *
 *  Created on: 02.12.2015
 *      Author: thorsten
 */
#include <QDebug>
#include <QNetworkInterface>
#include <QApplication>
#include <QFile>
#include <QMessageBox>
#include <QFileDialog>
#include <arpa/inet.h> /* htonl */

#include "geoplan.h"

sid_t      TrackKnot::glob_id;
velocity_t TrackKnot::v_default   = 80;
int32_t    TrackKnot::model_scale = 50000/3000;


TrackKnot::TrackKnot(int pin_straight, int pin_right, int pin_left, int32_t len, velocity_t v = v_default) {

	orint = TKO_NA;
	d.sid = ++glob_id;
	d.length = model_scale * len;
	d.vStraight = v;
	d.vTurn = ((v >= 0) && (v < v_turn)) ? v : v_turn;

	d.lines[TKL_BLUNT].connected = true;
	d.lines[TKL_BLUNT].to = 0;
	d.lines[TKL_BLUNT].to_line = TKL_BLUNT;
	d.lines[TKL_STRAIGHT].connected = (pin_straight < 1); // connected if not a switch
	d.lines[TKL_STRAIGHT].to = 0;
	d.lines[TKL_STRAIGHT].to_line = TKL_BLUNT;
	d.lines[TKL_RIGHT].connected = false;
	d.lines[TKL_RIGHT].to = 0;
	d.lines[TKL_RIGHT].to_line = TKL_BLUNT;
	d.lines[TKL_LEFT].connected = false;
	d.lines[TKL_LEFT].to = 0;
	d.lines[TKL_LEFT].to_line = TKL_BLUNT;

	pin[TKL_BLUNT] = -1;
	pin[TKL_STRAIGHT] = pin_straight;
	pin[TKL_RIGHT] = pin_right;
	pin[TKL_LEFT] = pin_left;

	to[TKL_BLUNT] = NULL;
	to[TKL_STRAIGHT] = NULL;
	to[TKL_RIGHT] = NULL;
	to[TKL_LEFT] = NULL;

	lockedTo = nil;
	aT = NULL;
	vlength = 200; /* save default for the switch tracks */
	rTinverse = false;
}

TrackKnot *TrackKnot::Connector(int32_t length = 0, velocity_t v = v_default) {
	return new TrackKnot(-1, -1, -1, length, v);
}

TrackKnot *TrackKnot::CurveSL(int pStraight, int pLeft,  velocity_t v = v_curve_def) {
	return new TrackKnot(pStraight, -1, pLeft, curved_switch_length, v);
}

TrackKnot *TrackKnot::CurveSR(int pStraight, int pRight, velocity_t v = v_curve_def) {
	return new TrackKnot(pStraight, pRight, -1, curved_switch_length, v);
}

TrackKnot *TrackKnot::SwitchL(int pStraight, int pLeft,  velocity_t v = v_default) {
	return new TrackKnot(pStraight, -1, pLeft, straight_switch_length, v);
}

TrackKnot *TrackKnot::SwitchR(int pStraight, int pRight, velocity_t v = v_default) {
	return new TrackKnot(pStraight, pRight, -1, straight_switch_length, v);
}

void TrackKnot::resetId() { glob_id = 0; }

bool TrackKnot::sensePin(int signalledPin) {
	if (isActive()) {
		unsigned tkl=TKL_STRAIGHT;
		for(; tkl<TKLs; tkl++) if (signalledPin == pin[tkl]) break;
		if (canSwitch(tkl)) {
			for(unsigned i=TKL_STRAIGHT; i<TKLs; i++) d.lines[i].connected = (i==tkl);
			return true;
		}
	}
	return false;
}

bool TrackKnot::link(TrackKnotLine self, TrackKnot *tk2, TrackKnotLine other) {
	d.lines[self].to = tk2->getSid();
	to[self] = tk2;
	d.lines[self].to_line = other;
	return has(self);
}

bool TrackKnot::stretch(TrackKnotLine tkl, int32_t dist) {
	// question remains, could I stretch a switch that is not electrified, one that cannot switch?
	// No, that'll by buggy. Define such a zombie with pins that don't signal
	if ((tkl != TKL_STRAIGHT) || isActive())
		return false; // can only stretch STRAIGHT lines on non-switches

	d.length += model_scale*dist;
	return true;
}

bool TrackKnot::setOrientation(TrackKnotOrientation o) {
	if (orint == o) return false;
	orint = o;
	d.sid = (o == TKO_CW) ? -abs(d.sid) : abs(d.sid);
	TrackKnotOrientation no = (o==TKO_CCW) ? TKO_CW : (o==TKO_CW ? TKO_CCW : TKO_NA); // negate

	/* propagte this to connected neighbors */

	for (unsigned tkl=TKL_BLUNT; tkl<TKLs; tkl++) {
		if (has(tkl)) {
			to[tkl]->setOrientation( // flip direction of connected track when blunt xor non-blunt ends meet
					((tkl == TKL_BLUNT) == (d.lines[tkl].to_line == TKL_BLUNT)) ? no : o
			);
			d.lines[tkl].to = to[tkl]->getSid(); // make sure we have its right sid
		}
	}
	return true;
}

void TrackKnot::serialize(QByteArray *ba, bool marshall, bool hton) const {
	if (!marshall) {
		ba->append((const char *)&d, sizeof(trackKnot));
	} else {
		trackKnot t;
		if (hton) {
			t.sid = htonl(d.sid);
			t.length = htonl(d.length);
			t.vStraight = htons(d.vStraight);
			t.vTurn = htons(d.vTurn);
			for (unsigned tkl=0; tkl<TKLs; tkl++) {
				t.lines[tkl].to = htons(d.lines[tkl].to);
				t.lines[tkl].to_line = d.lines[tkl].to_line;
				t.lines[tkl].connected = d.lines[tkl].connected;
			}
		} else
			t = d;

		ba->append((char *)&t.sid, 4);
		ba->append((char *)&t.length, 4);
		ba->append((char *)&t.vStraight, 2);
		ba->append((char *)&t.vTurn, 2);

		for (unsigned tkl=0; tkl<TKLs; tkl++) {
			if (has(tkl)) {
				ba->append((char *)&t.lines[tkl].to, 2);
				ba->append((char)t.lines[tkl].to_line);
				ba->append((char)t.lines[tkl].connected);
			} else { // strive for fixed length packet
				uint32_t x = 0;
				ba->append((char *)&x, 4);
			}
		}
	}
}

bool TrackKnot::matchAndTake(ActiveTrack *signallingTrack) {
	if (signallingTrack && (has(TKL_RIGHT) || has(TKL_LEFT))
			&& signallingTrack->pins().contains(pin[TKL_STRAIGHT])) {
		aT = signallingTrack;
		return true;
	} else
		return false;
}

void TrackKnot::resetSections(const tid_t tid) {
	if (tid != nil) m_sections[tid] = Sections(); else m_sections.clear();
	if ((tid == lockedTo) || (tid == nil)) {
		lockedTo = nil;
		if (aT) aT->setLocked(lockedTo);
	}
	updateGui(tid);
}

void TrackKnot::reduceSections(const tid_t tid) {
	if (tid == nil) return;
	bool covers = m_sections[tid].reduce();
	if (tid == lockedTo && !covers) {
		lockedTo = nil;
		if (aT) aT->setLocked(lockedTo);
	}
	updateGui(tid);
}

void TrackKnot::importSections(tid_t tid, const sections *s, bool useCommFront) {
	if (tid != nil) {

		m_sections[tid] = Sections(s);
		if (has(TKL_RIGHT) || has(TKL_LEFT)) {
			bool isCrossing = (s->Rear != (useCommFront ? s->Front : s->ProtectedFront));
			lockedTo =
				isCrossing ? // if the train's body / protection zone spans across the TK
						tid : // lock it
						((lockedTo == tid) ? // otherwise, if we used to lock it
								nil :        // unlock
								lockedTo);   // or keep the lock as it was

			if (aT) aT->setLocked(lockedTo);
		} // links cannot be locked
		updateGui(tid);
//		if (atid==1 && abs(d.sid)==17) qDebug() << m_sections[atid-1].d.Rear << m_sections[atid-1].d.Front;
	}
}

void TrackKnot::updateGui(tid_t tid) const { /* maps visual sections into the link */
	static const QColor color[] = {
			QColor::fromHsv(  0,  0,  0) , QColor::fromHsv(210,255,128, 192) , QColor::fromHsv(  0,  0,255) ,
			QColor::fromHsv( 60,255,255) , QColor::fromHsv(30,255,192) , QColor::fromHsv(  0,255,192) ,
			QColor::fromHsv(150,192,255,128) };

	std::vector<PartialTrackTint> sects; /* 0|___|Rear|_B_|Front|_LG_|ProtF|_Gy_|PI|_W_|I|_Y_|P|_Or_|FLOI|_R_|Target|___<>...*/
	/* should contain 14 entries with relative start and end for a color*/


	if (tid != nil) for (QMap<tid_t, Sections>::const_iterator s = m_sections.begin(); s != m_sections.end(); ++s) {
		if (s->d.ProtectedFront !=     s->d.PI)    sects.push_back(PartialTrackTint(s->d.ProtectedFront, s->d.PI    , color[1], 'G'));
		if (s->d.Front  != s->d.ProtectedFront)    sects.push_back(PartialTrackTint(s->d.Front , s->d.ProtectedFront, color[6], 'g'));
		if (s->d.PI         != s->d.Indication)    sects.push_back(PartialTrackTint(s->d.PI        , s->d.Indication, color[2], 'W'));
		if (s->d.Indication != s->d.Permitted )    sects.push_back(PartialTrackTint(s->d.Indication, s->d.Permitted , color[3], 'Y'));
		if (s->d.Permitted  != s->d.FLOI      )    sects.push_back(PartialTrackTint(s->d.Permitted , s->d.FLOI      , color[4], 'O'));
		if (s->d.FLOI       != s->d.Target    )    sects.push_back(PartialTrackTint(s->d.FLOI      , s->d.Target    , color[5], 'R'));
		if (s->d.Rear       != s->d.Front     )    sects.push_back(PartialTrackTint(s->d.Rear      , s->d.Front     , color[0], '0' + (tid % 10)));
	}
	if (aT)
		aT->setTint(tid, sects);
	else {
		double start = 0;
		double end = 0;
		for (int i=0; i<rT.size(); i++) {  /* go through all visual tiles along the link, assuming they are in order */
			std::vector<PartialTrackTint> ssects;
			if (tid != nil) {
				end += rT[i]->getLength()/(double)vlength;
	/*			if (getSid()==-26) {
					qDebug() << getSid() << "subsect[" << i << "] Type" << rT[i]->trackType() << (int)(100*start) << (int)(100*end) << "length" << rT[i]->getLength();
					rT[i]->setDebug(true);
				}
	*/			for(PartialTrackTint& ptt: sects) { /* see, how the sections fit into the sub-tiles */
					if (ptt.isWithinTile(start, end)) { /* does this section (from above) overlap the current tile area? (relative to cmplt. link) */
						PartialTrackTint sptt = ptt.sect(start, end);
						ssects.push_back(sptt); /* create a subsection for the particular tile */
	//					if (getSid()==-26) qDebug() << ptt.m_id << "sub" << (int)(sptt.m_start*100) << (int)(sptt.m_end*100) << "within" << (int)(ptt.m_start*100) << (int)(ptt.m_end*100);
					}
				}
				start = end;
			}
			rT[i]->setTint(tid, ssects); /* pint the tile */
		}
	}
}

/* Zur Info:
ShortStraight 0, QPointF(50,25)
              1, QPointF( 0,25)

LongStraight  0, QPointF(100,25)
              1, QPointF(-50,25)

LongCurved    2, QPointF( 100, 25)
              1, QPointF(-100,-25)

ShortCurved   5, QPointF( 50, 50)
              4, QPointF(-50,-25)

HalfCurved    2, QPointF( 50, 25)
              3, QPointF(-50,-12)

StraightSwitch0, QPointF( 100,-25)
              1, QPointF(-100,-25)
              2, QPointF( 100, 25)

CurvedSwitch  7, QPointF( 100,-12)
              1, QPointF(-100,-25)
              2, QPointF( 100, 25)

Decouple      0, QPointF(100,25)
              1, QPointF(-50,25)

ShortAngled   5, QPointF( 50, 50)
              8, QPointF(  0,  0)

Switch3       0,  QPointF( 100, 25)
              1,  QPointF(-100, 25)
              11, QPointF( 100,-25)
              2,  QPointF( 100, 75)
 */

QPointF TrackKnot::toNodePoint(TrackKnotLine tkl) {
	switch (tkl) {
	case TKL_BLUNT:
		return (has(TKL_RIGHT) != has(TKL_LEFT)) ? QPointF(-100,-25) : QPointF(-100, 25);
	case TKL_STRAIGHT:
		return (has(TKL_RIGHT) != has(TKL_LEFT)) ? QPointF( 100,-25) : QPointF( 100, 25); /* a curved switch's straight line-node is at -12, so an unsharp search is required */
	case TKL_RIGHT:
		return (has(TKL_RIGHT) != has(TKL_LEFT)) ? QPointF( 100, 25) : QPointF( 100, 75);
	case TKL_LEFT:
		return (has(TKL_RIGHT) != has(TKL_LEFT)) ? QPointF( 100, 25) : QPointF( 100,-25);
	default:
		return QPointF(0,0);
	}
}

void TrackKnot::collectTracks(QList<Railtrack*> rtl, TrackKnot *from) {
	if (rtl.isEmpty() || orint == TKO_NA) return; /* could also be an assert */
	if (isActive()) {                /* this is a connected activeTrack */
		QVector<TrackKnot*> spur;
		QVector<Railtrack*> crumbs;
		// find next known
		for (unsigned tkl=TKL_BLUNT; tkl <= TKL_LEFT; tkl++) if (has(tkl) && (to[tkl] != from) && to[tkl]->rT.isEmpty()) {
			spur.clear(); /* takes all links in between two ActiveTracks */
			crumbs.clear();
			double slength = 0;
			TrackKnot *curr = this;
			TrackKnot *next = curr->to[tkl];
			TrackKnotLine tkln = curr->d.lines[tkl].to_line == TKL_BLUNT ? TKL_STRAIGHT : TKL_BLUNT; /* select the other side's type of the next/connected passive track */
			while (!next->isActive()) { /* as long as the next is a passive links */
				curr = next;
				curr->rTinverse = (tkln == TKL_BLUNT); /* if the other side is blunt, it is a reversed track */
				spur << curr;
				slength += curr->d.length;
				if (curr->has(tkln)) { /* as above, preload next track */
					next = curr->to[tkln];
					tkln = curr->d.lines[tkln].to_line == TKL_BLUNT ? TKL_STRAIGHT : TKL_BLUNT;
				} else break;
			}
			if (spur.size()) { // so there is something in between the known switch-tracks
				double vlen = aT->find(rtl, crumbs, toNodePoint((TrackKnotLine)tkl), next->aT); /* summarize the on-screen length */
				double upTo = 0;   // relative distance on the spur of TrackKnots
				double passed = 0; // relative ground made good on the trackplan
				int i = 0;         // crappy iterator

				upTo += spur[0]->d.length/slength; /* how much length of the overall spur are we covering in the first spur link element */
				spur[0]->vlength = 0;
				foreach(Railtrack *crt, crumbs) {
					double l = crt->getLength()/vlen; /* relative length of that one visual onscreen track */
					if (((passed+(l/2.0)) >= upTo) && ((i+1)<spur.size())) { /* if the mid of that track reaches beyond the first spur element and there is more */
						upTo += spur[++i]->d.length/slength; /* extend our search range */
						spur[i]->vlength = 0;              /* and include the next spur link element */
					}

					if (spur[i]->rTinverse) {              /* before we add the onscreen tile to the Link, check if the link is reversed */
						crt->toggleTintDirection();
						spur[i]->rT.prepend(crt);          /* in a reversed track, we obviously need to add tiles the other way */
					} else
						spur[i]->rT.append(crt);
					spur[i]->vlength += crt->getLength();  /* tell the spur, how long it grows on the screen */
					passed += l;                           /* sumup, what has been achieved */
				}
			}
			if (next->isActive()) /* go on with next active track */
				next->collectTracks(rtl, this);
			// it could also be just a dead-end, in which case the recursion ends
		}
	} else {
		if (has(TKL_LEFT) || has(TKL_RIGHT)) { /* and not known */
			qDebug() << "Inconsistency problem. Switching TrackKnot not linked. bailing out";
			QMessageBox::critical(QApplication::activeWindow(), QString("Assembling TrackPlan"), "Inconsistency problem. Switching TrackKnot not linked. bailing out");
		} else {
			/* basically only on first run, to seed to first known part */
			for (unsigned tkl=0; tkl<TKLs; tkl++) if (has(tkl)) to[tkl]->collectTracks(rtl, this);
		}
	}
}

/* ************************************************************************* */
/* *** GeoPlan ************************************************************* */
/* ************************************************************************* */

GeoPlan::GeoPlan(QObject *parent) : QObject( parent ) {

	QStringList args = QApplication::arguments();
	QString xname;
	QString title = QString("TRDP Interface");
	QWidget *top = QApplication::activeWindow();

	if (args.size() > 1) {
		foreach (const QString &arg, args) {
			QString s;
			if (arg.contains(".xml", Qt::CaseInsensitive)) {
				if (QFile::exists(arg)) {
					if (xname.isEmpty()) {
						xname = arg;
					} else {
						QTextStream(&s) << "Found multiple xml-path references on command line. Took: " << xname;
					}
				} else {
					QTextStream(&s) << "Oops, cannot find " << arg;
				}
			} else if (arg.contains("scale=", Qt::CaseInsensitive)) {
				bool ok = false;
				QString parm = arg;
				parm.remove("scale=", Qt::CaseInsensitive);
				uint sc = parm.toUInt(&ok);
				if (ok && sc > 100 && sc < 10000) {
					TrackKnot::model_scale = sc/10;
					if (sc > 300) TrackKnot::v_default = 180;
				} else {
					QTextStream(&s) << "Scale given on command line, but that seems unreasonable: " << arg;
				}
			}
			if (!s.isEmpty()) {
				qDebug() << "[ WARN ] " << s;
				QMessageBox::warning(top, title, s);
			}
		}
	}

	if (xname.isEmpty()) {
		xname = QFileDialog::getOpenFileName(nullptr, tr("Please select TRDP-XML configuration"), "../", tr("XML files (*.xml)"));
	}
	if (xname.isEmpty()) {
		QString s = QString("Please name a valid config file for TRDP on the command line.");
		qDebug() << "[ CRIT ] " << s;
		QMessageBox::critical(top, title, s);
		return;
	}

	if (trdp.load( xname.toLocal8Bit() , 0, GeoPlan::cerr, NULL ) == TRDP_NO_ERR)
		trdp.init( "GG.nano.uplink",
					5000, /* actually, could be -1, we are not sending cyclic packets */
					5000, /* allow a margin of 5ms for sending */
					this);

	dumpArray = false;
	active = false;
	marshall = true;
	netByteOrder = true;
	configuredTrains = 0;
	useCommFront = true; /* TODO, this is actually a decision, the eStw must calculate, ie, only if all choices are safe - const only for early trials. */
	/* this will become even more interesting: multiple trains may like to get certain guarantees from a switch, passing it consecutively */
	trdpPRID = nil;
	trdpAllPosiID = nil;
	trdpTrackDMIID = nil;
	trdpTrackPlanID = nil;
	trdpTimeTrialsID = nil;

	if ( trdp.up() ) {
		entry = constructKoffer();

		TRDP_DATASET_ELEMENT_T *el = NULL;
		trdp.lookupVariable(1141, "PositionRep", &el);
		configuredTrains = (el && (el->size < maxTrains)) ? el->size : maxTrains;
		timerRefreshId = startTimer(50); /* start after an arbitrary timeout */
		trdp.subscribe(1112, &trdpTrackDMIID,   1, callReadLocs);
		trdp.subscribe(1142, &trdpPRID,         1, callReadPosi);
		trdp.subscribe(1301, &trdpTimeTrialsID, 1, callReadTime);
		updatePosis();
		updatePlan();
	} else {
		entry = NULL;
		configuredTrains = 0;
		timerRefreshId = -1;
		QString s = QString("TRDP session is not available, is the network configured right?");
		qDebug() << "[ WARN ] " << s;
		QMessageBox::warning(top, title, s);
	}
}

bool GeoPlan::link(TrackKnot *tk1, TrackKnotLine tk1l, TrackKnotLine tk2l, TrackKnot *tk2, int32_t dist = 0) {

	if (!dist) { // direct connect
		if (!tk1->link(tk1l, tk2, tk2l)) return false;
		if (!tk2->link(tk2l, tk1, tk1l)) return false;

		int maxIndex = tk1->getIndex() > tk2->getIndex() ? tk1->getIndex() : tk2->getIndex();
		if (m_knots.size() <= maxIndex) m_knots.resize(maxIndex+1);
		m_knots[tk1->getIndex()] = tk1;
		m_knots[tk2->getIndex()] = tk2;
	} else {
		if (tk1->stretch(tk1l, dist) || tk2->stretch(tk2l, dist))
			link(tk1, tk1l, tk2l, tk2);
		else {
			TrackKnot *tk = TrackKnot::Connector(dist); // recurse into direct connect with helper
			if (!link(tk1, tk1l, TKL_BLUNT, tk) || !link(tk, TKL_STRAIGHT, tk2l, tk2)) {
				delete(tk);
				return false;
			}
		}
	}
	return true;
}

void GeoPlan::link(ActiveTrack *newAT) {
	foreach(TrackKnot *tk, m_knots) if (tk->matchAndTake(newAT)) break;
}

/* This is the "hard-coded" logical track description of the Koffer. */
/* First, the active TrackKnots are linked to the IDs from the control board */
/* Then they are connected, potentially with passive tracks inbetween. */

TrackKnot *GeoPlan::constructKoffer() {
	while (!m_knots.isEmpty()) delete m_knots.last();
	m_knots.resize(38);

	TrackKnot::resetId();

	TrackKnot *no0 = TrackKnot::SwitchL(48,66);
	TrackKnot *no1 = TrackKnot::SwitchL(47,65);

	TrackKnot *o1  = TrackKnot::CurveSL(50,68);
	TrackKnot *o2  = TrackKnot::CurveSR(51,69);
	TrackKnot *o0  = TrackKnot::SwitchR(49,67);

	TrackKnot *so3 = TrackKnot::SwitchR(56,74);
	TrackKnot *so2 = TrackKnot::SwitchR(55,73);
	TrackKnot *so1 = TrackKnot::SwitchR(54,72);

	TrackKnot *nw0 = TrackKnot::SwitchR( 2,20);
	TrackKnot *nw1 = TrackKnot::SwitchR( 3,21);

	TrackKnot *w1  = TrackKnot::CurveSR( 4,22);
	TrackKnot *w2  = TrackKnot::CurveSL( 7,25);
	TrackKnot *w0  = TrackKnot::SwitchL( 5,23);

	TrackKnot *sw2 = TrackKnot::SwitchL( 8,26);
	TrackKnot *sw1 = TrackKnot::SwitchL( 9,27);

	TrackKnot *xct = TrackKnot::Connector(17, 0); // dead end

	TrackKnot *sw0 = TrackKnot::Connector(); // 17
	TrackKnot *sw3 = TrackKnot::Connector();
	TrackKnot *so0 = TrackKnot::Connector();
	TrackKnot *no2 = TrackKnot::Connector();
	TrackKnot *nw2 = TrackKnot::Connector();

	TrackKnot *so2_ = TrackKnot::Connector(); // 22
	TrackKnot *sw2_ = TrackKnot::Connector();
	TrackKnot *no1_ = TrackKnot::Connector();
	TrackKnot *nw1_ = TrackKnot::Connector();

	const int csl = TrackKnot::curved_switch_length; // shortcut

	link(nw0, TKL_STRAIGHT, TKL_STRAIGHT, no0, 476);
	link(nw1, TKL_BLUNT   , TKL_BLUNT   , no1, 257);
	link(nw0, TKL_RIGHT   , TKL_RIGHT   , nw1,   0);
	link(no0, TKL_LEFT    , TKL_LEFT    , no1,   0);

	link( w0, TKL_BLUNT   , TKL_BLUNT   , nw0, 464);
	link( o0, TKL_BLUNT   , TKL_BLUNT   , no0, 466);
	link( w1, TKL_BLUNT   , TKL_BLUNT   ,  w2,   0);
	link( o1, TKL_BLUNT   , TKL_BLUNT   ,  o2,   0);

	link( o1, TKL_LEFT    , TKL_BLUNT   , no2,-csl+375);
	link(no2, TKL_STRAIGHT, TKL_BLUNT   , nw2, 677);
	link(nw2, TKL_STRAIGHT, TKL_RIGHT   ,  w1, 373-csl);
	link(nw1, TKL_STRAIGHT, TKL_BLUNT   , nw1_,  0);
	link(nw1_,TKL_STRAIGHT, TKL_STRAIGHT,  w1,-csl+483);
	link(no1, TKL_STRAIGHT, TKL_BLUNT   , no1_,  0);
	link(no1_,TKL_STRAIGHT, TKL_STRAIGHT,  o1, 516-csl);


	link( w2, TKL_LEFT    , TKL_BLUNT   , sw3, 387-csl);
	link(sw3, TKL_STRAIGHT, TKL_STRAIGHT, so3, 580);
	link( o2, TKL_RIGHT   , TKL_BLUNT   , so3, 371    -csl);
	link(xct, TKL_BLUNT   , TKL_RIGHT   , so3,   0);


	link( w2, TKL_STRAIGHT, TKL_BLUNT   , sw2_, 513-csl-105);
	link(sw2_,TKL_STRAIGHT, TKL_STRAIGHT, sw2, 105);
	link(sw2, TKL_BLUNT   , TKL_BLUNT   , so2, 313);
	link( o2, TKL_STRAIGHT, TKL_STRAIGHT, so2_, 457-csl);
	link(so2_,TKL_BLUNT   , TKL_STRAIGHT, so2,   0);

	link(so1, TKL_RIGHT   , TKL_RIGHT   , so2,   0);
	link(sw1, TKL_LEFT    , TKL_LEFT    , sw2,   0);

	link( w0, TKL_LEFT    , TKL_BLUNT   , sw1, 340);
	link(sw1, TKL_STRAIGHT, TKL_STRAIGHT, so1, 534);
	link( o0, TKL_RIGHT   , TKL_BLUNT   , so1, 307);

	link( w0, TKL_STRAIGHT, TKL_BLUNT   , sw0, 357);
	link(sw0, TKL_STRAIGHT, TKL_BLUNT   , so0, 794-50);
	link(so0, TKL_STRAIGHT, TKL_STRAIGHT,  o0, 373+50);
	// 16 Knoten, 22 Linkelemente (nur die, mit Länge != 0)

	xct->setOrientation(TKO_CW); /* this runs recursive */

	int length = 0;
	qDebug() << "Have" << m_knots.size() << "tracks,";
	foreach(TrackKnot *tk, m_knots) {
		length += tk->getLength();
	}
	qDebug() << "covering" << length/100 << "m at scale. (Switch points only count once.)";
	return xct;
}

/* this slot is linked to the signal, when a switch is changed. */

void GeoPlan::sensePin(int pin) {
	for (int i=0; i<m_knots.count(); i++) {
		// sometime, replace for-loop by lut
		if (m_knots[i]->sensePin(pin)) {
			updatePlan();
			return;
		}
	}
}

/**********************************************************************************************************************/
/**    Callback for receiving indications, timeouts, releases, responses.
 *
 *  @param[in]    pRefCon       pointer to user context
 *  @param[in]    appHandle     application handle returned by tlc_openSession
 *  @param[in]    pMsg          pointer to received message information
 *  @param[in]    pData         pointer to received data
 *  @param[in]    dataSize      size of received data pointer to received data
 */
void GeoPlan::callReadLocs(void *pRefCon, TRDP_APP_SESSION_T appHandle, const TRDP_PD_INFO_T *pMsg, UINT8 *pData, UINT32 dataSize) {
	if (pRefCon && appHandle) {
		GeoPlan *gp = (GeoPlan *)pRefCon;
		gp->readLocs(pMsg, pData, dataSize);
	}
}

void GeoPlan::callReadPosi(void *pRefCon, TRDP_APP_SESSION_T appHandle, const TRDP_PD_INFO_T *pMsg, UINT8 *pData, UINT32 dataSize) {
	if (pRefCon && appHandle) {
		GeoPlan *gp = (GeoPlan *)pRefCon;
		gp->readPosi(pMsg, pData, dataSize);
	}
}

void GeoPlan::callReadTime(void *pRefCon, TRDP_APP_SESSION_T appHandle, const TRDP_PD_INFO_T *pMsg __unused, UINT8 *pData __unused, UINT32 dataSize __unused) {
	if (pRefCon && appHandle) {
		GeoPlan *gp __unused = (GeoPlan *)pRefCon;

	}
}

void GeoPlan::cerr(const char *lead, const char *str, int nl __unused) {
	qDebug() << lead << str; // cannot suppress the NL
}

void GeoPlan::updatePlan() {
	QByteArray out;

	if (dumpArray || trdp.up()) {
		out.reserve(defBufSiz);
		int size = m_knots.count();
		if (netByteOrder) size = htonl(size);
		out.append((char *)&size, 4);
		for (int i=0; i<m_knots.count(); i++) m_knots[i]->serialize(&out, marshall, netByteOrder);
	}

	if (trdp.up() && m_knots.count() >0) {
		TRDP_ERR_T result;
		result = (trdpTrackPlanID < 0)
				? trdp.publish(1111, &trdpTrackPlanID, 1, (UINT8*)out.data(), out.size(), NULL)
				: trdp.setCom(trdpTrackPlanID, (UINT8*)out.data(), out.size());
		if (result != TRDP_NO_ERR) {
			qDebug() << "Now, update-State failed for" << trdp.lastError();
		}
	}

	/* debug code, dumps the stream for insight */
	if (dumpArray) {
		QString str = QString::number(out.size()) + "\n[";
		for(int i=0; i<out.size();i+=4) {
			str += QString::number((int)(
					(i<out.size())
					?((out[i]&0xFF)|((out[i+1]&0xFF)<<8)|((out[i+2]&0xFF)<<16)|((out[i+3]&0xFF)<<24))
					:0
					 )) + ",";
		}
		str += "]\n";
		qDebug() << str;
	}
}

/* connection timeout handler */

void GeoPlan::checkFreshness(void) {
	QMutableMapIterator<int, Train> t(trains);
	bool notifyAllGone = t.hasNext(); /* only potentially notify, if there is one left */
	bool change = false;
	QString title;

	while (t.hasNext()) {
		t.next();

		if (t.value().freshness.isValid() && t.value().freshness.hasExpired(trainTimeout_ms)) {
			if (t.value().pdInfoP.seqCount > 0)
				qDebug() << "Train" << t.value().id << "on" << t.value().raddr.toString() << ":TRDP(" << t.value().pdInfoP.comId << "@" << t.value().pdInfoP.seqCount << ") --> [ Timed out ]";


			for (int i=0; i<m_knots.count(); i++) m_knots[i]->reduceSections(t.value().id);
			t.value().freshness.invalidate();
			t.remove();
			change = true;
		} else {
			notifyAllGone = false;
			title.append("  ").append(QString::number(t.value().id)).append(":").append(t.value().raddr.toString());
		}
	}
	if (notifyAllGone) {
		if (active) {
			QString s = QString("[ Not Connected ]");
			qDebug() << s;
			active = false;
			emit isConnected(active, s);
		}
	} else {
		if (!active || change) {
			active = true;
			emit isConnected(active, title);
		}
	}


}

void GeoPlan::timerEvent(QTimerEvent *event) {
	static bool beginning = true;
	int64_t to = 0;
	if (event->timerId() == timerRefreshId) {
		killTimer(timerRefreshId); /* redundant ?? */

		if (beginning) {
			checkFreshness();
			beginning = false;
		}

		TRDP_ERR_T result = trdp.cycle( &to );
		if (result != TRDP_NO_ERR) {
			if (result == TRDP_NODATA_ERR) {
				beginning = true;
			} else {
				qDebug() << "Ooops, trdp-cycle said" << trdp.lastError();
				/* as much as this is probably fatal, however retry later */
				to = 250000; /* some arbitrary default to be overwritten */
			}
		}
		to /= 1000;
		if (!to) to++;
		timerRefreshId = startTimer(to);
	}
}

/* works for all input sources */
/* very likely fires GUI-update */
bool GeoPlan::parseSections(QByteArray in) {
	if (in.size() < 8) {
		qDebug() << "Size mismatch in received data package: " << in.size();
		return false;
	}

	struct trackDMI *trackDMI = (struct trackDMI *)in.constData();/* the whole struct should not be affected by alignments */
	tid_t TrainId      = netByteOrder ? ntohl(trackDMI->TrainId    ) : trackDMI->TrainId;
	UINT32 size_tracks = netByteOrder ? ntohl(trackDMI->size_tracks) : trackDMI->size_tracks;


	if (size_tracks != (unsigned)m_knots.count()) {
		qDebug() << "Size mismatch in expected track usage descriptions: " << size_tracks << " != " << m_knots.count();
		return false;
	}
	int blockSz = 4+ 4+ size_tracks * sizeof(sections);
	if (in.size() != blockSz) {
		qDebug() << "Size mismatch in received data package: " << in.size() << " != " << blockSz;
		return false;
	}

	//const rloc_t *rl = (const rloc_t *)buf;
	for (unsigned i=0; i<size_tracks; i++ /*rl += sections_len*/) {
		m_knots[i]->importSections(TrainId, &(trackDMI->tracks[i]), useCommFront); /* memory checking is done at the top */
	}
	return true;
}

void GeoPlan::readLocs(const TRDP_PD_INFO_T *info, UINT8 *indataL, UINT32 length) {
	QHostAddress sender(info->srcIpAddr);
	bool TO = info->resultCode == TRDP_TIMEOUT_ERR;
	if (TO) {
		/* the current design captures all data on one comID. Might have to pop up individual subscriptions per train for "the right way" */
		/* But activating that subscr. would need some kind of overlaid protocol. E.g., send a packet to knocking-comID x1, then you get a subscr. sink for the actual comID x2. */
		QString s;
		QTextStream(&s) << "TRDP signaled a timeout for " << sender.toString() << '@' << info->comId;
		qDebug() << s;
		isConnected(false, s);
		return;
	}
	if (length < 4) {
		qDebug() << "Bad short packet received on readLocs, size:" << length << "from" << sender.toString() << info->comId << info->msgType << trdp.getResultString(info->resultCode) << info->seqCount;
	} else {
		tid_t tid = netByteOrder ? (int32_t)ntohl(*(uint32_t *)indataL) : (*(int32_t *)indataL);
		if (tid != nil) {
			if (trains.contains(tid) || trains.size()<configuredTrains) { /* limit the size of the trains array */
				Train &t = trains[tid];
				if (!t.freshness.isValid() || t.freshness.hasExpired(trainTimeout_ms)) {
					qDebug() << "Train" << tid << "on" << sender.toString() << ":TRDP(" << info->comId << ") --> [" << length << "]";
				}

				if (!t.freshness.isValid() || t.freshness.hasExpired(trainTimeout_ms) || (t.raddr == sender)) {
					t.raddr = sender;
					t.pdInfoL = *info;
					t.id = tid;
					if (!parseSections(QByteArray::fromRawData((char *)indataL, length))) {
						t.pdInfoL.resultCode = TRDP_PACKET_ERR;
					}
					t.freshness.start();
				} else {
					qDebug() << sender.toString() << "tried to activate Train" << tid << "which is still active on" << t.raddr.toString();
				}
			} else {
				qDebug() << sender.toString() << "tried to activate Train" << tid << "but there is no space left. Reached" << configuredTrains;
			}
		}
	}
}

void GeoPlan::updatePosis() {
	QByteArray posis;
	int PL_wireSize = sizeof( struct protectionLocation )-1;  // TODO marshalling is OFF (always in callback approach), account for the packed wire-structure
	posis.reserve(4+ PL_wireSize*configuredTrains);

	int tsize = trains.size();

	if (netByteOrder) tsize = htonl(tsize);
	posis.append((char *)&tsize, 4);
	for (QMap<int, Train>::iterator t = trains.begin(); t != trains.end(); ++t)
		posis.append(t->protection);        /* simply concatenate all data */
	if (configuredTrains-trains.size() > 0) /* fill up the buffer to match expectations */
		posis.append(QByteArray( PL_wireSize * (configuredTrains-trains.size()), 0));

	TRDP_ERR_T result;
	result = (trdpAllPosiID < 0)
			? trdp.publish(1141, &trdpAllPosiID, 1, (UINT8*)posis.data(), posis.size(), NULL)
			: trdp.setCom(trdpAllPosiID, (UINT8*)posis.data(), posis.size());
	if (result != TRDP_NO_ERR) {
		qDebug() << "Now, update-Posis failed for " << trdp.lastError();
	}

}

void GeoPlan::readPosi(const TRDP_PD_INFO_T *info, UINT8 *indataP, UINT32 length) {
	QHostAddress sender(info->srcIpAddr);
	bool TO = info->resultCode == TRDP_TIMEOUT_ERR;
	if (TO) return;
	if (length < 44) {
		qDebug() << "Bad short packet received on readPosi, size:" << length << "from" << sender.toString() << info->comId << info->msgType << trdp.getResultString(info->resultCode) << info->seqCount;
	} else {
		tid_t tid = netByteOrder ? (int32_t)ntohl(*(uint32_t *)indataP) : (*(int32_t *)indataP);
		if (tid != nil) {
			if (trains.contains(tid) || trains.size()<configuredTrains) { /* limit the size of the trains array */
				Train &t = trains[tid];
				if (!t.freshness.isValid() || t.freshness.hasExpired(trainTimeout_ms)) {
					qDebug() << "Train" << tid << "on" << sender.toString() << ":TRDP(" << info->comId << ") --> [" << length << "]";
				}

				if (!t.freshness.isValid() || t.freshness.hasExpired(trainTimeout_ms) || (t.raddr == sender)) {
					t.raddr = sender;
					t.pdInfoP = *info;
					t.id = tid;
					t.protection = QByteArray::fromRawData((char *)indataP, length);
					updatePosis();
					t.freshness.start();
				} else {
					qDebug() << sender.toString() << "tried to position Train" << tid << "which is still active on" << t.raddr.toString();
				}
			} else {
				qDebug() << sender.toString() << "tried to activate Train" << tid << "but there is no space left. Reached" << configuredTrains;
			}
		}
	}
}
