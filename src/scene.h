#ifndef SCENE_H
#define SCENE_H

#include <QGraphicsScene>
#include <QPainter>

#include <QDebug>

class Scene : public QGraphicsScene
{
    Q_OBJECT

public:
    Scene( int raster = 50, QObject *parent = 0 );

    int  raster() const;
    void setRaster( int raster );

    bool gridVisible() const;
    void setGrid( bool on );

public slots:
    inline void showGrid() { setGrid(true);  }
    inline void hideGrid() { setGrid(false); }
    void toggleGrid();

protected:
    void drawBackground( QPainter* painter, const QRectF& rect );

private:
    int  m_raster;
    bool m_grid;
};

#endif // SCENE_H
