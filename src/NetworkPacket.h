#ifndef NETWORKPACKET_H
#define NETWORKPACKET_H

#include <QtCore>
#include <QtNetwork>
#include <QObject>
#include <stdlib.h>
#include <memory.h>

#ifdef WIN32
#include <WinSock.h>
#else
#include <arpa/inet.h>
#endif

#include "global_includes.h"

#define SERVER_NETWORK_TCP_PORT 55555
#define SERVER_NETWORK_HTTP_PORT 55556

struct QTrainData
{
	quint32 TrainID;
	quint32 UserID;
	quint32 vmax;
	quint32 TrainLength;
	quint8 TrainName[32];
	quint8 TrainPictureURL[192];
};

struct QUserData
{
	quint32 UserID;
	quint32 TrainID;
	quint8 UserName[32];
};

Q_DECLARE_METATYPE(QTrainData)
Q_DECLARE_METATYPE(QUserData)

class CNetworkPacket : public QObject
{
        Q_OBJECT
public:
        CNetworkPacket(QObject *parent = NULL);
	CNetworkPacket(const CNetworkPacket&);
	~CNetworkPacket(void);
	CNetworkPacket& operator=(const CNetworkPacket&);
//Get-Set Header - Marshalling through this class
	quint16 GetVersion(void) const;
	void SetVersion(const quint16 version);
	quint32 GetSender(void) const;
	void SetSender(const quint32 sender);
	quint32 GetReceiver(void) const;
	void SetReceiver(const quint32 receiver);
	quint8 GetResponseFlag(void) const;
	void SetResponseFlag(const quint8 response);
	quint8 GetFragmentFlag(void) const;
	void SetFragmentFlag(const quint8 fragment);
        quint32 GetFragmentNumber(void) const;
        void SetFragmentNumber(const quint32 fragmentno);
	quint32 GetSegmentCount(void) const;
	void SetSegmentCount(const quint32 segmentcnt);
	quint32 GetLength(void) const;
	void SetLength(const quint32 length);
	quint32 GetPacketID(void) const;
	void SetPacketID(const quint32 packetid);
	quint32 GetTrainID(void) const;
	void SetTrainID(const quint32 trainid);
//Get-Set DATA - needs to be marshalled!
	void GetData(quint8 data[], const quint8 offset, const quint16 length) const;
	void GetData(quint8& data, const quint8 offset) const;
	void GetData(quint16& data, const quint8 offset) const;
	void GetData(quint32& data, const quint8 offset) const;
	void SetData(const quint8 data[], const quint8 offset, const quint16 length);
	void SetData(const quint8 data, const quint8 offset);
	void SetData(const quint16 data, const quint8 offset);
	void SetData(const quint32 data, const quint8 offset);
//Get-Packet / Set-Packet - for TCP access
	void SendPacket(QTcpSocket *socket) const;
	void RecvPacket(QTcpSocket *socket);
private:
	quint8 data[288]; //Array of marshalled data!
};

#endif
