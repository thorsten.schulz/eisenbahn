#include "trackconfig.h"

TrackConfig::TrackConfig(ActiveTrack* track, QGraphicsItem *parent) :
    QGraphicsObject(parent) {

    int pc = track->pins().count();

    m_bounds = QRectF(-220,-35-100*pc,310,70+200*pc);
    m_track = track;

    m_title = QString("Pin Setup");
    QGraphicsTextItem* titleItem = new QGraphicsTextItem(m_title, this);
    titleItem->setDefaultTextColor(Qt::white);
    titleItem->setFont(QFont("Arial", 20, QFont::Bold));
    titleItem->setPos(m_bounds.topLeft());

    QSize size = track->renderer()->defaultSize();
    m_mapcnt = new QSignalMapper(this);
    m_mapper = new QSignalMapper(this);
    m_pins << m_track->pins();
    qDebug() << m_pins.count() << QString::number(m_pins[0]/10) << QString::number(m_pins[0]%10);

    for(int i=0; i<pc; ++i) {
        QPixmap pixmap(QSize(150,150*size.height()/size.width()));
        pixmap.fill(Qt::transparent); ///IMPORTANT TO SUPPRESS RENDER ARTEFACTS!!!!!!
        QPainter painter(&pixmap);
        track->renderer()->render(&painter, "state"+QString::number(i));
        Button* rect = new Button(Button::NoType,160, this);
        rect->setHighlightColor(Qt::white);
        rect->setSelected(true);
        rect->setPos(m_bounds.topLeft()+QPointF(220,140+200*i));
        rect->setEnabled(false);

        QGraphicsPixmapItem *item = new QGraphicsPixmapItem(pixmap, this);
        item->setTransform(m_track->transform(), false);
        item->setPos(rect->pos()-item->mapToParent(item->boundingRect().center()));

        b_scrollup0   = new Button(Button::ScrollUp,      50, this);
        b_scrollup1   = new Button(Button::ScrollUp,      50, this);
        b_state0      = new TextButton(QString::number(m_pins[i]/10), 50, this);
        b_state1      = new TextButton(QString::number(m_pins[i]%10), 50, this);
        b_scrolldown0 = new Button(Button::ScrollDown, 50, this);
        b_scrolldown1 = new Button(Button::ScrollDown, 50, this);

        b_scrollup0->setPos(m_bounds.topLeft()+QPointF(35, 85+200*i));
        b_scrollup1->setPos(m_bounds.topLeft()+QPointF(87, 85+200*i));
        b_state0->setPos(m_bounds.topLeft()+QPointF(35, 140+200*i));
        b_state1->setPos(m_bounds.topLeft()+QPointF(87, 140+200*i));
        b_scrolldown0->setPos(m_bounds.topLeft()+QPointF(35, 195+200*i));
        b_scrolldown1->setPos(m_bounds.topLeft()+QPointF(87, 195+200*i));

        b_state0->setFontSize(15);
        b_state1->setFontSize(15);
        b_state1->setEnabled(false);
        b_state0->setEnabled(false);

        m_states << b_state0;
        m_states << b_state1;

        connect(b_scrollup0, SIGNAL(pressed()), m_mapcnt, SLOT(map()));
        m_mapcnt->setMapping(b_scrollup0, 10);
        connect(b_scrollup0, SIGNAL(pressed()), m_mapper, SLOT(map()));
        m_mapper->setMapping(b_scrollup0, i);
        connect(b_scrollup1, SIGNAL(pressed()), m_mapcnt, SLOT(map()));
        m_mapcnt->setMapping(b_scrollup1, 1);
        connect(b_scrollup1, SIGNAL(pressed()), m_mapper, SLOT(map()));
        m_mapper->setMapping(b_scrollup1, i);

        connect(b_scrolldown0, SIGNAL(pressed()), m_mapcnt, SLOT(map()));
        m_mapcnt->setMapping(b_scrolldown0, -10);
        connect(b_scrolldown0, SIGNAL(pressed()), m_mapper, SLOT(map()));
        m_mapper->setMapping(b_scrolldown0, i);
        connect(b_scrolldown1, SIGNAL(pressed()), m_mapcnt, SLOT(map()));
        m_mapcnt->setMapping(b_scrolldown1, -1);
        connect(b_scrolldown1, SIGNAL(pressed()), m_mapper, SLOT(map()));
        m_mapper->setMapping(b_scrolldown1, i);
    }

    connect(m_mapcnt, SIGNAL(mapped(int)),this, SLOT(setIncDec(int)));
    connect(m_mapper, SIGNAL(mapped(int)),this, SLOT(setPin(int)));

    b_delete = new Button(Button::Delete, 90, this);
    b_ok     = new Button(Button::Ok,     90, this);
    b_cancel = new Button(Button::Back,   90, this);
    connect(b_delete, SIGNAL(pressed()), this, SIGNAL(deleteItem()) );
    connect(b_ok,     SIGNAL(pressed()), this, SLOT(apply())        );
    connect(b_cancel, SIGNAL(pressed()), this, SIGNAL(close())      );

    b_delete->setPos(140,-95);
    b_ok->setPos(140,0);
    b_cancel->setPos(140,95);
    setAcceptHoverEvents(false);

    if(pc == 2) {
        if(m_pins[0] == m_pins[1])
            b_cancel->setEnabled(false);
        checkConflicts(2);
    }
}

void TrackConfig::setIncDec(int incdec) {
    m_incdec = incdec;
}

void TrackConfig::setPin(int num) {
    m_pins[num] = m_pins[num]+m_incdec+100;
    m_pins[num] %= 100;
    m_states[num*2]->setLabel(QString::number(m_pins[num]/10));
    m_states[num*2+1]->setLabel(QString::number(m_pins[num]%10));

    checkConflicts(num);
}

void TrackConfig::checkConflicts(int last) {
    if(m_pins.count()==2) {
        if(m_pins[0] == m_pins[1]) {
            b_ok->setEnabled(false);
            m_states[last*2]->setHighlightColor(Qt::red);
            m_states[last*2]->setSelected(true);
            m_states[last*2+1]->setHighlightColor(Qt::red);
            m_states[last*2+1]->setSelected(true);
        } else {
            m_states[0]->setSelected(false);
            m_states[1]->setSelected(false);
            m_states[2]->setSelected(false);
            m_states[3]->setSelected(false);
            b_ok->setEnabled(true);
        }
    }
}

void TrackConfig::apply() {
    m_track->setPins(m_pins);
    emit close();
}

QRectF TrackConfig::boundingRect() const {
    return m_bounds;
}

void TrackConfig::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setRenderHint(QPainter::Antialiasing);

    QRectF shadowrect(m_bounds.translated(2,2));
    QLinearGradient shadowgrad(shadowrect.topLeft(), shadowrect.bottomRight());
    shadowgrad.setColorAt(0, QColor(0x0,0x0,0x0,0x40));
    shadowgrad.setColorAt(1, QColor(0x0,0x0,0x0,0x40));
    painter->setPen(Qt::NoPen);
    painter->setBrush(shadowgrad);
    painter->drawRoundedRect(shadowrect, 3, 3, Qt::AbsoluteSize);

    QLinearGradient grad(m_bounds.topLeft(),m_bounds.bottomLeft());
    grad.setColorAt(0, QColor(0x60,0x60,0x60));
    grad.setColorAt(1, QColor(0x10,0x10,0x10));
    painter->setPen(Qt::NoPen);
    painter->setBrush(grad);
    painter->drawRoundedRect(m_bounds, 3, 3, Qt::AbsoluteSize);
}
