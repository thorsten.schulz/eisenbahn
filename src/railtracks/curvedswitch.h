#ifndef CURVEDSWITCH_H
#define CURVEDSWITCH_H

#include "activetrack.h"

class CurvedSwitch : public ActiveTrack
{
public:
    CurvedSwitch( QGraphicsItem* parent = 0 );
    int trackType() const;
    void init();
    void nextState();
    static QList<TrackNode*> nodesFromType( QGraphicsItem* parent = 0);

private:
    static QSvgRenderer* m_renderer;
};

#endif // CURVEDSWITCH_H
