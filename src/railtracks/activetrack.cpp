#include "activetrack.h"
#include "trackplan.h"


void ActiveTrack::setPinNumber(int count) {
    for(int i=0; i<count; ++i)
        m_pins << i;
}

ActiveTrack::ActiveTrack( QSvgRenderer* shared, QGraphicsItem* parent )
    : Railtrack(shared, parent) {
    m_time = 50;
}

bool ActiveTrack::isActivePart() const {
    return true;
}

QList<int> ActiveTrack::pins() const {
    return m_pins;
}

void ActiveTrack::setPins(QList<int> pins) {
    m_pins = pins;
}

int ActiveTrack::activationTime() const {
    return m_time;
}

void ActiveTrack::setActivationTime(int time) {
    //Limit Activation Time to 5000 seconds
    if(qAbs(time) > 5000)
        m_time = 5000;
    else
        m_time = time;
}

void ActiveTrack::init() {
    int pc = m_pins.count();
    //qDebug() << "Default Init - Set Activation Time" << pc;
    for(int i=0; i<pc; ++i)
        emit changeActivationTime(m_pins[i], m_time);
}

void ActiveTrack::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    Railtrack::mousePressEvent(event);
    if (static_cast<TrackPlan*>(parentItem())->mode() == TrackPlan::PlayMode) {
        if (isLocked()) // aka not locked
        	setRedFrame(true);
        else
        	nextState();
    }
}

void ActiveTrack::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    Railtrack::mouseReleaseEvent(event);
    if (static_cast<TrackPlan*>(parentItem())->mode() == TrackPlan::PlayMode) {
        if (isLocked()) // aka not locked
        	setRedFrame(false);
    }
}
