#ifndef ACTIVETRACK_H
#define ACTIVETRACK_H

#include "railtrack.h"

class ActiveTrack : public Railtrack
{
    Q_OBJECT
public:
    ActiveTrack( QSvgRenderer* shared, QGraphicsItem* parent = 0 );
    void setPinNumber(int count);
    bool isActivePart() const;

    int activationTime() const;
    void setActivationTime(int time);

    virtual void init();

    QList<int> pins() const;
    void setPins(QList<int> pins);

signals:
    void activatePin(int);
    void changeActivationTime(int pin, int time);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    virtual void nextState() = 0;
    QList<int> m_pins;
    int m_time;

};

#endif // ACTIVETRACK_H
