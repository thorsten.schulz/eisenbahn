#include "shortcurved.h"

QSvgRenderer* ShortCurved::m_renderer = new QSvgRenderer(QString(GFX_DIR "shortcurved.svg"));

ShortCurved::ShortCurved( QGraphicsItem* parent )
    : Railtrack(m_renderer, parent)
{
    m_nodes << nodesFromType(this);
    m_length = 125;
}

bool ShortCurved::isActivePart() const {
     return false;
}

int ShortCurved::trackType() const {
     return Rt_ShortCurved;
}

QList<TrackNode*> ShortCurved::nodesFromType( QGraphicsItem* parent) {
    QList<TrackNode*> nodes;
    nodes << new TrackNode(Node5, QPointF( 50, 50), parent) << \
             new TrackNode(Node4, QPointF(-50,-25), parent);
    return nodes;
}
