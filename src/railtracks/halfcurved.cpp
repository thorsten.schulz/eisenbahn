#include "halfcurved.h"

QSvgRenderer* HalfCurved::m_renderer = new QSvgRenderer(QString(GFX_DIR "halfcurved.svg"));

HalfCurved::HalfCurved( QGraphicsItem* parent )
    : Railtrack(m_renderer, parent)
{
    m_nodes << nodesFromType(this);
    m_length = 107;
}

bool HalfCurved::isActivePart() const {
     return false;
}

int HalfCurved::trackType() const {
     return Rt_HalfCurved;
}

QList<TrackNode*> HalfCurved::nodesFromType( QGraphicsItem* parent) {
    QList<TrackNode*> nodes;
    nodes << new TrackNode(Node2, QPointF( 50, 25), parent) << \
             new TrackNode(Node3, QPointF(-50,-12), parent);
    return nodes;
}
