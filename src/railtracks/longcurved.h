#ifndef LONGCURVED_H
#define LONGCURVED_H

#include "railtrack.h"

class LongCurved : public Railtrack
{
public:
    LongCurved( QGraphicsItem* parent = 0 );
    bool isActivePart() const;
    int trackType() const;
    static QList<TrackNode*> nodesFromType( QGraphicsItem* parent = 0);

private:
    static QSvgRenderer* m_renderer;
};

#endif // LONGCURVED_H
