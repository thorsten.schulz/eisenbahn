#ifndef SHORTANGLED_H
#define SHORTANGLED_H

#include "railtrack.h"

class ShortAngled : public Railtrack
{
public:
    ShortAngled( QGraphicsItem* parent = 0 );
    bool isActivePart() const;
    int trackType() const;
    static QList<TrackNode*> nodesFromType( QGraphicsItem* parent = 0);

private:
    static QSvgRenderer* m_renderer;
};

#endif // SHORTANGLED_H
