#ifndef DECOUPLE_H
#define DECOUPLE_H

#include "activetrack.h"
#include <QTimerEvent>

class Decouple : public ActiveTrack
{
public:
    Decouple( QGraphicsItem* parent = 0 );
    int trackType() const;
    void init();
    void nextState();
    static QList<TrackNode*> nodesFromType( QGraphicsItem* parent = 0);
protected:
    void timerEvent(QTimerEvent* event);

private:
    static QSvgRenderer* m_renderer;
};

#endif // DECOUPLE_H
