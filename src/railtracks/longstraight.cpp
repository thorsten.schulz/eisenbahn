#include "longstraight.h"

QSvgRenderer* LongStraight::m_renderer = new QSvgRenderer(QString(GFX_DIR "longstraight.svg"));

LongStraight::LongStraight( QGraphicsItem* parent )
    : Railtrack(m_renderer, parent)
{
    m_nodes << nodesFromType(this);
    m_length = 150;
}

bool LongStraight::isActivePart() const {
     return false;
}

int LongStraight::trackType() const {
     return Rt_LongStraight;
}

QList<TrackNode*> LongStraight::nodesFromType( QGraphicsItem* parent) {
    QList<TrackNode*> nodes;
    nodes << new TrackNode(Node0, QPointF(100,25), parent) << \
             new TrackNode(Node1, QPointF(-50,25), parent);
    return nodes;
}
