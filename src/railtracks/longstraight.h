#ifndef LONGSTRAIGHT_H
#define LONGSTRAIGHT_H

#include "railtrack.h"

class LongStraight : public Railtrack
{
public:
    LongStraight( QGraphicsItem* parent = 0 );
    bool isActivePart() const;
    int trackType() const;
    static QList<TrackNode*> nodesFromType( QGraphicsItem* parent = 0);

private:
    static QSvgRenderer* m_renderer;
};

#endif // LONGSTRAIGHT_H
