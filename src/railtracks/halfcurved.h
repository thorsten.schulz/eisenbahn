#ifndef HALFCURVED_H
#define HALFCURVED_H

#include "railtrack.h"

class HalfCurved : public Railtrack
{
public:
    HalfCurved( QGraphicsItem* parent = 0 );
    bool isActivePart() const;
    int trackType() const;
    static QList<TrackNode*> nodesFromType( QGraphicsItem* parent = 0);

private:
    static QSvgRenderer* m_renderer;
};

#endif // HALFCURVED_H
