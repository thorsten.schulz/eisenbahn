#include "switch3.h"

QSvgRenderer* Switch3::m_renderer = new QSvgRenderer(QString(GFX_DIR "switch3.svg"));

Switch3::Switch3( QGraphicsItem* parent )
    : ActiveTrack(m_renderer, parent)
{
    m_nodes << nodesFromType(this);
    m_length = 200;
    setPinNumber(3);	// TODO: 4, und dann in der Software einen Zustand verbieten
    setActivationTime(50);
}

int Switch3::trackType() const {
     return Rt_Switch3;
}

void Switch3::init() {
    //qDebug() << "Switch3 - Setting Initial State";
    ActiveTrack::init();
    setElementId(QString("state0"));
    emit activatePin(m_pins[0]);
}

void Switch3::nextState() {
    switch(m_current) {
    case 0: m_current = 1;
            setElementId(QString("state1"));
            emit activatePin(m_pins[1]);
            break;
    case 1: m_current = 0;
            setElementId(QString("state0"));
            emit activatePin(m_pins[0]);
            break;
	case 2: m_current = 2;
            setElementId(QString("state2"));
            emit activatePin(m_pins[2]);
		break;
    }
}

QList<TrackNode*> Switch3::nodesFromType( QGraphicsItem* parent) {
    QList<TrackNode*> nodes;
    nodes << new TrackNode(Node0,  QPointF( 100, 25), parent) << \
             new TrackNode(Node1,  QPointF(-100, 25), parent) << \
             new TrackNode(Node11, QPointF( 100,-25), parent) << \
             new TrackNode(Node2,  QPointF( 100, 75), parent);
    return nodes;
}
