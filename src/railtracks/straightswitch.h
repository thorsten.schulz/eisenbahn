#ifndef STRAIGHTSWITCH_H
#define STRAIGHTSWITCH_H

#include "activetrack.h"

class StraightSwitch : public ActiveTrack
{
public:
    StraightSwitch( QGraphicsItem* parent = 0 );
    int trackType() const;
    void init();
    void nextState();
    static QList<TrackNode*> nodesFromType( QGraphicsItem* parent = 0);

private:
    static QSvgRenderer* m_renderer;
};

#endif // STRAIGHTSWITCH_H
