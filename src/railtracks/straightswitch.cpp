#include "straightswitch.h"

QSvgRenderer* StraightSwitch::m_renderer = new QSvgRenderer(QString(GFX_DIR "straightswitch.svg"));

StraightSwitch::StraightSwitch( QGraphicsItem* parent )
    : ActiveTrack(m_renderer, parent)
{
    m_nodes << nodesFromType(this);
    m_length = 200;
    setPinNumber(2);
    setActivationTime(50);
}

int StraightSwitch::trackType() const {
     return Rt_StraightSwitch;
}

void StraightSwitch::init() {
    //qDebug() << "StraightSwitch - Setting Initial State";
    ActiveTrack::init();
    setElementId(QString("state0"));
    emit activatePin(m_pins[0]);
}

void StraightSwitch::nextState() {
    switch(m_current) {
    case 0: m_current = 1;
            setElementId(QString("state1"));
            emit activatePin(m_pins[1]);
            break;
    case 1: m_current = 0;
            setElementId(QString("state0"));
            emit activatePin(m_pins[0]);
            break;
    default:
        break;
    }
}

QList<TrackNode*> StraightSwitch::nodesFromType( QGraphicsItem* parent) {
    QList<TrackNode*> nodes;
    nodes << new TrackNode(Node0, QPointF( 100,-25), parent) << \
             new TrackNode(Node1, QPointF(-100,-25), parent) << \
             new TrackNode(Node2, QPointF( 100, 25), parent);
    return nodes;
}
