#ifndef SHORTCURVED_H
#define SHORTCURVED_H

#include "railtrack.h"

class ShortCurved : public Railtrack
{
public:
    ShortCurved( QGraphicsItem* parent = 0 );
    bool isActivePart() const;
    int trackType() const;
    static QList<TrackNode*> nodesFromType( QGraphicsItem* parent = 0);

private:
    static QSvgRenderer* m_renderer;
};

#endif // SHORTCURVED_H
