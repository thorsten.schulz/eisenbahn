#include "shortangled.h"

QSvgRenderer* ShortAngled::m_renderer = new QSvgRenderer(QString(GFX_DIR "shortangled.svg"));

ShortAngled::ShortAngled( QGraphicsItem* parent )
    : Railtrack(m_renderer, parent)
{
    m_nodes << nodesFromType(this);
    m_length = 71;
}

bool ShortAngled::isActivePart() const {
     return false;
}

int ShortAngled::trackType() const {
     return Rt_ShortAngled;
}

QList<TrackNode*> ShortAngled::nodesFromType( QGraphicsItem* parent) {
    QList<TrackNode*> nodes;
    nodes << new TrackNode(Node5, QPointF( 50, 50), parent) << \
             new TrackNode(Node8, QPointF(  0,  0), parent);
    return nodes;
}
