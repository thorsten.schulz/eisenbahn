#include "longcurved.h"

QSvgRenderer* LongCurved::m_renderer = new QSvgRenderer(QString(GFX_DIR "longcurved.svg"));

LongCurved::LongCurved( QGraphicsItem* parent )
    : Railtrack(m_renderer, parent)
{
    m_nodes << nodesFromType(this);
    m_length = 206;
}

bool LongCurved::isActivePart() const {
     return false;
}

int LongCurved::trackType() const {
     return Rt_LongCurved;
}

QList<TrackNode*> LongCurved::nodesFromType( QGraphicsItem* parent) {
    QList<TrackNode*> nodes;
    nodes << new TrackNode(Node2, QPointF( 100, 25), parent) << \
             new TrackNode(Node1, QPointF(-100,-25), parent);
    return nodes;
}
