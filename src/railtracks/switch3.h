#ifndef SWITCH3_HPP__
#define SWITCH3_HPP__

#include "activetrack.h"

class Switch3 : public ActiveTrack
{
public:
    Switch3( QGraphicsItem* parent = 0 );
    int trackType() const;
    void init();
    void nextState();
    static QList<TrackNode*> nodesFromType( QGraphicsItem* parent = 0);

private:
    static QSvgRenderer* m_renderer;
};

#endif
