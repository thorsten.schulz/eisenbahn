#include "decouple.h"

QSvgRenderer* Decouple::m_renderer = new QSvgRenderer(QString(GFX_DIR "decouple.svg"));

Decouple::Decouple( QGraphicsItem* parent )
    : ActiveTrack(m_renderer, parent) {
    m_nodes << nodesFromType(this);
    m_length = 150;
    setPinNumber(1);
    //setActivationTime(5000);
    setActivationTime(50);
}

int Decouple::trackType() const {
     return Rt_Decouple;
}

void Decouple::init() {
    ActiveTrack::init();
    setElementId(QString("state0"));
}

void Decouple::nextState() {
    switch(m_current) {
    case 0: m_current = 1;
            setElementId(QString("state1"));
            emit activatePin(m_pins[0]);
            startTimer(m_time);
            break;
    default:
        break;
    }
}

void Decouple::timerEvent(QTimerEvent *event) {
    killTimer(event->timerId());
    m_current = 0;
    setElementId(QString("state0"));
}

QList<TrackNode*> Decouple::nodesFromType( QGraphicsItem* parent) {
    QList<TrackNode*> nodes;
    nodes << new TrackNode(Node0, QPointF(100,25), parent) << \
             new TrackNode(Node1, QPointF(-50,25), parent);
    return nodes;
}
