#include "shortstraight.h"

QSvgRenderer* ShortStraight::m_renderer = new QSvgRenderer(QString(GFX_DIR "shortstraight.svg"));

ShortStraight::ShortStraight( QGraphicsItem* parent )
    : Railtrack(m_renderer, parent)
{
    m_nodes << nodesFromType(this);
    m_length = 50;
}

bool ShortStraight::isActivePart() const {
     return false;
}

int ShortStraight::trackType() const {
     return Rt_ShortStraight;
}

QList<TrackNode*> ShortStraight::nodesFromType( QGraphicsItem* parent) {
    QList<TrackNode*> nodes;
    nodes << new TrackNode(Node0, QPointF(50,25), parent) << \
             new TrackNode(Node1, QPointF( 0,25), parent);
    return nodes;
}
