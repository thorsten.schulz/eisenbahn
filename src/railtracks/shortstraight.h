#ifndef SHORTSTRAIGHT_H
#define SHORTSTRAIGHT_H

#include "railtrack.h"

class ShortStraight : public Railtrack
{
public:
    ShortStraight( QGraphicsItem* parent = 0 );
    bool isActivePart() const;
    int trackType() const;
    static QList<TrackNode*> nodesFromType( QGraphicsItem* parent = 0);

private:
    static QSvgRenderer* m_renderer;
};

#endif // SHORTSTRAIGHT_H
