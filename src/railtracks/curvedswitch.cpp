#include "curvedswitch.h"

QSvgRenderer* CurvedSwitch::m_renderer = new QSvgRenderer(QString(GFX_DIR "curvedswitch.svg"));

CurvedSwitch::CurvedSwitch( QGraphicsItem* parent )
    : ActiveTrack(m_renderer, parent) {
    m_nodes << nodesFromType(this);
    m_length = 200;
    setPinNumber(2);
    setActivationTime(50);
}

int CurvedSwitch::trackType() const {
     return Rt_CurvedSwitch;
}

void CurvedSwitch::init() {
    //qDebug() << "CurvedSwitch - Setting Initial State";
    ActiveTrack::init();
    setElementId(QString("state0"));
    emit activatePin(m_pins[0]);
}

void CurvedSwitch::nextState() {
    switch(m_current) {
    case 0: m_current = 1;
            setElementId(QString("state1"));
            emit activatePin(m_pins[1]);
            break;
    case 1: m_current = 0;
            setElementId(QString("state0"));
            emit activatePin(m_pins[0]);
            break;
    default:
        break;
    }
}

QList<TrackNode*> CurvedSwitch::nodesFromType( QGraphicsItem* parent) {
    QList<TrackNode*> nodes;
    nodes << new TrackNode(Node7, QPointF( 100,-12), parent) << \
             new TrackNode(Node1, QPointF(-100,-25), parent) << \
             new TrackNode(Node2, QPointF( 100, 25), parent);
    return nodes;
}
