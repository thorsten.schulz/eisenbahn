#ifndef _HEADER_MEDIA_USB_
#define _HEADER_MEDIA_USB_


#include "crc.h"
#include <termios.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/select.h>


#define BAUD B9600

#define ESC 16
#define END 3
#define START 2

typedef enum States {IDLE, PRE_START, RUNNING, ESC_IN_STREAM, FINISHED} uartStates;

char *media_usbinit();
char *media_usbopen ( char *file_name, int *stream );
char *media_usbclose( int stream );
char *media_usbread ( int stream, unsigned char *msg, int *msg_len );
char *media_usbwrite( int stream, unsigned char *msg, int msg_len );
char *media_usbioctl( int stream, char *buffer, int len );
char *media_usbintfc( int* cnt, char **file_names );


#define USB	{ "USB", "", media_usbinit, media_usbopen, \
			  media_usbclose, media_usbread, \
                          media_usbwrite, media_usbioctl, media_usbintfc }

#endif //_HEADER_MEDIA_USB_
