#ifndef CRC_H
#define CRC_H

#define CRC16_INIT 0xFFFF
#define CRC16_POLY 0x1021

int calcCrc16(unsigned char* data, int len);

#endif //CRC_H
