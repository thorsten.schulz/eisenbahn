
/*
 * name: media_usb.c
 *
 * this module implements an USB-based communication for the
 * railroad project.
 *
 */

#include <stdio.h>
#include "../rail_kernel/media.h"
#include "media_usb.h"
#include "../rail_kernel/global.h"

static struct termios oldtio;

char *media_usbinit()
     {
        if ( sh_debug( DB_NETWORK ) )
            fprintf( sh_fp(), "media_usbinit\n" );
       //printf  ("INFO: INIT USB \n");
        return (char*)0;

     }

char *media_usbopen(  char *file_name, int *stream )
     {
           struct termios newtio;
           int fd;
           if((fd = open(file_name, O_RDWR))==-1)//open for read and write
           {
               return "Error opening port\n";
           }
           if (tcgetattr (fd, &oldtio) == -1)//read existing config
           {
               return "Error reading config\n";
           }

           //configure
           newtio.c_cflag = BAUD | CS8 | CREAD; //control flags | CREAD - enable receiver | CS8 - 8Bit
           newtio.c_iflag = IGNPAR;    //input control, ig. char with parity errors
           newtio.c_oflag = 0;         //output control
           newtio.c_lflag = 0;         //local modes
           newtio.c_cc[VMIN] = 1;
           newtio.c_cc[VTIME] = 0;


           //apply new config
           tcflush (fd, TCIFLUSH);
           tcsetattr (fd, TCSANOW, &newtio);

           *stream = fd;
           if ( sh_debug( DB_NETWORK ) )
               fprintf( sh_fp(), "media_usbopen: file='%s'\n", file_name );

           printf ("INFO: OPEN USB \n");
            return (char*)0;
     }

char *media_usbclose( int stream )
     {
       //reset old config
       tcflush (stream, TCIFLUSH);
       tcsetattr (stream, TCSANOW, &oldtio);
       close(stream);

       if ( sh_debug( DB_NETWORK ) )
           fprintf( sh_fp(), "media_usbclose\n" );
//       printf ("INFO: CLOSE USB \n");
        return (char*)0;
     }

char *media_usbread(  int stream, unsigned char *msg, int *msg_len )
     {
       unsigned char byte;
       int crc, crcCheck, length, numOfBytesReceived, buffersize;
       uartStates uartStatus;
       uartStatus = IDLE;
       crc = 0;
       unsigned char *buffer_wr_ptr;
       buffer_wr_ptr = msg;
       buffersize = *msg_len;

		while(uartStatus != FINISHED){
		    length=read(stream, &byte, 1); //reads 1 byte max
           if (length==1)
           {
               switch(uartStatus){
                   case IDLE:

                   numOfBytesReceived = 0;
                   //Zeiger zurücksetzen
                   buffer_wr_ptr = msg;
                   //ESC suchen
                   if (byte == ESC)
                       uartStatus = PRE_START;
                   break;

               case PRE_START:

                   if(byte == START)
                       uartStatus = RUNNING;
                   else
                       uartStatus = IDLE;
                   break;

               case RUNNING:

                   if(byte == ESC)
                       uartStatus = ESC_IN_STREAM;
                   else{
                       //zeiger am Ende des Puffers?
                       if(buffer_wr_ptr == (msg + buffersize))
                           uartStatus = IDLE;
                       else
                       {
                           numOfBytesReceived++;
                           *buffer_wr_ptr++ = byte;

//                               printf("byte - %d\n",byte);
                       }
                   }
                   break;

               case ESC_IN_STREAM:

                   if(byte == END){
                       crcCheck = calcCrc16((unsigned char*)msg, numOfBytesReceived-2);
                       crc = msg[numOfBytesReceived-2];
                       crc = crc << 8;
                       crc |= msg[numOfBytesReceived-1];
                       if (crc == crcCheck){
                           *msg_len =  numOfBytesReceived-2;
                       }
                       else
                       {
                           printf("rcv CRC    : %X\n", crc);
                           printf("rcv CRCcalc: %X\n", crcCheck);
                           printf("rcv CRC Missmatch\n");
                       }
                       crcCheck = 0;
                       crc = 0;
                       uartStatus = FINISHED;
                   }
                   else{
                       if (byte == ESC){
                                       //zeiger am Ende des Puffers?
                           if(buffer_wr_ptr == (msg + buffersize))
                               uartStatus = IDLE;
                           else
                           {
                               *buffer_wr_ptr++ = byte;
                               numOfBytesReceived++;
                           }
                       }
                       uartStatus = RUNNING;
                   }
                   break;
			   case FINISHED:
				   // will break from the loop
				   break;
               }
           }
		}



       if ( sh_debug( DB_NETWORK ) )
       fprintf( sh_fp(), "media_usbread: buffer_size=%d\n", *msg_len );
//       printf ("INFO: READ USB \n");
        return (char*)0;
     }

char *media_usbwrite( int stream, unsigned char *msg, int msg_len )
     {
		//ESC START MSG (ESC ESC) CRC CRC ESC STOP
       int crc, i, size;
       crc = calcCrc16(msg,msg_len);
       //printf("msg_len: %d\t strlen: %d\n", msg_len, );
       unsigned char tmp;

       //ESC
       tmp = ESC;
       if((size = write((int)stream, &tmp , 1)) != 1)
       {
           printf("Error Writing\n");
           return (char*)-1;
       }
//       printf("\nESC - %X",tmp);

       //STX
       tmp = START;
       if((size = write((int)stream, &tmp , 1)) != 1)
       {
           printf("Error Writing\n");
           return (char*)-1;
       }
//       printf("\nSTX - %X",tmp);

       //MSG
       for(i=0; i< msg_len; i++){
//           printf("\nMSG - %X", msg[i]);
           if(msg[i] == ESC){
                   if((size = write((int)stream, &msg[i] , 1)) != 1)
                   {
                       printf("Error Writing\n");
                       return (char*)-1;
                   }
                   if((size = write((int)stream, &msg[i] , 1)) != 1)
                   {
                       printf("Error Writing\n");
                       return (char*)-1;
                   }
           }
           else{
                   if((size = write((int)stream, &msg[i] , 1)) != 1)
                   {
                       printf("Error Writing\n");
                       return (char*)-1;
                   }
           }
       }

       //CRC
       tmp = (crc>>8) & 0xff;
       if (tmp == ESC){
           if((size = write((int)stream, &tmp , 1)) != 1)
           {
               printf("Error Writing\n");
               return (char*)-1;
           }
           if((size = write((int)stream, &tmp , 1)) != 1)
           {
               printf("Error Writing\n");
               return (char*)-1;
           }
       }
       else {
           if((size = write((int)stream, &tmp , 1)) != 1)
           {
               printf("Error Writing\n");
               return (char*)-1;
           }
       }
//       printf("\nCRC1 - %X",tmp);

       tmp = crc & 0xff;
       if (tmp == ESC){
           if((size = write((int)stream, &tmp , 1)) != 1)
           {
               printf("Error Writing\n");
               return (char*)-1;
           }
           if((size = write((int)stream, &tmp , 1)) != 1)
           {
               printf("Error Writing\n");
               return (char*)-1;
           }
       }
       else {
           if((size = write((int)stream, &tmp , 1)) != 1)
           {
               printf("Error Writing\n");
               return (char*)-1;
           }
       }
//       printf("\nCRC2 - %X",tmp);

       //ESC
       tmp = ESC;
       if((size = write((int)stream, &tmp , 1)) != 1)
       {
           printf("Error Writing\n");
           return (char*)-1;
       }
//       printf("\nESC - %X",tmp);

       //END
       tmp = END;
       if((size = write((int)stream, &tmp , 1)) != 1)
       {
           printf("Error Writing\n");
           return (char*)-1;
       }

//       printf("\nETX - %X\n",tmp);

       if ( sh_debug( DB_NETWORK ) )
       fprintf( sh_fp(), "media_usbwrite: message length=%d\n", msg_len );
      printf ("INFO: WRITE USB success \n");
        return NULL;
     }

char *media_usbioctl( int stream, char *buffer, int len )
     {
       len = len;
       buffer = buffer;
       stream = stream;
       if ( sh_debug( DB_NETWORK ) )
       fprintf( sh_fp(), "media_usbioctl\n" );
        return NULL;
     }

char *media_usbintfc( int* cnt, char **file_names ) {
    int icnt=0;

    *cnt=0;
    for(icnt=0; icnt<1; ++icnt) {
            file_names[(*cnt)] = (char *)malloc(sizeof(char)*sizeof("/dev/ttyUSB0"));
            sprintf(file_names[(*cnt)++], "/dev/ttyUSB%i", icnt);
    }

    return NULL;
}



