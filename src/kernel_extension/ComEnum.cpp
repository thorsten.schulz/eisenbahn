//*****************************************************************************
//*
//*
//*		ComEnum.cpp
//*
//*
//*****************************************************************************
//
//	(C) Copyright Anton Zechner 2007
//
//	Diese Programm listet alle vorhandenen COM-Ports auf.
//
#include	"StdAfx.h"
#include	"ComEnumPorts.h"

#include	<conio.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>



//*****************************************************************************
//*
//*		main
//*
//*****************************************************************************
int main(int argc, char* argv[])
{
int			i,iCount;
ComInfo	   *pInfo;



		iCount = ComEnumPorts(NULL,0);
	if(!iCount)
		{
		printf("\n\nNo Ports found.\n\n");
		getch();
		return -1;
		}


	pInfo  = new ComInfo[iCount];
	iCount = ComEnumPorts(pInfo,iCount);

	for(i=0;i<iCount;i++)
		{
		printf("\nPort %i",i);
		printf("\n\tPort = %s",pInfo[i].cPortName);
		printf("\n\tName = %s",pInfo[i].cFriendlyName);
		printf("\n\tPath = %s",pInfo[i].cDevPath);
		printf("\n\tDesk = %s",pInfo[i].cPortDesc);
		printf("\n\tUSB  = %i",pInfo[i].bUsbDevice);
		printf("\n");
		}

	delete pInfo;

	getch();


return 0;
}
