//*****************************************************************************
//*
//*
//*		ComDemo.cpp
//*
//*
//*****************************************************************************
//	
//	Mit deisem Programm kann man eine Konsolenverbindung �ber die serielle 
//	Schnittstelle mit einem Null-Modem-Kabel herstellen.
//
#include <conio.h>
#include <stdio.h>
#include <windows.h>
#include "ComTools.h"


#define 	COM1		0
#define 	COM2		1
#define 	COM3		2
#define 	COM4		3

#define 	ESC			27


//*****************************************************************************
//*
//*		main
//*
//*****************************************************************************
int main(int argc, char* argv[])
{
int		iLen,iKey;	
char	cBuffer[65];

	ComInit();

	if(!ComOpen(COM1,9600,P_NONE,S_1BIT,D_8BIT))
		{
		printf("\nKann die Schnittstelle nich oeffnen !\n\n");
		return -1;
		}	

	printf("\nMit Null-Modem-Kabel an COM1 beide Rechner verbindnen.");
	printf("\nAbbruch mit ESC. \n\n");

	for(;;) 
		{
		if(kbhit())							// Wurde eine Taste gedr�ckt
			{
			   iKey=getch();
			if(iKey==ESC)break;
			ComWrite(COM1,iKey);
			}				
		
		   iLen=ComRead(COM1,cBuffer,64);	// Zeichen einlesen
		if(iLen>0)
			{
			cBuffer[iLen]=0;
			printf("%s",cBuffer);
			}
		else{
			Sleep(50);						// Wenn keine Zeichen im Buffer sind dann warten
			}
		}		
	
	ComClose(COM1);
	ComExit();

return 0;
}
