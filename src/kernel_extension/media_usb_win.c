
/*
 * name: media_usb.c
 *
 * this module implements an USB-based communication for the
 * railroad project.
 *
 */

#include "../rail_kernel/media.h"
#include "media_usb.h"
#include "../rail_kernel/global.h"



char *media_usbinit()
     {
        if ( sh_debug( DB_NETWORK ) )
            fprintf( sh_fp(), "media_usbinit\n" );
//        printf  ("INFO: INIT USB \n");
        return (char*)0;

     }

char *media_usbopen( char* file_name, void **stream )
     {
    // *file_name = COM0 for COM1, COM1 for COM2 etc.
    int com = (unsigned)atoi(file_name+3)-1;
    int ret = ComOpen (com,BAUD,0,0,8) -1;
    *stream = (void*)com;

//            int fd;
//            if((fd = open(file_name, O_RDWR))==-1)//open for read and write
//            {
//                return "Error opening port\n";
//            }
//            if (tcgetattr (fd, &oldtio) == -1)//read existing config
//            {
//                return "Error reading config\n";
//            }

//            //configure
//            newtio.c_cflag = BAUD | CS8 | CREAD; //control flags | CREAD - enable receiver | CS8 - 8Bit
//            newtio.c_iflag = IGNPAR;    //input control, ig. char with parity errors
//            newtio.c_oflag = 0;         //output control
//            newtio.c_lflag = 0;         //local modes
//            newtio.c_cc[VMIN] = 1;
//            newtio.c_cc[VTIME] = 0;


//            //apply new config
//            tcflush (fd, TCIFLUSH);
//            tcsetattr (fd, TCSANOW, &newtio);

//            *stream = fd;

//            if ( sh_debug( DB_NETWORK ) )
//                fprintf( sh_fp(), "media_usbopen: file='%s'\n", file_name );

////            printf ("INFO: OPEN USB \n");
            return (char*)ret;
     }

char *media_usbclose( void *stream )
     {
//        //reset old config
//        tcflush (stream, TCIFLUSH);
//        tcsetattr (stream, TCSANOW, &oldtio);
//        close(stream);

//        if ( sh_debug( DB_NETWORK ) )
//            fprintf( sh_fp(), "media_usbclose\n" );
////        printf ("INFO: CLOSE USB \n");

        int ret = ComClose((unsigned)stream) -1;
        return (char*)ret;
     }

char *media_usbread(  void *stream, unsigned char *msg, int *msg_len )
     {
    int crc, crcCheck, length = 0;
    unsigned char byte;
    uartStates uartStatus = IDLE;
    int size = 0;

    while(uartStatus != FINISHED) {
        switch(uartStatus){
        case IDLE:
            ComRead((unsigned)stream, &byte, 1);
            if(byte == ESC)
                uartStatus = PRE_START;
            else return "ERROR: IDLE";
            break;
        case PRE_START:
            ComRead((unsigned)stream, &byte, 1);
            if(byte == START)
                uartStatus = RUNNING;
            else return "ERROR: STX";
            break;
        case RUNNING:
            ComRead((unsigned)stream, &byte, 1);
            if(byte == ESC)
                uartStatus = ESC_IN_STREAM;
            else
                msg[size++] = byte;
            break;
        case ESC_IN_STREAM:
            ComRead((unsigned)stream, &byte, 1);
            if(byte == END) {
                crcCheck = calcCrc16((unsigned char*)msg, size-2);
                crc = (msg[size-2]<<8)|msg[size-1];
                if (crc == crcCheck) {
                    *msg_len =  size-2;
                    uartStatus = FINISHED;
                }
                else return "ERROR: CRC";
            } else {
                if (byte == ESC) {
                    msg[size++] = byte;
                    uartStatus = RUNNING;
                } else return "ERROR: ESC";
            }
            break;
        case FINISHED:
        default:
            break;
        }
    }



    if ( sh_debug( DB_NETWORK ) )
    fprintf( sh_fp(), "media_usbread: buffer_size=%d\n", *msg_len );
//        printf ("INFO: READ USB \n");
    return (char*)0;

////        fd_set fds;
//        int crc, crcCheck, length = 0;
//        unsigned char buf[255];
//        int size = 0;

//        //while(!ComGetReadCount((unsigned)stream)) {; }

//        length = ComRead((int)stream, buf, 255);
//        if(length < 6)
//            return "ERROR: MSG TOO SHORT";

//        if(buf[0] != ESC  || buf[1] != START) {
//            return "ERROR: STX";
//        }
//        int i;
//        for(i=2;i<length;++i) {
//            if(buf[i] == ESC && buf[i+1] == END) {
//                crcCheck = calcCrc16((unsigned char*)msg, size-2);
//                crc = (msg[size-2]<<8)|msg[size-1];
//                if (crc == crcCheck)
//                    *msg_len =  size-2;
//                else
//                    return "ERROR: CRC";
//                break;
//            }
//            if(buf[i] == ESC && buf[i+1] == ESC)
//                i++;
//            msg[size++] = buf[i];
//        }


//        if ( sh_debug( DB_NETWORK ) )
//        fprintf( sh_fp(), "media_usbread: buffer_size=%d\n", *msg_len );
////        printf ("INFO: READ USB \n");
//        return (char*)0;
     }

char *media_usbwrite( void *stream, unsigned char *msg, int msg_len )
     {
		//ESC START MSG (ESC ESC) CRC CRC ESC STOP
        int crc, i;
        crc = calcCrc16(msg,msg_len);

        unsigned char tmp;
        unsigned char buf[255];

        int size = 2;
        buf[0] = ESC;
        buf[1] = START;
        for(i=0; i<msg_len; ++i) {
            buf[size++] = msg[i];
            if(msg[i] == ESC)
                buf[size++] = ESC;
        }
        tmp = (crc>>8)&0xff;
        buf[size++] = tmp;
        if(tmp == ESC)
            buf[size++] = ESC;
        tmp = crc&0xff;
        buf[size++] = tmp;
        if(tmp == ESC)
            buf[size++] = ESC;
        buf[size++] = ESC;
        buf[size++] = END;

        int checkSize = ComWrite((unsigned)stream, &buf, size);
        while(ComGetWriteCount((unsigned)stream)) {;}
        if(checkSize != size) {
            printf("Error Writing\n");
            return (char*)checkSize;
        }

        if ( sh_debug( DB_NETWORK ) )
        fprintf( sh_fp(), "media_usbwrite: message length=%d\n", msg_len );
//        printf ("INFO: WRITE USB \n");
        return (char*)0;
     }

char *media_usbioctl( void *stream, char *buffer, int len )
     {
//        len = len;
//        buffer = buffer;
//        stream = stream;
//        if ( sh_debug( DB_NETWORK ) )
//        fprintf( sh_fp(), "media_usbioctl\n" );
        return (char*)0;
     }

char *media_usbintfc( int* cnt, char **file_names ) {
    int icnt=0;
    int pMode[MAX_INTFC];
    ComDetectPorts(&icnt,pMode,MAX_INTFC);

    *cnt=0;
    for(icnt=0; icnt<MAX_INTFC; ++icnt) {
        if(pMode[icnt]==1) {
            //never forget the termination!!!!!!
            file_names[(*cnt)] = (char *)malloc(sizeof(char)*sizeof("COMXX"));
            sprintf(file_names[(*cnt)++], "COM%i", icnt+1);
            //file_names[(*cnt)++] = (int)icnt;
        }
    }

    return (char*)0;
}


