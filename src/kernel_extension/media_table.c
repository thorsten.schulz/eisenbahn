#include "media_table.h"
#include "media_usb.h"

static MEDIA_ENTRY media_table[] = {
                                     USB,
                                   };


MED_PTR media_getentry( int entry )
{
    return (entry>=0 && entry < (int)TAB_SIZE)? media_table + entry: 0;
}

char *media_getinterfaces( int entry, int* cnt, char** file_names ) {
    MED_PTR mp = media_getentry(entry);
    return (mp) ? (*mp->med_intfc)(cnt, file_names) : 0;
}
