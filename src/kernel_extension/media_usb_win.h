#ifndef _HEADER_MEDIA_USB_
#define _HEADER_MEDIA_USB_


#include "crc.h"
//#include <termios.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//#include <sys/select.h>
#include "ComTools.h"


//#define BAUD 115200
#define BAUD 9600

#define ESC 16
#define END 3
#define START 2

//struct termios oldtio, newtio;

typedef enum States {IDLE, PRE_START, RUNNING, ESC_IN_STREAM, FINISHED} uartStates;

uartStates uartStatus;

char *media_usbinit();
char *media_usbopen ( char* file_name, void **stream );
char *media_usbclose( void *stream );
char *media_usbread ( void *stream, unsigned char *msg, int *msg_len );
char *media_usbwrite( void *stream, unsigned char *msg, int msg_len );
char *media_usbioctl( void *stream, char *buffer, int len );
char *media_usbintfc( int* cnt, char **file_names );


#define MEDIA_USB	{ "RS232", "", media_usbinit, media_usbopen, \
                          media_usbclose, media_usbread, \
                          media_usbwrite, media_usbioctl, media_usbintfc }

#endif //_HEADER_MEDIA_USB_
