#include "crc.h"

int calcCrc16(unsigned char* data, int len)
{
	int i, crcReg;
	for (i = 0, crcReg = CRC16_INIT; i < len; i++)
	{
		int crcData = data[i], j;
		for (j = 0; j < 8; j++)
		{
			if ( ((crcReg&0x8000)>>8) ^ (crcData&0x80) )
				crcReg=(crcReg<<1)^CRC16_POLY;
			else
				crcReg=(crcReg<<1);
			crcData<<=1;
		}
	}

        // ensure 16 bit boundary ...
	return crcReg&0xffff;
}
