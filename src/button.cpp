#include "button.h"
#include <cmath>

Button::Button(ButtonType type, qreal size, QGraphicsItem *parent)
    : QGraphicsObject(parent) {
    //TYPE
    m_buttontype = type;

    //SIZE
    m_size = size;
    m_bounds = QRectF(QPointF(),QSizeF(size,size));
    m_bounds.moveCenter(QPointF());

    //COLOR
    m_color = Qt::white;
    m_hcolor = QColor(0xdc,0x16,0x00);
    m_gradient.setStart(m_bounds.topLeft());
    m_gradient.setFinalStop(m_bounds.bottomLeft());
    m_gradient.setColorAt(0, QColor(0x60,0x60,0x60) );
    m_gradient.setColorAt(1, QColor(0x10,0x10,0x10) );

    //STATE
    m_pressed = false;
    m_selected = false;

    setCacheMode(QGraphicsItem::DeviceCoordinateCache);
}

Button::Button(ButtonType type, qreal width, qreal height, QGraphicsItem *parent)
    : QGraphicsObject(parent) {
    //TYPE
    m_buttontype = type;

    //SIZE
    m_size = qMin(width, height);
    m_bounds = QRectF(QPointF(),QSizeF(width,height));
    m_bounds.moveCenter(QPointF());

    //COLOR
    m_color = Qt::white;
    m_hcolor = QColor(0xdc,0x16,0x00);
    m_gradient.setStart(m_bounds.topLeft());
    m_gradient.setFinalStop(m_bounds.bottomLeft());
    m_gradient.setColorAt(0, QColor(0x60,0x60,0x60) );
    m_gradient.setColorAt(1, QColor(0x10,0x10,0x10) );

    //STATE
    m_pressed = false;
    m_selected = false;

    setCacheMode(QGraphicsItem::DeviceCoordinateCache);
}

QRectF Button::boundingRect() const
{ return m_bounds.adjusted(0, 0, 2, 2); }

QPainterPath Button::shape() const {
    QPainterPath path;
    path.addRoundedRect(m_bounds, 3, 3);
    return path;
}

void Button::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget* widget) {
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);

    bool highlighted = m_pressed || m_selected;
    QColor color = m_color;

    QTransform transform = painter->transform();
    //shadow
    QRectF shadowrect(m_bounds.translated(2,2));
    QLinearGradient shadowgrad(shadowrect.topLeft(), shadowrect.bottomRight());
    shadowgrad.setColorAt(0, QColor(0x0,0x0,0x0,0x40));
    shadowgrad.setColorAt(1, QColor(0x0,0x0,0x0,0x40));
    painter->setPen(Qt::NoPen);
    painter->setBrush(shadowgrad);
    painter->drawRoundedRect(shadowrect, 3, 3, Qt::AbsoluteSize);

    QLinearGradient grad(m_bounds.topLeft(),m_bounds.bottomLeft());
    grad.setColorAt(0, highlighted ? m_hcolor : QColor(0x60,0x60,0x60));
    grad.setColorAt(1, highlighted ? m_hcolor : QColor(0x10,0x10,0x10  ));
    painter->setPen(Qt::NoPen);
    painter->setBrush(grad);
    painter->drawRoundedRect(m_bounds, 3, 3, Qt::AbsoluteSize);

    qreal scale_factor = m_size/100.0;
    painter->scale(scale_factor, scale_factor);

    QPen pen(color);
    pen.setWidth(6);
    painter->setPen(pen);
    painter->setBrush(Qt::NoBrush);

    if(m_pressed)
        painter->scale(1.05, 1.05);

    switch(m_buttontype) {
        case SaveTrack: {
            pen.setWidth(4);
            painter->setPen(pen);
            QPolygonF polygon;
            polygon << QPointF(30,-30) << QPointF(30,30) << QPointF(-30,30) << QPointF(-30,-22) << QPointF(-22,-30);
            painter->drawPolygon(polygon);
            painter->drawRect(QRectF(-15,-29,30,15));
            painter->setBrush(color);
            painter->drawRect(QRectF(-5,-29,20,15));
            painter->drawRect(QRectF(-20,0,40,20));
            break;
        }
        case Refresh:
        case SwitchNode:
        case RotateTrack: {
            painter->setBrush(color);
            if(m_buttontype == SwitchNode)
                painter->drawEllipse(-5,-5,10,10);
            QRectF rect(-30,-30,60,60);
            painter->drawArc(rect,0, 90*16);
            painter->drawArc(rect,180*16,90*16);
            painter->setPen(Qt::NoPen);
            QPolygonF polygon;
            polygon << QPointF(0,-8) << QPointF(0,8) << QPointF(-12,0);
            polygon.translate(0,-30);
            painter->drawPolygon(polygon);
            painter->rotate(180);
            painter->drawPolygon(polygon);
            if(m_buttontype == RotateTrack) {
                painter->scale(0.8,0.8);
                pen.setWidth(2);
                painter->setPen(pen);
                painter->drawLine(-25,-10,25,-10);
                painter->drawLine(-25,10,25,10);
                pen.setWidth(4);
                painter->setPen(pen);
                for (int i = 0; i < 5; ++i) {
                    painter->drawLine(i*10-20,-16, i*10-20, 16);
                }
            }
            break;
        }
        case ZoomIn:
        case ZoomOut: {
            painter->translate(-10,-10);
            painter->drawLine(QPointF(-10,0),QPointF(10,0));
            if (m_buttontype == ZoomIn) {
                painter->drawLine(QPointF(0,-10),QPointF(0,10));
            }
            painter->drawEllipse(QPointF(0,0),25,25);
            pen.setWidth(9);
            painter->setPen(pen);
            painter->drawLine(QPoint(21,21),QPoint(36,36));
            break;
        }
        case Config: {
            for(int i = 0; i < 2; ++i) {
                painter->translate(-15+i*45,-15+i*45);
                pen.setWidth(5);
                painter->setPen(pen);
                painter->drawEllipse(QPointF(0,0),15,15);
                pen.setWidth(7);
                painter->setPen(pen);
                for (int j = 0; j < 360; j+=45) {
                    qreal x2 = cos((j+i*22.5)*M_PI/180.0);
                    qreal y2 = sin((j+i*22.5)*M_PI/180.0);
                    painter->drawLine(QPointF(x2*18, y2*18), QPointF(x2*19, y2*19));
                }
            }
            break;
        }
        case AddTrack: {
            painter->scale(1.1,1.1);
            pen.setWidth(2);
            painter->setPen(pen);
            painter->translate(-30,-15);
            painter->drawLine(0,0,50,0);
            painter->drawLine(0,20,50,20);
            pen.setWidth(4);
            painter->setPen(pen);
            for (int i = 0; i < 5; ++i) {
                painter->drawLine(i*10+5,-6, i*10+5, 26);
            }
            painter->translate(55,40);
            painter->drawLine(-7,0,7,0);
            painter->drawLine(0,-7,0,7);
            break;
        }
        case RotateLeft:
        case RotateRight: {
            QRectF rect(-30,-30,60,60);
            painter->setFont(QFont("Arial", 20, QFont::Bold));
            painter->drawText(rect.adjusted(12,12,-8,-8), Qt::AlignCenter, "90�");
            if (m_buttontype == RotateRight) {
                painter->scale(-1,1);
            }
            painter->drawArc(rect,270*16, 270*16);
            QPolygonF polygon;
            polygon << QPointF(-8,0) << QPointF(8,0) << QPointF(0,12) << QPointF(-8,0);
            polygon.translate(-30,0);
            painter->setPen(Qt::NoPen);
            painter->setBrush(color);
            painter->drawPolygon(polygon);
            break;
        }
        case MirrorVertical:
        case MirrorHorizontal: {
            pen.setWidth(Qt::NoPen);
            painter->setPen(pen);
            painter->setBrush(color);
            QPolygonF polygon;
            if (m_buttontype == MirrorVertical) {
                painter->rotate(90);
            }
            polygon << QPointF(-10,30) << QPointF(-10,-30) << QPointF(-35,30);
            painter->drawPolygon(polygon);
            painter->scale(-1,1);
            painter->drawPolygon(polygon);
            QLineF line(0,-40,0,40);
            pen.setWidth(4);
            pen.setStyle(Qt::DotLine);
            painter->setPen(pen);
            painter->drawLine(line);
            break;
        }
        case ScrollUp:
        case ScrollDown:
        case ScrollLeft:
        case ScrollRight: {
            pen.setWidth(Qt::NoPen);
            painter->setPen(pen);
            painter->setBrush(color);
            QPolygonF polygon;

            if      (m_buttontype == ScrollUp)    { painter->scale(1,-1); }
            else if (m_buttontype == ScrollLeft)  { painter->rotate( 90); }
            else if (m_buttontype == ScrollRight) { painter->rotate(-90); }

            polygon << QPointF(0,15) << QPointF(25,-15) << QPointF(35,-15) << \
                       QPointF(0,25) << QPointF(-35,-15) << QPointF(-25,-15);
            painter->drawPolygon(polygon);
            break;
        }
        case Delete: {
            pen.setWidth(11);
            painter->setPen(pen);
            QLineF line(-25,-25,25,25);
            painter->drawLine(line);
            painter->scale(-1,1);
            painter->drawLine(line);
            pen.setColor(highlighted ? m_color : m_hcolor);
            pen.setWidth(9);
            painter->setPen(pen);
            painter->drawLine(line);
            painter->scale(-1,1);
            painter->drawLine(line);
            break;
        }
        case BoardConfig:
        case AddBoard:{
            pen.setWidth(4);
            painter->setPen(pen);
            painter->drawRoundedRect(QRectF(-30,-30,60,60),5,5,Qt::AbsoluteSize);
            for(int i = 0; i < 5; ++i) {
                QLineF line(-20+i*10,-32,-20+i*10,-40);
                QLineF line2(-20+i*10,32,-20+i*10,40);
                QLineF line3(-32,-20+i*10,-40,-20+i*10);
                QLineF line4(32,-20+i*10,40,-20+i*10);
                painter->drawLine(line);
                painter->drawLine(line2);
                painter->drawLine(line3);
                painter->drawLine(line4);
            }
            if(m_buttontype == BoardConfig){
                painter->scale(0.7,0.7);
                for(int i = 0; i < 2; ++i) {
                    painter->translate(-15+i*45,-15+i*45);
                    pen.setWidth(5);
                    painter->setPen(pen);
                    painter->drawEllipse(QPointF(0,0),15,15);
                    pen.setWidth(7);
                    painter->setPen(pen);
                    for (int j = 0; j < 360; j+=45) {
                        qreal x2 = cos((j+i*22.5)*M_PI/180.0);
                        qreal y2 = sin((j+i*22.5)*M_PI/180.0);
                        painter->drawLine(QPointF(x2*18, y2*18), QPointF(x2*19, y2*19));
                    }
                }
            }
            else {
                pen.setWidth(10);
                painter->setPen(pen);
                painter->drawLine(-14,0,14,0);
                painter->drawLine(0,-14,0,14);
            }
            break;
        }
        case TrainConfig: {
            painter->translate(0,5);
            painter->setPen(Qt::NoPen);
            painter->setBrush(color);
            painter->drawRoundedRect(QRectF(-25,-20,50,40),5,5,Qt::AbsoluteSize);
            painter->drawRect(QRectF(-25,16,50,9));
            painter->setBrush(grad);
            painter->drawRoundedRect(QRectF(-18,-13,36,13),5,5,Qt::AbsoluteSize);
            painter->drawRect(QRectF(-18,-8,36,13));
            painter->setPen(color);
            pen.setWidth(4);
            painter->setPen(pen);
            painter->drawLine(-15,20,-15,32);
            painter->drawLine(15,20,15,32);
            QPolygonF polygon;
            polygon << QPointF(-10,-20) << QPointF(-15,-30) << QPointF(-10,-40) << QPointF(10,-40) << QPointF(15,-30) << QPointF(10,-20);
            painter->drawPolygon(polygon);
            painter->setPen(Qt::NoPen);
            painter->drawEllipse(-19,12,7,7);
            painter->drawEllipse(12,12,7,7);
            break;
        }
        case TrackConfig: {
            painter->scale(1.1,1.1);
            pen.setWidth(2);
            painter->setPen(pen);
            painter->translate(-35,-20);
            painter->drawLine(0,0,50,0);
            painter->drawLine(0,20,50,20);
            pen.setWidth(4);
            painter->setPen(pen);
            for (int i = 0; i < 5; ++i) {
                painter->drawLine(i*10+5,-6, i*10+5, 26);
            }
            painter->translate(58,40);

            painter->scale(0.7,0.7);
                pen.setWidth(5);
                painter->setPen(pen);
                painter->drawEllipse(QPointF(0,0),15,15);
                pen.setWidth(7);
                painter->setPen(pen);
                for (int j = 0; j < 360; j+=45) {
                    qreal x2 = cos((j+22.5)*M_PI/180.0);
                    qreal y2 = sin((j+22.5)*M_PI/180.0);
                    painter->drawLine(QPointF(x2*18, y2*18), QPointF(x2*19, y2*19));
                }
            break;
        }
        case Ok: {
            pen.setWidth(1);
            painter->setPen(pen);
            painter->setBrush(color);
            QPolygonF polygon;
            painter->setBrush(highlighted ? m_color : QColor(0,208,0));
            polygon << QPointF(-1,26) << QPointF(1,26) << QPointF(26,-34) << QPointF(35,-34) << \
                       QPointF(5,35) << QPointF(-4,35) << QPointF(-35,-4) << QPointF(-26,-4);
            painter->drawPolygon(polygon);
            break;
        }
        case Back: {
            painter->setPen(Qt::NoPen);
            painter->setBrush(m_color);
            QPolygonF polygon;
            polygon << QPointF(-30,0) << QPointF(0,-25) << QPointF(0,-10) << QPointF(30,-10) << \
                       QPointF(30,10) << QPointF(0,10) << QPointF(0,25);
            painter->drawPolygon(polygon);
            break;
        }
        case Add: {
            painter->setBrush(Qt::NoBrush);
            pen.setWidth(15);
            painter->setPen(pen);
            painter->drawLine(-20,0,20,0);
            painter->drawLine(0,-20,0,20);
            break;
        }
        case Exit: {
            painter->setPen(Qt::NoPen);
            painter->setBrush(m_color);
            QPolygonF polygon;
            polygon << QPointF(30,-38) << QPointF(28,-38) << QPointF(28,38) << \
                       QPointF(-5,35) << QPointF(-5,-35) << QPointF(28,-38)  << \
                       QPointF(30,-38) << QPointF(30,-40) << QPointF(-30,-40) << \
                       QPointF(-30,40) << QPointF(30,40);
            painter->drawPolygon(polygon);
            painter->drawRect(-2,-2,6,3);
            break;
        }
        case TrainLight: {
            painter->drawArc(QRectF(-25, -35, 50, 50), -45*16, 270*16);
            painter->setPen(Qt::NoPen);
            painter->setBrush(m_color);
            painter->drawRect(-15, 25, 30, 5);
            painter->drawRect(-15, 15, 30, 5);
            painter->drawChord(QRectF(-10, 20, 20, 20), 210*16, 120*16);
            break;
        }
        case TrainHorn: {
            painter->drawArc(QRectF(-20, -15, 30, 30), 120*16, 120*16);
            painter->drawArc(QRectF(-35, -30, 60, 60), 120*16, 120*16);
            painter->setPen(Qt::NoPen);
            painter->setBrush(m_color);
            QPolygonF polygon;
            polygon << QPointF(-5,-30) << QPointF(-5,30) << QPointF(15,15) << QPointF(15,-15);
            painter->drawPolygon(polygon);
            painter->drawRect(18, -15, 15, 30);
            break;
        }
        case PowerOn: {
            painter->drawArc(QRectF(-30, -30, 60, 60), 120*16, 300*16);
            painter->drawLine(0, 0, 0, -35);
            break;
        }
        case Stop:
        case SOS: {
            pen.setWidth(2);
            painter->setPen(pen);
            painter->setBrush(m_hcolor);
            QPolygonF polygon;
            polygon << QPointF(-17.5,-40) << QPointF(17.5,-40) << QPointF(40,-17.5) \
                    << QPointF(40,17.5) << QPointF(17.5,40) << QPointF(-17.5,40) \
                    << QPointF(-40,17.5) << QPointF(-40,-17.5);
            painter->drawPolygon(polygon);
            painter->setFont(QFont("Arial", 18, QFont::Bold));
            painter->drawText(QRectF(-40,-40,80,80), Qt::AlignCenter, m_buttontype == Stop ? "STOP" : "SOS");
        }
        case NoType:
        default:
            break;
    }

    if(!isEnabled()) {
        painter->setTransform(transform);
        painter->setOpacity(0.6);
        painter->setPen(Qt::NoPen);
        painter->setBrush(Qt::lightGray);
        painter->drawRoundedRect(m_bounds, 3, 3, Qt::AbsoluteSize);

    }
    painter->setTransform(transform);

    if(m_pressed)
        painter->scale(1.05, 1.05);

}

Button::ButtonType Button::buttonType() const
{ return m_buttontype; }

QColor Button::color() const
{ return m_color; }

void Button::setColor(const QColor& color) {
    m_color = color;
    update();
}

QColor Button::highlightColor() const
{ return m_hcolor; }

void Button::setHighlightColor(const QColor& color) {
    m_hcolor = color;
    update();
}

QLinearGradient Button::linearGradient() const
{ return m_gradient; }

void Button::setLinearGradient(const QLinearGradient& gradient) {
    m_gradient = gradient;
    update();
}

void Button::setSelected(bool selected) {
    m_selected = selected;
    update();
}

bool Button::isSelected() const {
    return m_selected;
}

void Button::timerEvent(QTimerEvent *event) {
    killTimer(event->timerId());
    m_pressed = false;
    update();
}

void Button::mousePressEvent(QGraphicsSceneMouseEvent *event) {
	Q_UNUSED( event );
    m_pressed = true;
    update();
}

void Button::mouseMoveEvent(QGraphicsSceneMouseEvent *event ) {
	Q_UNUSED( event );
    if( !QGraphicsObject::contains(mapFromScene(event->scenePos())) ) {
        m_pressed = false;
        update();
    }
}

void Button::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
	Q_UNUSED( event );
    if(m_pressed && isEnabled()) {
        startTimer(200);
        emit pressed();
    } else
        m_pressed = false;
}
