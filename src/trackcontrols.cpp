#include "trackcontrols.h"

TrackControls::TrackControls(QGraphicsItem *parent) :
    QGraphicsObject(parent) {

    b_rotleft   = new Button(Button::RotateLeft,       70, this);
    b_rotright  = new Button(Button::RotateRight,      70, this);
    b_hmirror   = new Button(Button::MirrorHorizontal, 70, this);
    b_vmirror   = new Button(Button::MirrorVertical,   70, this);
    b_nextnode  = new Button(Button::SwitchNode,       70, this);
    b_delete    = new Button(Button::Delete,           70, this);
    b_trackconf = new Button(Button::TrackConfig,      70, this);

    b_rotright->moveBy(0,73);
    b_hmirror->moveBy(0,146);
    b_vmirror->moveBy(0,219);
    b_nextnode->moveBy(0,292);
    b_delete->moveBy(0,365);
    b_trackconf->moveBy(0,438);

    connect(b_rotleft,   SIGNAL(pressed()), this, SIGNAL(rotateLeft()));
    connect(b_rotright,  SIGNAL(pressed()), this, SIGNAL(rotateRight()));
    connect(b_hmirror,   SIGNAL(pressed()), this, SIGNAL(hMirror()));
    connect(b_vmirror,   SIGNAL(pressed()), this, SIGNAL(vMirror()));
    connect(b_nextnode,  SIGNAL(pressed()), this, SIGNAL(nextNode()));
    connect(b_delete,    SIGNAL(pressed()), this, SIGNAL(deleteTrack()));
    connect(b_trackconf, SIGNAL(pressed()), this, SIGNAL(showTrackConfig()));

    b_rotleft->setEnabled(false);
    b_rotright->setEnabled(false);
    b_hmirror->setEnabled(false);
    b_vmirror->setEnabled(false);
    b_delete->setEnabled(false);
    b_nextnode->setEnabled(false);
    b_trackconf->setEnabled(false);

//    m_ani = new QSequentialAnimationGroup(this);
//    QPropertyAnimation* ani;
//    ani = new QPropertyAnimation(this, "pos");
//    ani->setDuration(300);
//    m_ani->addAnimation(ani);
//    ani = new QPropertyAnimation(this, "visible");
//    ani->setDuration(100);
//    m_ani->addAnimation(ani);

    setFlag(QGraphicsItem::ItemHasNoContents);
    setAcceptHoverEvents(false);
}

void TrackControls::show() {
    QGraphicsItem::show();
}

void TrackControls::hide() {
    QGraphicsItem::hide();
}

void TrackControls::setGeom(bool islrot, bool isrrot, bool ishmir, bool isvmir) {
    b_rotleft->setEnabled(islrot);
    b_rotright->setEnabled(isrrot);
    b_hmirror->setEnabled(ishmir);
    b_vmirror->setEnabled(isvmir);
}

void TrackControls::setDelete(bool enable) {
    b_delete->setEnabled(enable);
}

void TrackControls::setNextNode(bool enable) {
    b_nextnode->setEnabled(enable);
}

void TrackControls::setTrackConfig(bool enable) {
    b_trackconf->setEnabled(enable);
}

QRectF TrackControls::boundingRect() const {
    return QRectF();
}

void TrackControls::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}
