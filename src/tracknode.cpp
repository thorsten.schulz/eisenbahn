#include "tracknode.h"

TrackNode::TrackNode( int type, const QPointF& pos, QGraphicsItem* parent )
: QGraphicsEllipseItem(QRectF(-5,-5,10,10), parent)
{
    m_nodetype = type;
    setPos(pos);
    m_conn = connFromType(type);
    setBrush(Qt::black);
    hide();
}

void TrackNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Q_UNUSED(option);
	Q_UNUSED(widget);
	if( !isVisible() )
		return;
	painter->setRenderHint(QPainter::Antialiasing);
	QGraphicsEllipseItem::paint(painter, option, widget);
}

QPainterPath TrackNode::shape() const
{ return QPainterPath(); }

bool TrackNode::contains(const QPointF& point) const
{
	Q_UNUSED(point);
	return false;
}

/*
das node-Konzept ist Grütze. Es berücksichtigt keine Symmetrieen.

hmirror: spiegelung an der vertikalen
vmirror: spielegung an der horizontalen
rotated: 90° gegen den Uhrzeigersinn

 * Node0 : gerade rechts
 * Node1 : gerade links
 * Node2 : rechts unten 22,5°
 * Node3 : Node7, rechts unten ~10°
 * Node4 : links oben 22,5°
 * Node5 : 45° rechts unten
 * Node7 : links oben ~10°
 * Node8 : 45° links oben
 * Node11: rechts oben 22,5°
 */
QList<ConType*> TrackNode::connFromType(int type)
{
	QList<ConType*> conn;
	switch(type)
	{
	        case Node0: conn << new ConType(Node0, NodeVMirror | NodeHMirror)	<< \
				new ConType(Node0, NodeHMirror)			<<  \
				new ConType(Node1, NodeDefault)			<<  \
				new ConType(Node1, NodeVMirror);
                    break;
	        case Node1: conn << new ConType(Node1, NodeVMirror | NodeHMirror)  <<  \
				new ConType(Node1, NodeHMirror)			<<  \
				new ConType(Node0, NodeDefault)			<<  \
				new ConType(Node0, NodeVMirror);
                    break;
	        case Node2: conn << new ConType(Node2, NodeVMirror | NodeHMirror)  <<  \
				new ConType(Node4, NodeDefault)		<< \
	        		new ConType(Node11, NodeHMirror);
                    break;
	        case Node3: conn << new ConType(Node3, NodeVMirror | NodeHMirror) << \
				new ConType(Node7, NodeDefault);
                    break;
	        case Node4: conn << new ConType(Node4, NodeVMirror | NodeHMirror)	<< \
				new ConType(Node2,  NodeDefault)                <<  \
				new ConType(Node11, NodeDefault);
                    break;
	        case Node5: conn << new ConType(Node5, NodeVMirror | NodeHMirror)  <<  \
                            new ConType(Node5, NodeRotated | NodeHMirror)  <<  \
                            new ConType(Node8, NodeDefault);
                    break;
	        case Node7: conn << new ConType(Node7, NodeVMirror | NodeHMirror) << \
				new ConType(Node3, NodeDefault);
			break;
	        case Node8: conn << new ConType(Node8, NodeVMirror | NodeHMirror)  <<  \
				new ConType(Node5, NodeDefault)                <<  \
				new ConType(Node5, NodeRotated | NodeVMirror);
			break;
	        case Node11:	conn << new ConType( Node11, NodeVMirror | NodeHMirror )  <<  \
					new ConType( Node2,  NodeHMirror )  <<  \
					new ConType( Node4,  NodeVMirror );
                    break;
	}
	return conn;
}
