#ifndef CONTYPE_H
#define CONTYPE_H

#include <QTransform>
#include "config.h"
#include "transform.h"

class ConType
{
public:
    ConType(int type, int direction)
        : m_type(type), m_direction(direction) {}

    int nodeType() const { return m_type; }
    int direction() const { return m_direction; }
    QTransform transform() { return toTransform(m_direction); }

private:
    int m_type;
    int m_direction;
    QTransform m_transform;
};

#endif // CONTYPE_H
