#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <QTransform>

QTransform toTransform(int dir);
int toDirection(QTransform& transform);

#endif // TRANSFORM_H
