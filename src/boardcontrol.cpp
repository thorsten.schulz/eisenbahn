#include "boardcontrol.h"

#include <QDebug>

BoardControl::BoardControl(QGraphicsItem* parent)
    : QGraphicsObject(parent) {

    m_bounds = QRectF(-385,-185,640,375);
    m_scroll = 0;

    m_title = QString("Boards");
    QGraphicsTextItem* titleItem = new QGraphicsTextItem(m_title, this);
    titleItem->setDefaultTextColor(Qt::white);
    titleItem->setFont(QFont("Arial", 20, QFont::Bold));
    titleItem->setPos(m_bounds.topLeft());

    m_dir = BoardDir;

    PixmapButton* b_menu = new PixmapButton(LOGO,  90, this);
    b_refresh    = new Button(Button::Refresh,     90, this);
    b_scrollup   = new Button(Button::ScrollUp,    90, this);
    b_scrolldown = new Button(Button::ScrollDown,  90, this);

    b_menu->setPos(305,-140);
    b_refresh->setPos(305,-45);
    b_scrollup->setPos(305,50);
    b_scrolldown->setPos(305,145);
//    b_load->setPos(305,190);
//    b_load->setEnabled(false);

    connect(b_menu,       SIGNAL(pressed()), this, SIGNAL(showMainMenu())  );
    connect(b_refresh,    SIGNAL(pressed()), this, SLOT(probeAll())         );
    connect(b_scrollup,   SIGNAL(pressed()), this, SLOT(scrollUp())         );
    connect(b_scrolldown, SIGNAL(pressed()), this, SLOT(scrollDown())       );

    QGraphicsTextItem* item = new QGraphicsTextItem("Available Boards:", this);
    item->setDefaultTextColor(Qt::white);
    item->setFont(QFont("Arial", 15, QFont::Bold));
    item->setPos(m_bounds.topLeft());

    item->setPos(m_bounds.topLeft()+QPointF(10,50));

    scanForBoards();

    setAcceptHoverEvents(false);
}

void BoardControl::setList() {
    qDebug() << "setLIST";

    int lc = m_list.count();
    for(int i=0; i<lc; ++i)
        delete m_list[i];
    m_list.clear();

    int bc = m_boards.count();

    if(m_scroll <= 0) {
        m_scroll = 0;
        b_scrollup->setEnabled(false);
        b_scrolldown->setEnabled(bc>5);
    }
    else if(m_scroll >= bc-5) {
        b_scrollup->setEnabled(true);
        b_scrolldown->setEnabled(false);
    }
    else {
        b_scrollup->setEnabled(true);
        b_scrolldown->setEnabled(true);
    }

    for(int i=m_scroll; i<bc && i<5+m_scroll; ++i) {
        QString label(m_boards[i]->boardID());
        label.append("    -    ");
        if(m_boards[i]->isActive())
            label.append(m_boards[i]->activeMedium()).append("    -    ").append(m_boards[i]->activeInterface()).append("    -    ").append("CONNECTED");
        else
            label.append(m_boards[i]->activeMedium()).append("DISCONNECTED");
        TextButton* b_added = new TextButton(label, 620, 50, this);
        b_added->setAlignment(Qt::AlignLeft|Qt::AlignVCenter);
        b_added->setEnabled(false);
        b_added->setPos(m_bounds.topLeft()+QPointF(320, 115+(i-m_scroll)*55));
        b_added->setFontSize(10);
        m_list << b_added;
    }
}


QRectF BoardControl::boundingRect() const {
    return m_bounds;
}

void BoardControl::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setRenderHint(QPainter::Antialiasing);

    QRectF shadowrect(m_bounds.translated(2,2));
    QLinearGradient shadowgrad(shadowrect.topLeft(), shadowrect.bottomRight());
    shadowgrad.setColorAt(0, QColor(0x0,0x0,0x0,0x40));
    shadowgrad.setColorAt(1, QColor(0x0,0x0,0x0,0x40));
    painter->setPen(Qt::NoPen);
    painter->setBrush(shadowgrad);
    painter->drawRoundedRect(shadowrect, 3, 3, Qt::AbsoluteSize);

    QLinearGradient grad(m_bounds.topLeft(),m_bounds.bottomLeft());
    grad.setColorAt(0, QColor(0x60,0x60,0x60));
    grad.setColorAt(1, QColor(0x10,0x10,0x10));
    painter->setPen(Qt::NoPen);
    painter->setBrush(grad);
    painter->drawRoundedRect(m_bounds, 3, 3, Qt::AbsoluteSize);
}

QList<Board*> BoardControl::boards() const{
    return m_boards;
}

void BoardControl::scanForBoards() {

    if (QDir(m_dir).exists()) {        

        QStringList xml("*.xml");
        QFileInfoList files = QDir(m_dir).entryInfoList(xml, QDir::Files, QDir::Time|QDir::Reversed);

        int fc = files.count();
        for(int i=0; i<fc; ++i) {
            QString file = m_dir+files.at(i).fileName();
            QFile eingabe(file);
            if(!eingabe.open(QIODevice::ReadOnly))
                continue;

            QXmlStreamReader stream(&eingabe);

            QString boardID;
            QMap<int, int> pinport;
            QList<QString> medias;
            int pin, port;

            stream.readNextStartElement();
            if(stream.name() != "Board")
                continue;

            stream.readNextStartElement();
            if(stream.name() != "BoardID")
                continue;
            else
                boardID = stream.readElementText();
            if(boardID.isEmpty())
                continue;

            stream.readNextStartElement();
            if(stream.name() != "BoardMedia")
                continue;
            while (stream.readNextStartElement()) {
                if (stream.name() != "Media")
                    break;
                else
                    medias << stream.readElementText();
                if(!stream.isEndElement())
                    break;
            }

            while (stream.readNextStartElement()) {
                if (stream.name() != "PortMap")
                    break;
                else {
                    stream.readNextStartElement();
                    if (stream.name() == "Pin")
                        pin=stream.readElementText().toInt();
                    else break;
                    stream.readNextStartElement();
                    if (stream.name() == "Port") {
                        port=stream.readElementText().toInt();
                        pinport.insert(pin,port);
                    } else break;
                    stream.readNextStartElement();
                    if(!stream.isEndElement())
                        break;
                }
            }
            eingabe.close();

            Board* newboard = new Board(boardID, medias, pinport, false);
            m_boards << newboard;

        }
    }
    else
        QDir(QDir::currentPath()).mkdir(QDir::cleanPath(BoardDir));

    probeAll();

}

void BoardControl::disconnectBoards() {
    int bc = m_boards.count();
    for(int i=0; i<bc; ++i) {
        if(m_boards[i]->isActive())
            m_kernel.stopBoard(m_boards[i]->boardID());
    }
}

void BoardControl::probeBoard(Board* board) {

    qDebug() << "probe" << (void*)board;
    int bc = m_boards.count();
    for(int i=0; i<bc; ++i) {
        if(m_boards[i]->isActive())
            m_kernel.stopBoard(m_boards[i]->boardID());
        m_boards[i]->deactivate();
        m_boards[i]->setActiveMedium(QString());
        m_boards[i]->setActiveInterface(QString());
    }

    //update interface list
    m_mediaintfc = m_kernel.getInterfaceList();
    bool found = false;

    if(board) {
        //probe one
        int idx = m_boards.indexOf(board);
        QList<QString> media = m_boards[idx]->media();
        int mc = media.count();
        for(int k=0; k<mc && !found; ++k) {
            QList<char*> intfc = m_mediaintfc.values(media[k]);
            int ic = intfc.count();
            for(int m=0; m<ic; ++m) {
                m_kernel.initBoard(board->boardID(), media[k], intfc[m]);
                if( (found = m_kernel.probeBoard(board->boardID())) ) {
                    board->activate();
                    board->setActiveMedium(media[k]);
                    board->setActiveInterface(QString(intfc[m]));
                    m_mediaintfc.remove(media.value(k), intfc.takeAt(m));
                    break;
                }
            }
        }
    } else {
    qDebug() << "board is 0";
        int bc = m_boards.count();
        for(int i=0; i<bc; ++i) {
            found = false;
            QList<QString> media = m_boards[i]->media();
            int mc = media.count();
            for(int k=0; k<mc && !found; ++k) {
                QList<char*> intfc = m_mediaintfc.values(media[k]);
                int ic = intfc.count();
                for(int m=0; m<ic; ++m) {
                    qDebug() << "scanning" << intfc[m];
                    m_kernel.initBoard(m_boards[i]->boardID(), media[k], intfc[m]);
                    qDebug() << "trying probeBoard:" << intfc[m];
                    if( (found = m_kernel.probeBoard(m_boards[i]->boardID())) ) {
                        qDebug() << m_boards[i]->boardID() << " found at intfc: " << intfc[m];
                        m_boards[i]->activate();
                        m_boards[i]->setActiveMedium(media[k]);
                        m_boards[i]->setActiveInterface(QString(intfc[m]));
                        m_mediaintfc.remove(media.value(k), intfc.takeAt(m));
                        break;
                    }
                }
            }
        }
    }

    setList();
}


void BoardControl::changeActivationTime(int pin, int time) {
    int port;
    int bc = m_boards.count();
    for(int i=0; i<bc; ++i) {
        if(!m_boards[i]->isActive())
            continue;
        port = m_boards[i]->mapPinToPort(pin);
        if(port != -1) {
            m_kernel.changeTime(m_boards[i]->boardID(), port, time);
            return;
        }
    }
}

void BoardControl::activatePin(int pin) {
    int port;
    int bc = m_boards.count();
    //qDebug() << "BOARDCOUNT:" << bc;
    for(int i=0; i<bc; ++i) {
        if(!m_boards[i]->isActive())
            continue;
        port = m_boards[i]->mapPinToPort(pin);
        //qDebug() << "Found portnumer?: " << port;
        if(port != -1) {
            m_kernel.setSwitch(m_boards[i]->boardID(), port);
            return;
        }
    }
}
