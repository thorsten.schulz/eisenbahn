#include "railtracklist.h"

RailtrackList::RailtrackList(QGraphicsItem *parent)
: QGraphicsObject(parent)
{
	showDefaultList( true );
	setFlag( QGraphicsItem::ItemHasNoContents );
	setAcceptHoverEvents(false);
}

void RailtrackList::addButton( Railtrack *track, int i )
{
	TrackButton* button = new TrackButton( track, 85, this );
	// TODO: get window/widget size
	int widgetbreite = 1200;
	int maxZeile = widgetbreite/88;
	int z = i % maxZeile;
	int s = (1 - i/maxZeile) * 100;
	button->setPos(88*z,s);
	connect(button, SIGNAL(pressed(Railtrack*)), this, SIGNAL(pressed(Railtrack*)));
	m_newbuttons << button;
}

void RailtrackList::showDefaultList( bool enabled )
{
	if( enabled )
		for( int i = 0; i < Num_RailTracks; ++i )
			addButton( Railtrack::fromType(i), i );
	fadeOutList();
}

void RailtrackList::updateRailtrackList( QList<Railtrack*> newlist )
{
	// TODO: test for multiple symmetric entries
	for ( int i = 0; i < newlist.count(); ++i )
		addButton( newlist[i], i );
	fadeOutList();
}

void RailtrackList::clearList()
{
    int lb = m_buttons.count();
    for(int i = 0; i < lb; ++i)
        delete m_buttons[i];
    m_buttons.clear();

    fadeInList();
}

void RailtrackList::fadeOutList() {

    QParallelAnimationGroup* fadeOut = new QParallelAnimationGroup(this);
    connect(fadeOut, SIGNAL(finished()), this, SLOT(clearList()));
    int lb = m_buttons.count();
    for(int i = 0; i < lb; ++i) {
        QPropertyAnimation* ani = new QPropertyAnimation(m_buttons[i], "pos");
        ani->setDuration(100);
        ani->setEndValue(m_buttons[i]->pos()+QPointF(0,100));
        fadeOut->addAnimation(ani);
    }
    fadeOut->start(QAbstractAnimation::DeleteWhenStopped);
}

void RailtrackList::fadeInList() {

    m_buttons = m_newbuttons;
    m_newbuttons.clear();

    QParallelAnimationGroup* fadeIn = new QParallelAnimationGroup(this);
    int lb = m_buttons.count();
    for(int i = 0; i < lb; ++i) {
        QPropertyAnimation* ani = new QPropertyAnimation(m_buttons[i], "pos");
        ani->setDuration(100);
        ani->setEndValue(m_buttons[i]->pos()+QPointF(0,-100));
        fadeIn->addAnimation(ani);
    }
    fadeIn->start(QAbstractAnimation::DeleteWhenStopped);
}

QRectF RailtrackList::boundingRect() const
{ return QRectF(); }

void RailtrackList::paint( QPainter* painter,
	const QStyleOptionGraphicsItem* option, QWidget* widget )
{
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}
