#ifndef RAILTRACK_H
#define RAILTRACK_H

#include <QGraphicsSvgItem>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QSvgRenderer>
#include <QtGlobal>

#include "tracknode.h"
#include "config.h"
#include "scene.h"

#include "transform.h"

class PartialTrackTint { /* mark partial tints of TrackKnot tracks from geoplan classes. They need to be mapped to Railtrack tiles in the go. */
public:
	PartialTrackTint() { m_start=0; m_end=0; m_color=QColor(); m_id = ' '; }
	PartialTrackTint(int start, int end, const QColor &color, char id = ' ', double scale = 255.0) {
		m_start = start/scale;
		m_end   =   end/scale;
		m_color = color;
		m_id    = id;
	}
	double m_start, m_end;
	QColor m_color;
	char m_id;

	inline bool isWithinTile(double start, double end) const { /* start and end are relative to spur -- composed link */
		return (m_start < m_end) ? ((m_start < end) && (m_end > start)) : ((m_end < end) && (m_start > start));
	}

	inline PartialTrackTint sect(double start, double end) const {
		PartialTrackTint ptt;
		ptt.m_color = m_color;
		ptt.m_id = m_id;

		double l = qAbs(end-start);
		ptt.m_start = qBound(0.0, (m_start-start)/l, 1.0);
		ptt.m_end   = qBound(0.0, (m_end  -start)/l, 1.0);

		return ptt;
	}
	inline bool operator==(const PartialTrackTint &c) const { return  ((m_start==c.m_start)&&(m_end==c.m_end)&&(m_color==c.m_color)); }
	inline bool operator!=(const PartialTrackTint &c) const { return !((m_start==c.m_start)&&(m_end==c.m_end)&&(m_color==c.m_color)); }
};

class Railtrack : public QGraphicsSvgItem
{
public:
    Railtrack( QSvgRenderer* shared, QGraphicsItem* parent = 0 );

    static Railtrack* fromType( int type, QGraphicsItem* parent = 0 );
	// die Anschlüsse am Gleisstück
    static QList<TrackNode*> nodesFromType(int tracktype);
    virtual bool isActivePart() const = 0;
    virtual int trackType() const = 0;

    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint( QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget );

	// mögliche Transformationen
    void rotate(qreal angle);
    inline void rotateLeft() { rotate(-90); }
    inline void rotateRight() { rotate(90); }

    void mirror(int hscale, int vscale);
    inline void mirrorVertical() { mirror(1,-1); }
    inline void mirrorHorizontal() { mirror(-1,1); }

    void setDirection( QTransform direction, bool comb = true);
    inline void setDirection( int direction, bool comb = true) { setDirection( toTransform(direction), comb ); }

    int direction() const;

    QList<TrackNode*> nodes() const;
    TrackNode* nextNode();
    bool hasNode(int type) const;
    QList<TrackNode*> openNodeList();

    void setLocked(int locked);
    bool highlighted() const;
    void setHighlighted(bool highlighted);
    void setTint(int tid, std::vector<PartialTrackTint> updatedSections);
    inline int getLength() const { return m_length; }
    inline void setDebug(bool debug) { dbg = debug; }
    inline void setRedFrame(bool vis) { redFrame = vis; update(); }
    inline bool isLocked() const { return m_locked != (-1); }

    Railtrack *closestRailtrack(const QList<Railtrack*> rtl, QPointF &nodeP); /* helper for find, returns 0 on switches, because find does not know where to go on */
    double find(QList<Railtrack*> rtl, QVector<Railtrack *> &crumbs, QPointF nodeP, Railtrack *upTo);
    inline void toggleTintDirection() { m_bluntFoundFirst = !m_bluntFoundFirst; }

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);    

    QList<TrackNode*> m_nodes;
    int  m_length;
    int  m_current;


private:
    //SHAPE
    QRectF m_bounds;
    QPainterPath m_shape;
    QPointF m_origin;

    //MOVING ITEM
    bool m_movetrack;
    bool m_trackmoved;
    QPoint m_oldpos;

    bool m_highlighted;
    int  m_locked; /* unlocked track if != -1 */
    //std::vector<PartialTrackTint> m_tintSections;
    std::map<int, std::vector<PartialTrackTint>> m_tintSections;
    bool m_bluntFoundFirst;
    bool dbg = false;
    bool redFrame = false;
};

#endif // RAILTRACK_H
