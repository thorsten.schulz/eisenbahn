#include "infopanel.h"

InfoPanel::InfoPanel(QRectF size, QGraphicsItem *parent) :
    QGraphicsObject(parent) {

    m_bounds  = size;
    m_msg     = 0;
    m_timerID = 0;
}

void InfoPanel::printMsg(const QString& msg) {
    if(m_msg)
        delete m_msg;
    if(m_timerID) {
        killTimer(m_timerID);
        m_timerID = 0;
    }
    m_msg = new QGraphicsTextItem(msg, this);
    m_msg->setFont(QFont("Arial", 10, QFont::Bold));
    m_msg->setDefaultTextColor(Qt::white);
    m_msg->setPos(m_bounds.topLeft()+QPointF(5,2));
    m_timerID = startTimer(2000);
    emit showMsg();
}

void InfoPanel::timerEvent(QTimerEvent *event) {
    killTimer(event->timerId());
    m_timerID = 0;
    emit hideMsg();
}

QRectF InfoPanel::boundingRect() const {
    return m_bounds;
}

void InfoPanel::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setRenderHint(QPainter::Antialiasing);

    QRectF shadowrect(m_bounds.translated(2,2));
    QLinearGradient shadowgrad(shadowrect.topLeft(), shadowrect.bottomRight());
    shadowgrad.setColorAt(0, QColor(0x0,0x0,0x0,0x40));
    shadowgrad.setColorAt(1, QColor(0x0,0x0,0x0,0x40));
    painter->setPen(Qt::NoPen);
    painter->setBrush(shadowgrad);
    painter->drawRoundedRect(shadowrect, 3, 3, Qt::AbsoluteSize);

    QLinearGradient grad(m_bounds.topLeft(),m_bounds.bottomLeft());
    grad.setColorAt(0, QColor(0x60,0x60,0x60));
    grad.setColorAt(1, QColor(0x10,0x10,0x10));
    painter->setPen(Qt::NoPen);
    painter->setBrush(grad);
    painter->drawRoundedRect(m_bounds, 3, 3, Qt::AbsoluteSize);
}
