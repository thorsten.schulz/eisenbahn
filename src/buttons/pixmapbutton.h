#ifndef PIXMAPBUTTON_H
#define PIXMAPBUTTON_H

#include "button.h"
#include "config.h"

class PixmapButton : public Button
{
public:
    PixmapButton(QString filename, qreal size = 100, QGraphicsItem *parent = 0);
    PixmapButton(QString filename, qreal width = 100, qreal height = 100, QGraphicsItem *parent = 0);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    void setPixmap(const QString filename);
    void setPixmap(const QPixmap pixmap);

private:
    QString m_filename;
    QPixmap m_pixmap;
};

#endif // PIXMAPBUTTON_H
