#include "pixmapbutton.h"

PixmapButton::PixmapButton( QString filename, qreal size, QGraphicsItem* parent )
    : Button(NoType, size, parent) {
    m_pixmap = QPixmap(filename);
}

PixmapButton::PixmapButton( QString filename, qreal width, qreal height, QGraphicsItem* parent )
    : Button(NoType, width, height, parent) {
    m_pixmap = QPixmap(filename);
}

void PixmapButton::setPixmap(const QString filename) {
    m_pixmap = QPixmap(filename);
    update();
}

void PixmapButton::setPixmap(const QPixmap pixmap) {
    m_pixmap = pixmap.copy();
    update();
}

void PixmapButton::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    Button::paint(painter, option, widget);
    if(!m_pixmap.isNull()) {
        painter->scale(0.85,0.85);
        QPixmap pixmap = m_pixmap.scaled(m_bounds.width(),m_bounds.height(),Qt::KeepAspectRatio,Qt::SmoothTransformation);
        qreal xmargin = qAbs(m_bounds.width()-pixmap.rect().width())/2.0;
        qreal ymargin = qAbs(m_bounds.height()-pixmap.rect().height())/2.0;
        painter->drawPixmap(m_bounds.adjusted(xmargin,ymargin,-xmargin,-ymargin), pixmap, pixmap.rect());
    }
}
