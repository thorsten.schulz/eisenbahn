#include "textbutton.h"
#include <QDebug>

TextButton::TextButton( QString label, qreal size, QGraphicsItem* parent )
    : Button(NoType, size, parent), m_fontsize(10) {
    m_label = label;
    m_alignment = Qt::AlignCenter;
}

TextButton::TextButton( QString label, qreal width, qreal height, QGraphicsItem* parent )
    : Button(NoType, width, height, parent), m_fontsize(10) {
    m_label = label;
    m_alignment = Qt::AlignCenter;
}

QString TextButton::label() const {
    return m_label;
}

void TextButton::setLabel(const QString label) {
    m_label = label;
    update();
}

void TextButton::setFontSize(int size) {
    m_fontsize = size;
    update();
}

void TextButton::setAlignment(int alignment) {
    m_alignment = alignment;
}

void TextButton::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
    Q_UNUSED(option);
    Q_UNUSED(widget);
    Button::paint(painter, option, widget);
    painter->setPen(color());
    painter->setFont(QFont("Arial", m_fontsize, QFont::Bold));
    painter->drawText(m_bounds.adjusted(10,0,-10,0), m_alignment, m_label);
}
