#ifndef TRACKBUTTON_H
#define TRACKBUTTON_H

#include "../button.h"
#include "../railtrack.h"

class TrackButton : public Button
{
	Q_OBJECT
public:
	TrackButton( Railtrack *track, qreal size = 100,
		QGraphicsItem *parent = 0 );
	TrackButton( Railtrack *track, qreal width = 100,
		qreal height = 100, QGraphicsItem *parent = 0 );

	void paint( QPainter *painter, const QStyleOptionGraphicsItem *option,
		QWidget *widget );

	Railtrack *track() const;
	void setTrack( Railtrack *track );

signals:
	void pressed( Railtrack *);

private:
	Railtrack *m_track;
private slots:
	void forwardTrack();
};

#endif // TRACKBUTTON_H
