#ifndef TEXTBUTTON_H
#define TEXTBUTTON_H

#include "../button.h"

class TextButton : public Button
{
public:
    TextButton( QString label, qreal size = 100, QGraphicsItem *parent = 0);
    TextButton( QString label, qreal width = 100, qreal height = 100, QGraphicsItem *parent = 0);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    QString label() const;
    void setLabel(const QString label);
    void setFontSize(int size);

    void setAlignment(int alignment);

private:
    QString m_label;
    int m_fontsize;
    int m_alignment;
};

#endif // TEXTBUTTON_H
