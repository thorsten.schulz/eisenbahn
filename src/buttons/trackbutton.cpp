#include "trackbutton.h"

TrackButton::TrackButton( Railtrack *track, qreal size, QGraphicsItem* parent )
: Button(Button::Track, size, parent), m_track(track)
{ connect( this, SIGNAL(pressed()), SLOT(forwardTrack())); }

TrackButton::TrackButton( Railtrack* track, qreal width, qreal height,
	QGraphicsItem* parent )
: Button(Button::Track, width, height, parent), m_track(track)
{ connect( this, SIGNAL(pressed()), SLOT(forwardTrack())); }

void TrackButton::forwardTrack()
{ emit pressed( m_track ); }

Railtrack* TrackButton::track() const
{ return m_track; }

void TrackButton::setTrack( Railtrack* track )
{
    m_track = track;
    update();
}

void TrackButton::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Button::paint(painter, option, widget);
	if ( m_track == 0)
		return;
	qreal xscale = 200.0/m_track->renderer()->defaultSize().width();
	qreal yscale = 200.0/m_track->renderer()->defaultSize().height();
	QRectF rect(-m_bounds.width()/xscale/2.0, -m_bounds.height()/yscale/2.0, m_bounds.width()/xscale, m_bounds.height()/yscale);
	painter->setTransform(m_track->transform(), true);
	painter->scale(0.85,0.85);
	m_track->renderer()->render(painter, "inverted", rect);
}
