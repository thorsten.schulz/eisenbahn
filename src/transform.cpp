#include "transform.h"

QTransform toTransform(int dir) {
    switch(dir) {
        case 0:  return QTransform( 1, 0,0, 0, 1,0,0,0,1); break;
        case 1:  return QTransform(-1, 0,0, 0, 1,0,0,0,1); break;
        case 2:  return QTransform( 1, 0,0, 0,-1,0,0,0,1); break;
        case 3:  return QTransform(-1, 0,0, 0,-1,0,0,0,1); break;
        case 4:  return QTransform( 0,-1,0, 1, 0,0,0,0,1); break;
        case 5:  return QTransform( 0,-1,0,-1, 0,0,0,0,1); break;
        case 6:  return QTransform( 0, 1,0, 1, 0,0,0,0,1); break;
        case 7:  return QTransform( 0, 1,0,-1, 0,0,0,0,1); break;
        default: return QTransform( 1, 0,0, 0, 1,0,0,0,1); break;
    }
}

int toDirection(QTransform& transform) {
    if(transform.m11()) {
        if(transform.m11() == 1) {
            if(transform.m22() == 1)
                return 0;
            else
                return 2;
        }
        else {
            if(transform.m22() == 1)
                return 1;
            else
                return 3;
        }
    }
    else {
        if(transform.m12() == 1) {
            if(transform.m21() == 1)
                return 6;
            else
                return 7;
        }
        else {
            if(transform.m21() == 1)
                return 4;
            else
                return 5;
        }
    }
}
