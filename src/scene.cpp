#include "scene.h"

Scene::Scene( int raster, QObject *parent )
    : QGraphicsScene(parent), m_raster(raster), m_grid(false)
{ }

int Scene::raster() const
{ return m_raster; }

void Scene::setRaster( int raster )
{
    if(m_raster == raster)
        return;
    m_raster = raster;
    invalidate(QRectF(), BackgroundLayer);
}

bool Scene::gridVisible() const
{ return m_grid; }

void Scene::setGrid( bool on )
{
    if(m_grid == on)
        return;
    m_grid = on;
    invalidate(QRectF(), BackgroundLayer);
}

void Scene::toggleGrid()
{
    m_grid = !m_grid;
    invalidate(QRectF(), BackgroundLayer);
}

void Scene::drawBackground( QPainter* painter, const QRectF& rect )
{
	if ( !m_grid )
		return;
        painter->setPen(QColor(0xe0, 0xe0, 0xe0));
        qreal left = int(rect.left()) - (int(rect.left()) % m_raster);
        qreal top  = int(rect.top() ) - (int(rect.top() ) % m_raster);

        for(qreal x = left; x < rect.right(); x += m_raster)
            painter->drawLine(QLineF(x, rect.top(), x, rect.bottom())  );

        for(qreal y = top; y < rect.bottom(); y += m_raster)
            painter->drawLine(QLineF(rect.left()  , y, rect.right(), y));
}
