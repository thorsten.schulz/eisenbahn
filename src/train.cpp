#include "train.h"

Train::Train(TrainData data, QGraphicsItem *parent) :
    QGraphicsObject(parent) {

    m_poweron = false;

    b_train     = new PixmapButton(LOGO,         141, this);
    b_power     = new Button(Button::PowerOn,     45, this);
    b_light     = new Button(Button::TrainLight,  45, this);
    b_horn      = new Button(Button::TrainHorn,   45, this);
    b_stop      = new Button(Button::Stop,        45, this);
    b_accel     = new Button(Button::ScrollUp,    45, this);
    b_decel     = new Button(Button::ScrollDown,  45, this);
    b_speed     = new TextButton(QString("0"),    45, this);
    b_speed->setFontSize(10);

    m_speed = 0;

    b_train->setPos(-23,-23);
    b_power->setPos(-71,73);
    b_light->setPos(-23,73);
    b_horn->setPos(25,73);
    b_stop->setPos(73,73);
    b_accel->setPos(73,-71);
    b_speed->setPos(73,-23);
    b_decel->setPos(73,25);

    connect(b_accel, SIGNAL(pressed()), this, SLOT(accelerate()));
    connect(b_decel, SIGNAL(pressed()), this, SLOT(decelerate()));
    connect(b_power, SIGNAL(pressed()), this, SLOT(power()));
    connect(b_stop, SIGNAL(pressed()), this, SLOT(stop()));

    m_data = data;

    b_speed->setEnabled(false);
    b_train->setEnabled(false);
    b_stop->setEnabled(false);
    b_horn->setEnabled(false);
    b_light->setEnabled(false);
    b_accel->setEnabled(false);
    b_decel->setEnabled(false);

    setFlag(QGraphicsItem::ItemHasNoContents);

}

void Train::accelerate() {
    if(m_poweron) {
        int err = m_speed%15;
        if(err)
            m_speed -= err;
        else
            m_speed += 15;

        if((unsigned int)abs(m_speed) > m_data.vmax)
            m_speed = m_data.vmax;
        b_speed->setLabel(QString::number(m_speed));
        emit speedChanged(m_data.id, m_speed);
    }
}

void Train::decelerate() {
    if(m_poweron) {
        int err = m_speed%15;
        if(err)
            m_speed -= err;
        else
            m_speed -= 15;

        if((unsigned int)abs(m_speed) > m_data.vmax)
            m_speed = -m_data.vmax;
        b_speed->setLabel(QString::number(m_speed));
        emit speedChanged(m_data.id, m_speed);
    }
}

void Train::power() {
    if(m_poweron) {
        b_power->setSelected(false);
        //disable buttons
        b_train->setEnabled(false);
        b_stop->setEnabled(false);
        b_horn->setEnabled(false);
        b_light->setEnabled(false);
        b_accel->setEnabled(false);
        b_decel->setEnabled(false);
        m_speed = 0;
        b_speed->setLabel(QString::number(m_speed));
        m_poweron = false;
        emit PowerOff(m_data.id);
    } else {
        emit PowerOn(m_data.id);
        b_power->setSelected(true);
        //enable buttons
        b_train->setEnabled(true);
        b_stop->setEnabled(true);
        b_horn->setEnabled(true);
        b_light->setEnabled(true);
        b_accel->setEnabled(true);
        b_decel->setEnabled(true);
        m_speed = 0;
        b_speed->setLabel(QString::number(m_speed));
        m_poweron = true;
    }
}

void Train::stop() {
    if(m_poweron) {
        m_speed = 0;
        b_speed->setLabel(QString::number(m_speed));
        emit fullStop(m_data.id);
    }
}

void Train::released() {
        m_speed = 0;
        b_speed->setLabel(QString::number(m_speed));
        m_poweron = false;
        b_power->setSelected(false);
        //disable buttons
        b_train->setEnabled(false);
        b_stop->setEnabled(false);
        b_horn->setEnabled(false);
        b_light->setEnabled(false);
        b_accel->setEnabled(false);
        b_decel->setEnabled(false);
}

QRectF Train::boundingRect() const {
    return QRectF();
}

void Train::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
    return;
}

void Train::updatePixmap(const QImage& img) {
    b_train->setPixmap(QPixmap::fromImage(img));
    m_data.img = new QImage(img);
}

void Train::updatePixmap(const QPixmap& pixmap) {
    b_train->setPixmap(pixmap);
}

int Train::getTrainID() const {
    return m_data.id;
}

void Train::updateTrainID(int id) {
    m_data.id = id;
}

int Train::getUser() const {
    return m_data.user;
}

void Train::updateUser(int user) {
    m_data.user = user;
}

int Train::getVmax() const {
    return m_data.vmax;
}

void Train::updateVmax(int vmax) {
    m_data.vmax = vmax;
}

int Train::getTrainLength() const {
    return m_data.TrainLength;
}

void Train::updateTrainLength(int length) {
    m_data.TrainLength = length;
}

QString Train::getTrainName() const {
    return m_data.TrainName;
}

void Train::updateTrainName(QString name) {
    m_data.TrainName = name;
}

QString Train::getTrainPicURL() const {
    return m_data.TrainPicURL;
}

void Train::updateTrainPicURL(QString url) {
    m_data.TrainPicURL = url;
}

QImage* Train::getImg() const {
    return m_data.img;
}

int Train::imgRequestPending() const {
    return m_data.img_request_pending;
}

void Train::updateImgRequest(bool pending) {
    m_data.img_request_pending = pending;
}

