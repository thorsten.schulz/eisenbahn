
/*
 *
 * name: global.c
 *
 * this module provides some global data, such as the current
 * debug level, the current filename from which the shell reads
 * its commands, and the current output file descriptor. the
 * main purpose of these global functions is that the parameters
 * are available everywhere without passing them from function
 * to functions.
 *
 * the variables are globally visible, but the access to them
 * is embedded into macros. this way, it can be easliy changes,
 * into functions for example.
 *
 */

#include <stdio.h>

/*
 *
 * warning: do not remove this c-file from the project, since it
 * contains the global data, even though it does not look like that ;-)
 *
 */

/*
 *
 * in order to declare the globaly availabe variables, we have
 * to remove the 'extern' specifier...
 *
 */

#define extern

#include "global.h"


