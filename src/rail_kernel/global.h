
/*
 *
 * name: global.c
 *
 * this module provides some global data, such as the current
 * debug level, the current filename from which the shell reads
 * its commands, and the current output file descriptor. the
 * main purpose of these global functions is that the parameters
 * are available everywhere without passing them from function
 * to functions.
 *
 * the variables are globally visible, but the access to them
 * is embedded into macros. this way, it can be easliy changes,
 * into functions for example.
 *
 */

typedef struct {
	char *sh_name;
	FILE *sh_fp;
        int   sh_debug;
        } SH_GLOBAL;

extern SH_GLOBAL __sh_global;

#define sh_debug( level )	(__sh_global.sh_debug & (level))
#define sh_name()		(__sh_global.sh_name)
#define sh_fp()			(__sh_global.sh_fp)

#define set_debug( l )		__sh_global.sh_debug = (l)
#define set_shname( name )	__sh_global.sh_name = name
#define	set_shfp( fp )		__sh_global.sh_fp = fp

#define sh_global()		__sh_global
#define sh_restore( save )	__sh_global = save

/*
 *
 * we already know some debug options. they are:
 * 
 */

#define DB_INPUT	0x01
#define DB_PARS		0x02
#define DB_EXEC		0x04
#define DB_COM		0x08
#define DB_NETWORK	0x10
#define DB_ANY		(~0)

#define DB_STR		"# "		/* to visualize debugging messages */
