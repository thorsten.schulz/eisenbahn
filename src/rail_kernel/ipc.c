
/*
 * name: ipc.c
 *
 * this module realizes the communication to and from the rail kernel.
 * since several communication media, such as RS232, USB, etc., are
 * conceivable, this module defines a standard interface in ipc.h.
 * the two functions ipc_getentry() and ipc_size() are not implemented
 * here, but elsewhere, which allows some flexibility and easyness
 * to maintain different versions/implementations.
 *
 * since several boards might be connected to the host PC, this
 * module also realizes all the requried routing, i.e., to which
 * device should the rail message be sent?
 *
 */

#include <stdio.h>
#include <string.h>

#include "protocol.h"
#include "media.h"
#include "ipc.h"
#include "msg-tools.h"
#include "tools.h"
#include "global.h"

/*
 *
 * we have to maintain a routing table. each entry consists of
 * an opaque stream handle, a link into the ipc media table,
 * the address of the first switch of the corresponding board
 * and the number of devices on that board. since the table might
 * be dynamically growing and shrinking, we embed all variables
 * into a struct.
 *
 */

typedef struct {
	MED_PTR  rte_media;	// a pointer to the media description
        char	*rte_stream;	// a 'stream handle' as an opaque type
	char	*rte_fname;	// name of the corresponding device file
        char    *rte_id;	// name of the board
        } RTE_ENTRY, *RTE_PTR;

typedef struct {
        RTE_PTR rt_table;		// a pointer to a connection array
        int     rt_size;		// the current size of the table
        int     rt_next;		// the number of valid entries
        } RT_TABLE, *RT_PTR;

static RT_TABLE rt_table = { 0, 0, 0 };

/*
 *
 * in the first section, we initialize this module
 *
 */

char *ipc_init()
     {
	MED_PTR p;
        int     i;
        for( i = 0; (p = media_getentry( i )); i++ )
	   (* p->med_init)();
	return 0;
     }


/*
 *
 * now, we want to be able to find a medium
 *
 */

static MED_PTR media_find( const char *media )
       {
	  MED_PTR p;
          int     i;
	  for( i = 0; (p = media_getentry( i )); i++ )
	     if ( ! strcmp( media, p->med_name ) )
	        return p;
	  return 0;
       }

/*
 *
 * now, we are maintaining the routing table. on demand, we grow
 * and shrink the table. also, we check for potential conflicts
 * when trying to add a new route...
 *
 */

// --- finding a route

static RTE_PTR route_find( const char *id )
       {
          RTE_PTR p = rt_table.rt_table;
          for( ; p < rt_table.rt_table + rt_table.rt_next; p++ )
	     if ( ! strcmp( id, p->rte_id ) )
	        return p;
	  return 0;
       }

// --- adding the entry

char *ipc_addroute( const char *id, const char *media,const char *fname )
     {
        MED_PTR mp;
	RTE_PTR ep;
	char   *errmsg;
	//int     i;
	void   *stream;
        if ( rt_table.rt_next == rt_table.rt_size )
           rt_table.rt_table = tab_grow( rt_table.rt_table, sizeof(RTE_ENTRY),
                                         & rt_table.rt_size );
        if (! (mp = media_find( media )))
	   return "unknown media";
	if ( route_find( id ) )
           return "board id already in routing table";
	if ( ! (errmsg = (* mp->med_open)( fname, & stream )))
	{
           ep = rt_table.rt_table + rt_table.rt_next++;
	   ep->rte_stream = stream;
	   ep->rte_fname  = save_string( fname );
	   ep->rte_media  = mp;
	   ep->rte_id     = save_string( id );
	}
	return errmsg;
     }

/*
 *
 * before removing a route, we first have to find the entry. this is done
 * by a match of the first addesses in rt_afind(). on sucess, we
 * call the close()-function on the media.
 *
 */

char *ipc_delroute( const char *id )
     {
	RTE_PTR ep = route_find( id );
	char   *errmsg;
	int     i;
	if ( ! ep )
	   return "unknown routing entry (use first address)";
	if ( ! (errmsg = (* ep->rte_media->med_close)( ep->rte_stream )))
	{
	   m_free( ep->rte_fname, "rt_delentry1", 0 );
	   m_free( ep->rte_id,    "rt_delentry2", 0 );
           i = ep - rt_table.rt_table;
           for( rt_table.rt_next--; i < rt_table.rt_next; i++ )
              rt_table.rt_table[ i ] = rt_table.rt_table[ i + 1 ];
	}
	return errmsg;
     }

/*
 *
 * how about a nicely formated printout of the routing table?
 *
 */

int ipc_prtroutes( FILE *fp )
    {
       RTE_PTR p = rt_table.rt_table;
       MED_PTR mp;
       int i;
       fprintf( fp, "Routing Table (" );
       for( i = 0; (mp = media_getentry( i )); i++ )
	  fprintf( fp, "%s%s", (i)? "' ": "", mp->med_name );
       fprintf( fp, "):\n" );
       if ( rt_table.rt_next )
       {
	  fprintf( fp, "%20s  %12s  %s\n",
	  	   "Id", "Media/Type", "Filename" );
          for( i = 0; i < rt_table.rt_next; i++, p++ )
	     fprintf( fp, "%20s  %12s  %s\n",
	     	      p->rte_id, p->rte_media->med_name, p->rte_fname );
       }
       else fprintf( fp, "\tsorry, is empty\n" );
       return 0;
    }

/*
 *
 * so, sending a message :-)
 *
 */

char *ipc_sendmsg( RM_PTR msg, const char *id, RM_PTR rsp )
     {
        RTE_PTR route = route_find( id );
	MED_PTR media;
        FILE   *fp = sh_fp();
        char   *errmsg;
        char    buf[ sizeof(RMSG) ];	      // that's more than enough space
        int     msize, bz = sizeof( buf );
	if ( route == 0 )
	{
           if ( sh_debug( DB_COM ) )
	      fprintf( fp, "%sipc_sendmsg: cannot send msg: cmd=0x%X; "
                           "no route to board '%s'\n", DB_STR,
			   msg->rm_cmd, id );
           return "no route to device (board)";
	}
	media = route->rte_media;
        msize = mt_baked2raw( (RBP)buf, msg );
        if ( sh_debug( DB_COM ) )
	   fprintf( fp, "%sipc_sendmsg: sending msg: cmd=0x%X "
	   		"msize=%d to media='%s' index=%d\n",
                        DB_STR, msg->rm_cmd, msize,
                        media->med_name, (int)(route - rt_table.rt_table));
        if (   (errmsg = (* media->med_write)( route->rte_stream, buf, msize ))
            || (errmsg = (* media->med_read) ( route->rte_stream, buf, & bz )))
	   return errmsg;
          // printf("bz %d\n", bz);
        msize = mt_raw2baked( rsp, (RBP)buf, bz, RM_MAXSTRING );
        if ( sh_debug( DB_COM ) )
	   fprintf( fp, "%ssendmsg: received an answer, cmd=0x%X left-over "
                        "bytes=%d\n", DB_STR, rsp->rm_cmd, msize );
	return  (msize == -1)? "received message was too short"
	       :(msize == -2)? "received message has too many parameters"
               :(msize >   0)? "leftover bytes in received message"
	       : 0;
     }


