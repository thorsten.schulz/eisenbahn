
/*
 *
 * name: tools.h
 *
 * this module provides a few fundamental functions, such as
 * m_alloc(), m_free(), and tab_grow().
 *
 */

void *m_alloc( int size, char *fnc );
void *m_free( void *p, char *fnc, void *ret );
void *tab_grow( void *tab, int type_size, int *entries );
char *save_string( const char *str );


