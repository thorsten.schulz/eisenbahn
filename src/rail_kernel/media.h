#ifndef MEDIA_H
#define MEDIA_H
/*
 * name: media.h
 *
 * the following data structure defines a standard interface for
 * all connection types. the function prototypes are:
 *
 *	char *media_init ();
 *	char *media_open ( char *file_name, void **stream );
 *	char *media_close( void *stream );
 *	char *media_read ( void *stream, unsigned char *msg, int *msg_len );
 *	char *media_write( void *stream, unsigned char *msg, int msg_len );
 *	char *media_ioctl( void *stream, char *buffer, int len );
 *
 * in all these functions, 'stream' refers to a module-specific internal
 * data type. in order to keep this interface simple, this data type
 * is 'opaque', which means that it is casted to a 'void *' when used
 * outside the actual module. in other words, non of the callers has
 * any idea about the particular details of this data type.
 *
 * when calling a read()-function, the third parameter specifies the
 * length of the buffer (which is currently 6*sizeof(UCHAR2) + RM_MAXSTRING
 * in the railroad project
 *
 * on success, all functions return a null-pointer, otherwise the return
 * a pointer to an error message.
 *
 */

#define MAX_INTFC 30

#ifdef __cplusplus
extern "C" {
#endif
typedef struct {
        char *med_name;		// name, such es RS232, USB, ETH, ...
	char *med_comment;	// any comment, if required or useful
	char *(*med_init)();	// initializing the medium
	char *(*med_open)();	// open a new connection
	char *(*med_close)();	// close an existing connection
	char *(*med_read)();	// read from an existing connection
        char *(*med_write)();	// write to an existing connection
        char *(*med_ioctl)();	// ioctl, a configuration means; see UNIX
        char *(*med_intfc)();	// return file_names of the corresponding interfaces
        } MEDIA_ENTRY, *MED_PTR;

MED_PTR media_getentry( int entry );
char *media_getinterfaces( int entry, int* cnt, char** file_names );
#ifdef __cplusplus
}
#endif

#endif //MEDIA_H
