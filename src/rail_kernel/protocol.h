#ifndef PROTOCOL_H
#define PROTOCOL_H
/*
*
* 	file: protocol.h
*
*	this file contains the relevant protocol definitions
*
*/

#define RM_MAXSTRING	246	// for non-fixed messages

/*
 *
 * the first definition is fundamental in that it defines
 * the data types that are transfered over a communication
 * connection, such as RS232, USB, or ethernet. the fundamental
 * problems are that data types, such as integers, depend
 * on the architecture and that the protocol should be very
 * simple. the usage of "regular" encode/decode stubs, as
 * they are used in, for example, corba, would work. however,
 * they would go not easily with the low resources of the devices
 * used within the railroad project.
 *
 * in so doing the first definition defines a platform-independent
 * 8-bit data type. yes, it's an unsigned char. the second data
 * type defines and provides an easy way of how to transmit a
 * 16-bit integer. 16-bit integer should suffice in all cases;
 * so no 32-bit integers are defined... but this can be easily done.
 *
 * any communication-specific definition, such as the frame sizes etc.,
 * are outside the scope of this file and should be made elsewhere.
 *
 */

/*
 *
 * IMPORTANT ASSUMPTIONS: this protocol definition makes two important
 * assumptions:
 *	1. it assumes that an MYBYTE is an 8-bit unsigned data type,
 *	   i.e., its an unsigned 8-bit container. the name RBYTE
 *	   is an abbreviation for rail byte.
 *	2. it furthermore assumes that within a struct, integers
 *	   are at consecutive addresses without any padding.
 *	   example: given a struct such as struct { int i; int j } st;
 *	   then, & st.i + 1 == & st.j
 *	   this assumption is more than just reasonable....
 *	3. the definitions made below assume that an integer has
 *	   has at least 16 bit.
 * both assumtions are relevant for the functions raw2backed() and
 * backed2raw(), which are both implemented in msg-tools.c.
 *
 * the define of MAJOR_FIRST specifies that the most-significant
 * byte of an integer appears first in a message stream. this
 * choise is only for the readability of an octal dump, see, e.g.,
 * the unix command 'od'. if we want it the other way around, we
 * should not define this label.
 *
 * furthermore, the first part defines the number of bytes that are
 * effectively used within this project. as far as the project team
 * is aware of, two bytes should be working in all cases.
 *
 * in summary, the following three definitions cope with the hardware
 * dependencies.
 *
 */

typedef unsigned char MYBYTE;		// just a simple 8-bit data container

#define	MAJOR_FIRST			// no value is ised in msg-tools.c
#define	BYTES_PER_INT	2		// two bytes per int are used
                                        // should be working in all cases
/*
 *
 * the following data structures define the basic layout of
 * a message as it is transfered to and from any device that
 * is somehow concerend with the railroad project. this definition
 * is above level zero, i.e., the MYBYTE definition.
 * in order to make all the basic (i/o) programming
 * as easy as possible, the message core does contain only integers,
 * which have to be transmitted in terms of BYTES_PER_INT * MYBYTE data items.
 *
 * the only exception from this rule is the entry rb_string, which
 * may host any char-string data, e.g., a message to be displayed
 * on a little tiny screen. in such cases, the variable rb_size
 * referes to *all* characters in rb_string. normally strings are
 * terminated by null-byte. therefore, rb_size should include that
 * null-byte, which makes further programming easier.
 *
 * the typedefs that are given prior to the actual message definition
 * describe and host all the parameters for all the commands.
 *
 * the rb_string-option can also be used for any other transfer
 * of any custom data, which resembles the unix-ioctl-functionality.
 * if using the custom command, the first parameter in
 * the data structure (i.e., rb_string) may contain the actual
 * command.
 *
 * after a long discussion and several prototypes, the railroad
 * project team came to the conclusion that a device consists
 * only of ports of any sort; switches are no longer part of
 * the rail kernel. rather, the application has to group several
 * ports together to control a switch.
 *
 * a consequence of this port vs. switch discussion concerns
 * the concept of port addresses. they of course all have
 * an address, which has to be passed along with the command.
 * since the a specific railroad project might have several
 * boards and since all boards might have a different number
 * of input and output ports, a portaddress is not transfered
 * by default. rather, the port is transfered on a per-command
 * basis. thus, the application has to know to which it wants
 * to send a command record.
 *
 * if receiving a message, the board checks, whether an (port)
 * address is valid or not. in case of an error, the board
 * returns RE_IPORT or RE_OPORT, depending on whether the target
 * was an input or an output port. many commands also allow for
 * the port address (-1), which matches all ports of the targeted
 * type. the value (-1) might also be interpreted as 'board address'.
 *
 * within a communication, it is not necessary to transmitt all
 * the bytes the defined message contains; the number of transmitted
 * bytes should please the requirements of the actual command rm_cmd...
 * the number of parameters is also encoded in the actual command
 * definition. this number can be extracted by using the macro
 * rc_nparameters( cmd ). this number refers to the command's
 * parameters. that is, a message contains its header, i.e., an
 * address and the command, and then BYTES_PER_INT * nparameters
 * bytes, which give the parameter values.
 *
 * within this project, the concept of a 'version' is represented by
 * an integer in which the minor and major bytes represent the
 * version's minor and major releases, respectively. for example,
 * the value 0x0431 would be version 4.31
 */

// --- the following is the definition of the system record as may
// --- be used in any board. except rs_control and rs_monotime,
// --- all the variables are read only for the PC.

typedef struct {
	int rs_control;		// control register (flags)
        int rs_monotime;	// default monostable time (puls width)
	int rs_version;		// the current protocol version (major.minor)
	int rs_maxstr;		// the maximum length of string messages
        int rs_iports;		// the number of input ports
        int rs_oports;		// the number of output ports
        } RSYS, *RS_PTR;

#define RS_SIZE			(sizeof( RSYS )/ sizeof( int ))

// --- a pair of a port and a value

typedef struct {
	int rpv_port;		// the port
	int rpv_value;		// the value
	} RPVAL, *RPV_PTR;

#define RPV_SIZE		(sizeof( RPVAL )/ sizeof( int ))

// --- timing of an otput port

typedef struct {
	int rtm_port;		// target port
	int rtm_mode;		// the port's mode, i.e., ROX_....
	int rtm_on;		// ``on'' time
	int rtm_off;		// ``off'' time
	int rtm_atime;		// adaptation time
        } RPTIME, *RTM_PTR;

#define RTM_SIZE		(sizeof( RPTIME )/ sizeof( int ))

// the plain string (byte) record

typedef struct {
        int  rb_port;		// target port number
        int  rb_size;		// the number of valid bytes in rb_string
        } RBYTE, *RB_PTR;

#define RB_SIZE			(sizeof( RBYTE )/ sizeof( int ))

// and now the actual message definition. the component
// rm_string is for RC_STRING and RC_IOCTL and declared
// at the end, since it makes a few things easier, e.g.,
// msg-tools.c

typedef struct {
        int   rm_cmd;			// the cmd
	union {
		int	rm_value;	// just one parameter
		RPVAL	rm_pval;	// port and value
		RSYS	rm_sys;		// the system record
        	RPTIME	rm_ptime;	// port timing
                RBYTE	rm_bytes;	// strings and any type
	      } rm_data;		// all the parameters
        char  rm_string[ RM_MAXSTRING ];// the variable data area for
        } RMSG, *RM_PTR;

/*
 *
 * next, we have to define all the commands. in order to ease all the
 * programming, any command definition should also provide further
 * information, such as the number of parameters, the command type,
 * whether it is a response, etc. no problem, since we have 16 bits
 * (i.e., two times an RBYTE, which in turn is an eight-bit data type)
 *
 * the approach resembles the way processor vendors define their
 * instruction sets. motorola, for example, insert the target and
 * source registers of a move instruction directly into the op-code.
 * the following table provides: number of bits, bit mask, and meaning.
 * the general command layout is:
 *
 *    4 bits: 0xF000 : number of parameters
 *    1 bit : 0x0800 : response from board (rkernel) to host
 *    1 bit : 0x0400 : command with a flexible number of bytes (RBYTE)
 *                     also called string command within this project
 *    1 bit : 0x0200 : request a data response from the board (rkernel)
 *    1 bit : 0x0100 : rail answer pending
 *    2 bits: 0x00C0 : not used
 *    6 bits: 0x003F : the actual command
 *
 * for the actual command definitions, we use the concepts of a mask
 * to extract the bits, e.g., 0x0F for the number of parameters,
 * and the number of bits the mask is shifted, e.g., 12 for the
 * number of parameters. in case of a flexible number of bytes,
 * i.e., the bit 0x0400 is set, the number of parameters should be 1,
 * since the size-count has to be transfered as well.
 *
 * some commands, such as RC_GETSWCONFIG (get_switch_config), request
 * some data from the rkernel. these commands have the RC_QMASK bit
 * set. this bit makes it easy for the program to note that it has
 * to wait for a response when they are operating in asynchronous mode.
 * furthermore, the request and response command codes are equal,
 * which make the macro rc_cmdmatch() quite easy.
 *
 */

// defining masks and shifts

#define RCC_PMASK	0x0F		/* number of parameters */
#define RCC_PSHFT	12
#define RCC_RMASK	0x01		/* response from rkernel */
#define RCC_RSHFT	11
#define RCC_BMASK	0x01		/* string/byte command */
#define RCC_BSHFT	10
#define RCC_QMASK	0x01		/* requesting a response */
#define RCC_QSHFT	9
#define RCC_CMASK	0x3F		/* the actual command code */
#define RCC_CSHFT	0

#define RCC_ECHO	0x0100

// defining some useful makros

#define rc_nparameters( x )	(((x) >> RCC_PSHFT) & RCC_PMASK)
#define rc_isresponse( x )	((x) & (RCC_RMASK << RCC_RSHFT))
#define rc_isstringcmd( x )	((x) & (RCC_BMASK << RCC_BSHFT))
#define rc_cmdmatch( x, y )	(((x) & RCC_CMASK) == ((y) & RCC_CMASK))

#define rc_cmake( size, resp, str, req, cmd )	( \
		    (((size) & RCC_PMASK) << RCC_PSHFT) \
		  | (((resp) & RCC_RMASK) << RCC_RSHFT) \
                  | (((str ) & RCC_BMASK) << RCC_BSHFT) \
		  | (((req ) & RCC_QMASK) << RCC_QSHFT) \
		  | (((cmd ) & RCC_CMASK) << RCC_CSHFT) )

// the actual command definitions: paras, resp, str, req, cmd

#define RC_RESET	rc_cmake(        0, 0, 0, 0, 0x00 )
                        // --- reseting the addressed board
#define RC_RECEIPT	rc_cmake( RPV_SIZE, 1, 0, 0, 0x01 )
			// --- the rkernel sends a receipt with an error code
#define RC_SETSYSTEM	rc_cmake(        1, 0, 0, 0, 0x02 )
                        // --- set the board's system control register
#define RC_GETSYSTEM	rc_cmake(        0, 0, 0, 1, 0x03 )
                        // --- request a board's the system record
#define RC_SYSTEM	rc_cmake(  RS_SIZE, 1, 0, 0, 0x03 )
			// --- and that's the rkernel's response
#define RC_OPCFG	rc_cmake( RPV_SIZE, 0, 0, 0, 0x04 )
			// --- configure an output port
#define RC_IPCFG	rc_cmake( RPV_SIZE, 0, 0, 0, 0x05 )
			// --- configure an input port
#define RC_MONOTIME	rc_cmake( RPV_SIZE, 0, 0, 0, 0x06 )
			// --- set a monoflop's default activation time
#define RC_MONOREPORT  	rc_cmake( RPV_SIZE, 0, 0, 0, 0x07 )
			// --- set an output port's report port
#define RC_PSET  	rc_cmake(        1, 0, 0, 0, 0x08 )
			// --- activate a monoflop with default time
#define RC_PCLEAR  	rc_cmake(        1, 0, 0, 0, 0x09 )
			// --- clears a monoflop
#define RC_SETPORT  	rc_cmake( RTM_SIZE, 0, 0, 0, 0x0A )
			// --- activate a port, i.e., PXM_ ...
#define RC_GETNAME  	rc_cmake(        0, 0, 0, 1, 0x0B )
                        // --- request the board's unique identifier
#define RC_NAME  	rc_cmake(  RB_SIZE, 1, 1, 0, 0x0B )
                        // --- and that's the rkernel's response

#define RC_STRING	rc_cmake(  RB_SIZE, 0, 1, 0, 0x30 )
			// --- regular string command
#define RC_MPSET	rc_cmake(  RB_SIZE, 0, 1, 0, 0x31 )
			// multiple monoflop activation with default timing
#define RC_MPCLEAR	rc_cmake(  RB_SIZE, 0, 1, 0, 0x32 )
			// multiple monoflop deactivation
#define RC_IOCTLI	rc_cmake(  RB_SIZE, 0, 1, 0, 0x3E )
			// --- any custom command (like UNIX ioctl)
#define RC_IOCTLO	rc_cmake(  RB_SIZE, 0, 1, 0, 0x3F )
			// --- any custom command (like UNIX ioctl)

// command arguments and auxilary constants (RX = Rail-Auxilary-Constant)

	// --- output port config values

#define RXO_LOWACTIVE	1		// set output port to low active

	// --- output port modes

#define RXM_MONO        0       // that's the classical monostable (mono-flop)
#define RXM_FLASH       1       // flashing on and off
#define RXM_PWM         2       // PWM, e.g., for speed or brightness control
#define RXM_INTVAL      3       // this port processes integer values
#define RXM_MODES	"mono", "flash", "pwm", "intval"  // conversion table

	// --- system config values


// finally, we have some error definitions (RE = Rail-Error)

#define RE_OK		0	// all right :-)
#define RE_CMD		1	// illegal command
#define RE_OPORT	2	// unknown output port
#define RE_IPORT	3	// unknown input port
#define RE_SIZE		4	// the received packet was too short
#define RE_TIMING	5	// illegal timing constant
#define RE_OPMODE	6	// unknown output port mode

// here, we define the corresponding error messages
// they may be used like 'char *msg_table[] = { RC_ERRMSGs };'

#define RC_ERRMSGs			  \
	"ok"				, \
	"unknown command"		, \
	"unknown output port"		, \
	"unknown input port"		, \
	"received packet is too short"	, \
	"illegal timing constant(s)"	, \
	"unknown mode for output port"
#endif //PROTOCOL_H
