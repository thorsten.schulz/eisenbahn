
/*
 *
 * name: msg-tools.c
 *
 * description: this file contains a few message tools, which
 * might be useful for both the core and the actual application.
 *
 * the function baked2raw converts a given msg data structure into
 * the byte, i.e., BYTES_PER_INT, representation as specified in
 * protocol.h. since this part is entirely under the control of the
 * programmer, this function does not do any error checking; it 
 * simply assumes that the raw data structure is large enough to
 * hold the msg. however, if we would be too studpid to avoid
 * buffer overflows, .... ;-)
 *
 * conversally, the function raw2baked does do error checking
 * with respect to potential buffer overflows. it checks
 * whether the given number of parameters would fit into the
 * rm_data data structure. it also checks for potential buffer
 * overflows in case of string commands. both sizes, raw_size
 * and max_string_size are given in multiples of bytes.
 *
 */

#include "protocol.h"
#include "msg-tools.h"

#define BPI	BYTES_PER_INT		// just to make it a short name

static RBP put_ints( RBP bp, int *ip, int size )
       {
          MYBYTE tmp[ BPI ];
	  int i, val;
	  while( size-- )
	  {
	     val = *ip++;
             for( i = BPI; i--; val >>= 8 )
		tmp[ i ] = val & 0xFF;
	     #ifdef MAJOR_FIRST
                for( i = 0; i < BPI; i++ )
                   *bp++ = tmp[ i ];
	     #else
                for( i = BPI; i--; )
                   *bp++ = tmp[ i ];
	     #endif
	  }
          return bp;
       }

static RBP get_ints( RBP bp, int *ip, int size )
       {
          MYBYTE tmp[ BPI ];
	  int i, val;
	  while( size-- )
	  {
	     #ifdef MAJOR_FIRST
                for( i = BPI; i--; )
                   tmp[ i ] = *bp++;
	     #else
                for( i = 0; i < BPI; i++ )
                   tmp[ i ] = *bp++;
	     #endif
             val = (signed char) tmp[ BPI - 1 ];
             for( i = BPI - 1; i--; )
		val = val << 8 | (tmp[ i ] & 0xFF);
	     *ip++ = val;
	  }
          return bp;
       }

static RBP copyrbytes( RBP to, RBP from, int size )
       {
	  while( size-- > 0 )
	     *to++ = *from++;
	  return to;
       }

/*
 *
 * the following function converts a raw message into a 'regular'
 * message struct. since anything can come in over the communication
 * line, we have to do several checks. mainly, we have to check
 * whether the raw message containes enough bytes and whether the
 * raw data fits into the message record.
 *
 * return values:
 * 	-2: the raw messag had too many parameters for the
 * 	    rm_data structure or too many chars for the rb_string
 * 	-1: the message struct is too small (to much raw data) or
 * 	    the raw message does not contain enough data
 *	 0: all bytes have been processed (converted) for the message
 * 	>0: some left-over bytes
 *
 */

int mt_raw2baked( RM_PTR mp, RBP buf, int raw_size, int max_str_size )
    {
       RBP rawp = buf;
       int parameters;
       if ((raw_size -= BPI) < 0 )
          return -1;
       rawp = get_ints( rawp, & mp->rm_cmd, 1 );
       parameters = rc_nparameters( mp->rm_cmd );
       if ((raw_size -= parameters * BPI) < 0 )
          return -1;
       if ( parameters > (int)(sizeof( mp->rm_data ) / sizeof( int )))
          return -2;
       rawp = get_ints( rawp, (int *) &(mp->rm_data), parameters );
       if ( rc_isstringcmd( mp->rm_cmd ) ) {
          if ((raw_size -= mp->rm_data.rm_bytes.rb_size * sizeof( char )) < 0 )
             return -1;
          else if ( mp->rm_data.rm_bytes.rb_size > max_str_size )
             return -2;
          else copyrbytes( (RBP) mp->rm_string, rawp,
                          mp->rm_data.rm_bytes.rb_size * sizeof( char ));
       }
       return raw_size;
    }

int mt_baked2raw( RBP buf, RM_PTR mp )
    {
       RBP tp;
       tp = put_ints( buf, (int *) & mp->rm_cmd,
       			       1 + rc_nparameters( mp->rm_cmd ) );
       if ( rc_isstringcmd( mp->rm_cmd ) )
          tp = copyrbytes( tp, (RBP) mp->rm_string,
                           mp->rm_data.rm_bytes.rb_size * sizeof(char));
       return tp - buf;
    }

/*
#include <stdio.h>


main()
{
	int i, j, k;
	RMSG msg1, msg2;
        MYBYTE buf[ sizeof( int ) ];
        MYBYTE mbuf[ sizeof( RMSG ) ];
	// testing put_ints() and get_ints()
	for( i = -2; i < 3; i++ )
	{
           put_ints( buf, & i, 1 );
           get_ints( buf, & j, 1 );
           printf( "converstion: i=%2d j=%2d buf=", i, j );
           for( k = 0; k < BPI; k++ )
              printf( " 0x%02X", (int) buf[ k ] );
	   printf( "\n" );
	   if ( i != j )
	      printf( "error: conversion %d != %d not matching\n", i, j );
	}
	msg1.rm_cmd = RC_SETPORT;
	msg1.rm_data.rm_ptime.rtm_port = -1;
	msg1.rm_data.rm_ptime.rtm_mode = 0;
	msg1.rm_data.rm_ptime.rtm_on = 1;
	msg1.rm_data.rm_ptime.rtm_off = 2;
	msg1.rm_data.rm_ptime.rtm_atime = 3;
        i = mt_baked2raw( mbuf, & msg1 );
        printf( "msg-converstion: i=%2d bytes converted\n", i );
        j = mt_raw2baked( & msg2, mbuf, i, RM_MAXSTRING );
        printf( "msg-converstion: j=%2d bytes not converted back\n", j );
	printf( "values:\n" );
	printf( "\t: cmd: 0x%X 0x%X\n", msg1.rm_cmd, msg2.rm_cmd );
	printf( "\t: port: %d %d\n", msg1.rm_data.rm_ptime.rtm_port,
				     msg2.rm_data.rm_ptime.rtm_port );
	printf( "\t: mode: %d %d\n", msg1.rm_data.rm_ptime.rtm_mode,
				     msg2.rm_data.rm_ptime.rtm_mode );
	printf( "\t: on: %d %d\n", msg1.rm_data.rm_ptime.rtm_on,
				     msg2.rm_data.rm_ptime.rtm_on );
	printf( "\t: off: %d %d\n", msg1.rm_data.rm_ptime.rtm_off,
				     msg2.rm_data.rm_ptime.rtm_off );
	printf( "\t: atime: %d %d\n", msg1.rm_data.rm_ptime.rtm_atime,
				     msg2.rm_data.rm_ptime.rtm_atime );
}
*/
