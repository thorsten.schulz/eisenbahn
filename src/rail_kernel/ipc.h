#ifndef IPC_H
#define IPC_H

/*
 * name: ipc.h
 *
 * the following data structure defines a standard interface for
 * all connection types. the function prototypes are:
 *
 *	char *ipc_init();
 *	char *ipc_open ( char *file_name, void **stream );
 *	char *ipc_close( void *stream );
 *	char *ipc_read ( void *stream, unsigned char *msg, int *msg_len );
 *	char *ipc_write( void *stream, unsigned char *msg, int msg_len );
 *	char *ipc_ioctl( void *stream, char *buffer, int len );
 *
 * in all these functions, 'stream' refers to a module-specific internal
 * data type. in order to keep this interface simple, this data type
 * is 'opaque', which means that it is casted to a 'void *' when used
 * outside the actual module. in other words, non of the callers has
 * any idea about the particular details of this data type.
 *
 * when calling a read()-function, the third parameter specifies the
 * length of the buffer (which is currently 6*sizeof(UCHAR2) + RM_MAXSTRING
 * in the railroad project
 *
 * on success, all functions return a null-pointer, otherwise the return
 * a pointer to an error message.
 *
 */

#include "protocol.h"

#ifdef __cplusplus
extern "C" {
#endif

    char *ipc_init();
    char *ipc_addroute( const char *id,const char *media, const char *fname );
    char *ipc_delroute( const char *id );
    int   ipc_prtroutes( FILE *fp );
    char *ipc_sendmsg( RM_PTR msg, const char *id, RM_PTR rsp );

#ifdef __cplusplus
}
#endif
#endif //IPC_H
