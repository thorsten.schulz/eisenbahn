
/*
 *
 * name: tools.c
 *
 * this module provides a few fundamental functions, such as
 * m_alloc(), m_free(), and tab_grow().
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tools.h"

void *m_alloc( int size, char *fnc )
     {
	void *p;
        if ((p = (void *) malloc( size )))
	   return p;
	fprintf( stderr, "malloc: called from '%s': sorry, cannot allocate "
                 "%d bytes\n", fnc, size );
	exit( 1 );
     }

void *m_free( void *p, char *fnc, void *ret )
     {
         fnc = fnc;
	if ( p )
	   free( p );
	return ret;
     }

void *tab_grow( void *tab, int type_size, int *entries )
     {
	void *p;
	char *s, *t;
	int i, new_size = *entries + 10;
        p = m_alloc( new_size * type_size, "tab_grow" );
        for( t = p, s = tab, i = *entries * type_size; i--; )
	   *t++ = *s++;
        *entries = new_size;
        m_free( tab, "tab_grow", 0 );
	return p;
     }

char *save_string( const char *str )
     {
        char *p = m_alloc( strlen( str ) + 1, "save_string" );
	strcpy( p, str );
	return p;
     }


