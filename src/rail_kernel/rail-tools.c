
/*
 *
 * name: rail-tools.c
 *
 * the module rail-tools.c work closely together with rail-cmd.c.
 * actually, we could have put both together into one file; it
 * would be more than just easy. however, this way, we de-couple
 * it a bit and also make the list of include files shorter in
 * each module.
 *
 * anyhow, this file is concerned with the operation on rail-commands.
 * that is their execution, i.e., forwarding them to send_msg() (ipc.c)
 * and evaluating the response. furthermore, this module provides
 * a few printing functions.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "protocol.h"
#include "rail-tools.h"
#include "ipc.h"


static int prthexstr( FILE *fp, char *str, int len )
       {
          int i; //, c
	  for( i = 0; i < len; i++ )
	     fprintf( fp, " 0x%02X", ((unsigned int) *str++) & 0xFF );
          return 0;
       }

static int prtmstr( FILE *fp, char *str, int len )
       {
          int i, c;
	  for( i = 0; i < len; i++ )
	     if ( (c = *str++) >= 32 && c < 127 )
		fprintf( fp, "%c", c );
	     else fprintf( fp, "'%02x'", c & 0xFF );
          return 0;
       }

void rt_prtparameters( FILE *fp, RM_PTR mp )
     {
	RPV_PTR rpv = & mp->rm_data.rm_pval;
	RS_PTR  syp = & mp->rm_data.rm_sys;
	RTM_PTR rtp = & mp->rm_data.rm_ptime;
        RB_PTR  stp = & mp->rm_data.rm_bytes;
	switch( mp->rm_cmd )
	{
	   case RC_RESET:
	   case RC_GETSYSTEM:		// no parameters et al.
           case RC_GETNAME:
                break;
	   case RC_SETSYSTEM:
	   case RC_PSET:
	   case RC_PCLEAR:
		fprintf( fp, "\tvalue=0x%X\n", mp->rm_data.rm_value );
                break;
	   case RC_SYSTEM:
	   	fprintf( fp, "\tcontrol=0x%X", syp->rs_control );
	   	fprintf( fp, "\tversion=%d.%d", (syp->rs_version >> 8) & 0xFF,
						syp->rs_version & 0xFF );
	   	fprintf( fp, "\tmonotime=%d\n", syp->rs_monotime );
	   	fprintf( fp, "\tmaxstring=%d", syp->rs_maxstr );
	   	fprintf( fp, "\tiports=%d", syp->rs_iports );
	   	fprintf( fp, "\toports=%d\n", syp->rs_oports );
                break;
	   case RC_RECEIPT:
	   case RC_OPCFG:
	   case RC_IPCFG:
	   case RC_MONOTIME:
	   case RC_MONOREPORT:
		fprintf( fp, "\taddress=%d", rpv->rpv_port );
		fprintf( fp, "\tvalue=%d\n", rpv->rpv_value );
                break;
	   case RC_SETPORT:
		fprintf( fp, "\tport=%d", rtp->rtm_port );
		fprintf( fp, "\tmode=%d\n", rtp->rtm_mode );
		fprintf( fp, "\non=%d", rtp->rtm_on );
		fprintf( fp, "\toff=%d", rtp->rtm_off );
		fprintf( fp, "\tadaptation time=%d\n", rtp->rtm_atime );
                break;
           case RC_STRING:
	   case RC_IOCTLI:
	   case RC_IOCTLO:
	   case RC_MPSET:
	   case RC_MPCLEAR:
                fprintf( fp, "\tport=%d\t'", stp->rb_port );
		if ( mp->rm_cmd == RC_STRING )
                   prtmstr( fp, mp->rm_string, stp->rb_size );
                else prthexstr( fp, mp->rm_string, stp->rb_size );
	  	fprintf( fp, "'\n" );
                break;
	   default:
		fprintf( fp, "\tsorry, printing details of this command"
			     "not implemented\n" );
                break;
	}
     }

void rt_prtshort( FILE *fp, RM_PTR mp, char *cname )
     {
	int i, n, *ip;
	if ( cname )
	   fprintf( fp, "%s ", cname );
	else fprintf( fp, "cmd=0x%X ", mp->rm_cmd );
	if ((n = rc_nparameters( mp->rm_cmd )) > 0 )
	{
	   fprintf( fp, " parameter(s)=" );
	   for( i = 0, ip = (int *) & mp->rm_data; i < n; i++ )
	      fprintf( fp, " 0x%X", *ip++ );
	}
	if ( rc_isstringcmd( mp->rm_cmd ) )
	{
	   fprintf( fp, " string='" );
           n = mp->rm_data.rm_bytes.rb_size;
	   prtmstr( fp, mp->rm_string, (n > 6)? 4: n );
	   if( n > 5 )
	      fprintf( fp, "..." );
	   fprintf( fp, "'" );
	}
     }

/*
 *
 * the following function forwards the message to ipc_sendmsg() for
 * execution and checks for any error condition; but no printing takes place.
 *
 */

char *rt_execcmd( RM_PTR mp, const char *id, RM_PTR rsp )
     {
	char *errmsg = ipc_sendmsg( mp, id, rsp );
        //printf("%s", errmsg);
	if ( errmsg )
	   return errmsg;
	if ( rsp->rm_cmd == RC_RECEIPT )
	   return (rsp->rm_data.rm_value != RE_OK )
	   	  ? rt_ecode2str( rsp->rm_data.rm_value ): 0;
	if (rc_isresponse( rsp->rm_cmd ) && rc_cmdmatch(mp->rm_cmd,rsp->rm_cmd))
	   return 0;
	return "unexpected response from rail kernel";
     }

/*
 *
 * very important, we should not forget a function that converts
 * an error code into an adequate string.
 *
 */

#define ERR_FORMAT	"sorry, no error message for error code %d\n"
static char  err_buf[ sizeof( ERR_FORMAT ) + (sizeof( int ) + 5)/3 ];

char *rt_ecode2str( int ecode )
     {
	static char *err_msgs[] = { RC_ERRMSGs };
        if ( ecode >= 0 && ecode < (int)(sizeof( err_msgs )/sizeof( char * )))
	   return err_msgs[ ecode ];
        sprintf( err_buf, ERR_FORMAT, ecode );
        return err_buf;
     }


