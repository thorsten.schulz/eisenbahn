
/*
 *
 * name: msg-tools.h
 *
 * description: this file contains a few message tools, which
 * might be useful for both the core and the actual application.
 *
 * the function baked2raw converts a given msg data structure into
 * the byte, i.e., major and minor, representation as specified in
 * protocol.h. since this part is entirely under the control of the
 * programmer, this function does not do any error checking; it 
 * simply assumes that the raw data structure is large enough to
 * hold the msg. however, if we would be too studpid to avoid
 * buffer overflows, .... ;-)
 *
 * conversally, the function raw2baked does do error checking
 * with respect to potential buffer overflows. it checks
 * whether the given number of parameters would fit into the
 * rm_data data structure. it also checks for potential buffer
 * overflows in case of string commands. both sizes, raw_size
 * and max_string_size are given in multiples of bytes.
 *
 */

typedef MYBYTE *RBP;		// to make it short, a point to a rail-byte

int mt_raw2baked( RM_PTR mp, RBP raw, int raw_size, int max_str_size );
int mt_baked2raw( RBP raw, RM_PTR mp );


