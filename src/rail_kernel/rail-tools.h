#ifdef __cplusplus
extern "C" {
#endif
/*
 *
 * name: rail-tools.h
 *
 * the module rail-tools.c work closely together with rail-cmd.c.
 * actually, we could have put both together into one file; it
 * would be more than just easy. however, this way, we de-couple
 * it a bit and also make the list of include files shorter in
 * each module.
 *
 * anyhow, this file is concerned with the operation on rail-commands.
 * that is their execution, i.e., forwarding them to send_msg() (ipc.c)
 * and evaluating the response. furthermore, this module provides
 * a few printing functions.
 *
 */

void  rt_prtparameters( FILE *fp, RM_PTR mp );
void  rt_prtshort( FILE *fp, RM_PTR mp, char *cname );
char *rt_execcmd( RM_PTR mp, const char *id, RM_PTR rsp );
char *rt_ecode2str( int ecode );


#ifdef __cplusplus
}
#endif
