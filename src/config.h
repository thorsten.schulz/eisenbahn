#ifndef CONFIG_H
#define CONFIG_H

#define TRACK_DIR "config/tracks/"
#define BoardDir  "config/boards/"
#define GFX_DIR   "res/"
#define LOGO      GFX_DIR "logo.png"

#define Num_RailTracks  10

//TRACKS
#define Rt_ShortStraight  0
#define Rt_LongStraight   1
#define Rt_LongCurved     2
#define Rt_ShortCurved    3
#define Rt_HalfCurved     4
#define Rt_StraightSwitch 5
#define Rt_CurvedSwitch   6
#define Rt_Decouple       7
#define Rt_ShortAngled    8
#define	Rt_Switch3		9

//NODES
#define Node0   0
#define Node1   1
#define Node2   2
#define Node3   3
#define Node4   4
#define Node5   5
#define Node6   6
#define Node7   7
#define Node8   8
#define Node9   9
#define Node10  10
#define Node11  11

//DIRECTION
#define NodeRotated 4
#define NodeVMirror 2
#define NodeHMirror 1
#define NodeDefault 0


#endif // CONFIG_H
