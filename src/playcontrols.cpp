#include "playcontrols.h"

PlayControls::PlayControls(QGraphicsItem *parent) :
    QGraphicsObject(parent) {

    b_menu = new PixmapButton(LOGO,  90, this);
    b_sos  = new Button(Button::SOS, 90, this);

    b_sos->moveBy(100,0);
    b_sos->hide();

    connect(b_menu, SIGNAL(pressed()), this, SIGNAL(showMainMenu()));
    connect(b_sos, SIGNAL(pressed()), this, SIGNAL(stopAllTrains()));

    setFlag(QGraphicsItem::ItemHasNoContents);
}

void PlayControls::showSOS(bool on) {
    b_sos->setVisible(on);
}

QRectF PlayControls::boundingRect() const {
    return QRectF();
}

void PlayControls::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

