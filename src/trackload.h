#ifndef TRACKLOAD_H
#define TRACKLOAD_H

#include <QGraphicsObject>
#include <QGraphicsPixmapItem>
#include <QPainter>
#include <QDir>
#include <QSignalMapper>
#include "button.h"
#include "buttons/pixmapbutton.h"
#include "buttons/textbutton.h"

#include "config.h"

class TrackLoad : public QGraphicsObject
{
    Q_OBJECT
public:
    TrackLoad(QGraphicsItem* parent = 0);

    QRectF boundingRect() const;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);
    void show();

signals:
     void loadTrack(const QString& filename);
     void showMainMenu();
     void newTrack();
     void sendMsg(QString msg);

private slots:
    void scrollUp() { m_scroll -=3; setList(); }
    void scrollDown() { m_scroll +=3; setList(); }
    void loadTrack();
    void deleteTrack();

public slots:
    void setList();
    void selectTrack(int selected);

private:
    QRectF m_bounds;
    QString m_title;

    QString m_dir;

    int m_scroll;
    QList<Button*> m_tracks;
    QList<QString> m_files;
    QSignalMapper* m_mapper;
    Button* m_selected;

    QString m_filename;

    PixmapButton* b_menu;
    Button* b_deletetrack;
    Button* b_scrollup;
    Button* b_scrolldown;
    Button* b_load;
};

#endif // TRACKLOAD_H
