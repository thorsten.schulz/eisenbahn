/*
 * geoplan.h
 *
 *  Created on: 02.12.2015
 *      Author: thorsten
 */

#ifndef GEOPLAN_H_
#define GEOPLAN_H_

#include <QObject>
#include <QUdpSocket>
#include <QHostInfo>
#include <cstdint>
#include <QMap>
#include <QElapsedTimer>
#include "railtracks/activetrack.h"
#include <vector>
#include <map>

#include "tau_xsession.hpp"

#define nil (-1)

typedef enum {
	TKL_BLUNT = 0,
	TKL_STRAIGHT,
	TKL_RIGHT,
	TKL_LEFT,

	TKLs
} TrackKnotLine;

typedef enum {
	TKO_NA,  // initial state
	TKO_CCW, // straight points counter math correct
	TKO_CW   // straight points clockwise
} TrackKnotOrientation;

typedef int32_t sid_t;         // there is one exception currently, the line spurs can only fit the lower 16 bit of sid
typedef int16_t velocity_t;
typedef int32_t tid_t;         // TrainID, for simplicity the 32bit type. All values but nil (-1) allowed
typedef uint8_t rloc_t;        // relative location of limit on this TrackKnot
typedef int32_t location_t;

typedef struct line {
	int16_t to;    // c, other knot's SID
	uint8_t to_line; // c, index of other knot's line
	uint8_t connected; // if rideable, i.e., connected to knot
} line;      //  blunt; straight; right of straight (math. before straight); left of straight;

typedef struct trackKnot {
	sid_t sid;
	int32_t length;  // c, distance between knot and end of Line
	velocity_t vStraight;  // c, v_max on this part of line
	velocity_t vTurn;      // c, v_max when LEFT or RIGHT

	line lines[TKLs];      //  blunt; straight; right of straight (math. before straight); left of straight;
} trackKnot;

typedef struct sections {
	rloc_t Rear, Front, PI, Indication, Permitted, FLOI, Target, ProtectedFront;
} sections;

struct trackDMI {        // this struct cannot be initialized on stack as such, due to the open array element
	tid_t TrainId;
	UINT32 size_tracks;
	sections tracks[];
};

struct trackLocation {
	sid_t sid;
	location_t dist;
	location_t loc;
};

struct protectionLocation {
	tid_t TrainId;
	velocity_t veloity;
	velocity_t acceleration;
	trackLocation Front, Rear, Ant;
	UINT8 sizeTrainType8;
	char trainType8[10];
	char16_t trainType16[8];
};

enum section_name {      // all locations (physical + brake section) of a train
	       Rear=0,
	/* B */Front,
	/* Gy*/PI,
	/* W */Indication,
	/* Y */Permitted,
	/* O */FLOI,
	/* R */Target,             // emptiness beyond
	/*LBl*/ProtectedFront,     // shall be overlayed by other colors, as it exists only for information and locking

	sections_len /* length indicator */
};

class Sections {
public:
	sections d;

	Sections() {
		d = {0, 0, 0, 0, 0, 0, 0, 0};
	}
	Sections(const sections *data) {
		if (data) d = *data; else Sections();
	}
	Sections(const rloc_t *data) {
		Sections((const sections *)data);
		/*
		Rear = data[section_name::Rear];
		Front = data[section_name::Front];
		PI = data[section_name::PI];
		Indication = data[section_name::Indication];
		Permitted = data[section_name::Permitted];
		FLOI = data[section_name::FLOI];
		Target = data[section_name::Target];
		ProtectedFront = data[section_name::ProtectedFront];
		*/
	}
	/* Removes all non-material sections and returns if it still (partially) covers this section */
	bool reduce() {
		d.PI = d.Front;
		d.Indication = d.Front;
		d.Permitted = d.Front;
		d.FLOI = d.Front;
		d.Target = d.Front;
		d.ProtectedFront = d.Front;
		return d.Rear != d.Front;
	}
};

class TrackKnot {
public:
	static       velocity_t v_default;
	static const velocity_t v_curve_def = 60;

	static const velocity_t v_turn = 40; // max turned-switch speed in km/h
	static const int32_t straight_switch_length = 113;
	static const int32_t curved_switch_length = (2*straight_switch_length); // factor 1.5 is put on top in eStw for passing room
	static       int32_t model_scale; // = 50000/3000;  // one lap on n1/s2 is ~3000mm but equals ~50000cm ; this is actually close to 1:160 ! But all measures are done in mm, so factor 10

	TrackKnot(int pin_straight, int pin_right, int pin_left, int32_t length, velocity_t v_turn_off);
	static TrackKnot *Connector(int32_t length, velocity_t v);
	static TrackKnot *CurveSL(int pStraight, int pLeft,  velocity_t v);
	static TrackKnot *CurveSR(int pStraight, int pRight, velocity_t v);
	static TrackKnot *SwitchL(int pStraight, int pLeft,  velocity_t v);
	static TrackKnot *SwitchR(int pStraight, int pRight, velocity_t v);
	static void resetId();

	bool link(TrackKnotLine self, TrackKnot *tk2, TrackKnotLine other);
	bool stretch(TrackKnotLine tkl, int32_t dist);
	bool setOrientation(TrackKnotOrientation o);
	bool canSwitch(TrackKnotLine tkl) const { return has(tkl) && ( tkl==TKL_RIGHT ? (pin[TKL_RIGHT]>=0) : ( tkl==TKL_LEFT ? (pin[TKL_LEFT]>=0) : true)); }
	bool canSwitch(unsigned tkl) const { return (tkl<TKLs) && canSwitch((TrackKnotLine)tkl); }
	//bool has(TrackKnotLine tkl) const {	return ( tkl==TKL_RIGHT ? (pin[TKL_RIGHT]>=0) : ( tkl==TKL_LEFT ? (pin[TKL_LEFT]>=0) : true)); }
	bool has(TrackKnotLine tkl) const {	return !!(to[tkl]); }
	inline bool has(unsigned tkl) const { return (tkl<TKLs) && has((TrackKnotLine)tkl); }
	inline TrackKnotOrientation getO() const { return orint; }
	inline sid_t getSid() const { return d.sid; }
	inline bool isCW() const { return orint==TKO_CW; }
	inline int getIndex() const { return abs(d.sid)-1; }
	inline bool isActive() const { return (pin[TKL_STRAIGHT] > -1 || pin[TKL_LEFT] > -1 || pin[TKL_RIGHT] > -1); }
	inline int getLength() const { return d.length; }
	void serialize(QByteArray *ba, bool marshall, bool hton) const ;
	bool sensePin(int pin);
	void resetSections(const tid_t tid = nil);
	void reduceSections(const tid_t tid);
	void importSections(tid_t tid, const sections *s, bool useCommFront);
	void updateGui(tid_t tid) const ;
	bool matchAndTake(ActiveTrack *aT);
	inline tid_t getLock() const { return lockedTo; }

	QPointF toNodePoint(TrackKnotLine tkl);
	void collectTracks(QList<Railtrack*> rtl, TrackKnot *from = NULL);

private:
	static sid_t glob_id;
	TrackKnotOrientation orint;

	trackKnot d;
	TrackKnot * to[TKLs];
	int pin[TKLs];           // c, when signalled, will connect this line and unconnect the others; <0 on const

	QMap<tid_t, Sections> m_sections; // list of trains and their sections
	tid_t lockedTo;  // contains a trainID if locked, otherwise 0; trainID must not be nil
	ActiveTrack *aT;
	QVector<Railtrack *> rT; // this list is empty for an ActiveTrack, and aT is to be used
	bool rTinverse;          // is set when rT has been collected CW
	int vlength;             // virtual length on the trackplan - for scaling of tints
};

class GeoPlan : public QObject {
	Q_OBJECT

	class Train {
	public:
		tid_t          id;     /* an opaque type, (~0) may show an invalid entry. */
		QHostAddress   raddr;
		QElapsedTimer  freshness;
		TRDP_PD_INFO_T pdInfoL; /* tinting locations */
		TRDP_PD_INFO_T pdInfoP; /* position report */
//		int trdpTrackDMIID = nil;     /* need to subscribe on per-train basis, otherwise they overwrite each other */
//		int trdpPRID = nil;

		/*struct protectionLocation protection;*/
		QByteArray     protection;
	};

public:
	static const int trainTimeout_ms = 1000;
	static const int maxTrains = 32; /* up to 32 fits in a packet, for more, a different approach is needed. */

	static const unsigned short defBufSiz = (267*4);  // default bytebuffer size
	GeoPlan( QObject *parent = 0);
	void link(ActiveTrack *aT);
	inline void updatePlan(QList<Railtrack*> rtl) { if (entry) entry->collectTracks(rtl); }

	static void callReadLocs(void *pRefCon, TRDP_APP_SESSION_T appHandle, const TRDP_PD_INFO_T *pMsg, UINT8 *pData, UINT32 dataSize);
	static void callReadPosi(void *pRefCon, TRDP_APP_SESSION_T appHandle, const TRDP_PD_INFO_T *pMsg, UINT8 *pData, UINT32 dataSize);
	static void callReadTime(void *pRefCon, TRDP_APP_SESSION_T appHandle, const TRDP_PD_INFO_T *pMsg, UINT8 *pData, UINT32 dataSize);
	static void cerr(const char *lead, const char *str, int nl);

protected:
	void timerEvent(QTimerEvent *event);
/* callback wont be used, as long as I employ marshalling
 * 	static void SendCallback(
		    void                    *pRefCon,
		    TRDP_APP_SESSION_T      appHandle,
		    const TRDP_PD_INFO_T    *pMsg,
		    UINT8                   *pData,
		    UINT32                  dataSize);
*/
public slots:
	void sensePin(int pin);
	void updatePlan();

signals:
	void isConnected(bool, QString&);

private:
	bool marshall;
	bool netByteOrder;
	int  configuredTrains;
	bool useCommFront;

	TAU_XSession trdp;
	int32_t trdpTrackPlanID; /* this will be either request-style, or MC */
	int32_t trdpAllPosiID;
	int32_t trdpTrackDMIID;
	int32_t trdpPRID;
	int32_t trdpTimeTrialsID;

	TrackKnot *entry;
	QVector<TrackKnot*> m_knots;

	QMap<tid_t, Train> trains; /* index is tid, always positive */

	int timerRefreshId;

	bool active;
	bool dumpArray;

	/* if both TKs can be stretched (i.e. STRAIGHT line and non-switch, only tk1 will be. */
	bool link(TrackKnot *tk1, TrackKnotLine tk1l, TrackKnotLine tk2l, TrackKnot *tk2, int32_t dist);
	TrackKnot *constructKoffer();
	void checkFreshness(void);

	bool parseSections(QByteArray in);
	void updatePosis();
	void readLocs(const TRDP_PD_INFO_T *pMsg, UINT8 *pData, UINT32 dataSize);
	void readPosi(const TRDP_PD_INFO_T *pMsg, UINT8 *pData, UINT32 dataSize);

};

#endif /* GEOPLAN_H_ */
