#ifndef PLAYCONTROLS_H
#define PLAYCONTROLS_H

#include <QGraphicsObject>
#include "button.h"
#include "buttons/pixmapbutton.h"

class PlayControls : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit PlayControls(QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);



private:
    PixmapButton* b_menu;
    Button* b_sos;

signals:
    void showMainMenu();
    void stopAllTrains();

public slots:
    void showSOS(bool on);

};

#endif // PLAYCONTROLS_H
