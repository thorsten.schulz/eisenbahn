#include "railway.h"

#define MINIMUM_WIDTH      1000
#define MINIMUM_HEIGHT      600
#define PLAYCONTROL_XOFFSET 45
#define PLAYCONTROL_YOFFSET 45
#define PLAYCONTROL_HEIGHT  90
#define DEFAULT_RASTER      50

#define SPACE               3
#define CONTROLS_XOFFSET    35
#define CONTROLS_YOFFSET    35
#define CONTROLS_SIZE       70
#define TRACKLIST_SIZE      80
#define TRACKLIST_XOFFSET   40
#define TRACKLIST_YOFFSET   40

Railway::Railway(QWidget* parent)
    : QGraphicsView(parent)
{
	m_baseTitle = "Model Visualizer";

    // SET VIEWPORT OPTIONS
    QSize size = QSize(MINIMUM_WIDTH,MINIMUM_HEIGHT);
    setMinimumSize(size);
    viewport()->setMinimumSize(size);
    setResizeAnchor(QGraphicsView::AnchorViewCenter);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setWindowTitle(m_baseTitle);

    // CREATE SCENE
    m_scene = new Scene(DEFAULT_RASTER);
    setScene(m_scene);
    QRect rect = QRect(QPoint(), size);
    rect.moveCenter(QPoint());
    m_scene->setSceneRect(rect);
    setSceneRect(rect);

    m_trackplan = new TrackPlan();
    m_scene->addItem(m_trackplan);

    connect(m_trackplan, SIGNAL(hideMenus()),      this, SLOT(hideMenus())     );
    connect(m_trackplan, SIGNAL(showMainMenu()),  this, SLOT(showMainMenu()) );
    connect(m_trackplan, SIGNAL(showTrackLoad()), this, SLOT(showTrackLoad()));
    connect(m_trackplan->getGeo(), SIGNAL(isConnected(bool, QString&)), this, SLOT(showConnectionStatus(bool, QString&)));

    m_railtracklist = new RailtrackList();
    m_scene->addItem(m_railtracklist);

    connect(m_trackplan,   SIGNAL(updateRailtrackList(QList<Railtrack*>)),
            m_railtracklist, SLOT(updateRailtrackList(QList<Railtrack*>)));
    connect(m_trackplan,   SIGNAL(showDefaultList(bool)),
            m_railtracklist, SLOT(showDefaultList(bool)));
    connect(m_railtracklist, SIGNAL(pressed(Railtrack*)), m_trackplan, SLOT(addTrack(Railtrack*)));

    m_controls = new Controls();
    m_scene->addItem(m_controls);

    connect(m_controls, SIGNAL(showMainMenu()),  this, SLOT(showMainMenu())           );
    connect(m_controls, SIGNAL(showMainMenu()),  m_trackplan, SLOT(saveTrackRetMain()) );
    connect(m_controls, SIGNAL(zoomIn()),        this, SLOT(zoomIn())                  );
    connect(m_controls, SIGNAL(zoomOut()),       this, SLOT(zoomOut())                 );
    connect(m_controls, SIGNAL(rotateTrack()),   m_trackplan, SLOT(rotateTrack())   );
    connect(m_controls, SIGNAL(showTrackLoad()), this, SLOT(showTrackLoad())          );
    connect(m_controls, SIGNAL(showTrackLoad()), m_trackplan, SLOT(saveTrackRetLoad()) );

    m_trackcontrols = new TrackControls();
    m_scene->addItem(m_trackcontrols);

    connect(m_trackcontrols, SIGNAL(rotateLeft()),      m_trackplan, SLOT(rotateLeft())         );
    connect(m_trackcontrols, SIGNAL(rotateRight()),     m_trackplan, SLOT(rotateRight())        );
    connect(m_trackcontrols, SIGNAL(hMirror()),         m_trackplan, SLOT(mirrorHorrizontal())  );
    connect(m_trackcontrols, SIGNAL(vMirror()),         m_trackplan, SLOT(mirrorVertical())     );
    connect(m_trackcontrols, SIGNAL(nextNode()),        m_trackplan, SLOT(nextNode())           );
    connect(m_trackcontrols, SIGNAL(deleteTrack()),     m_trackplan, SLOT(deleteTrack())        );
    connect(m_trackcontrols, SIGNAL(showTrackConfig()), m_trackplan, SLOT(showTrackConfig())    );

    connect(m_trackplan, SIGNAL(setGeom(bool,bool,bool,bool)), m_trackcontrols, SLOT(setGeom(bool,bool,bool,bool)));
    connect(m_trackplan, SIGNAL(setNextNode(bool)),     m_trackcontrols, SLOT(setNextNode(bool))    );
    connect(m_trackplan, SIGNAL(setDelete(bool)),       m_trackcontrols, SLOT(setDelete(bool))      );
    connect(m_trackplan, SIGNAL(setTrackConfig(bool)),  m_trackcontrols, SLOT(setTrackConfig(bool)) );

    m_mainmenu  = new MainMenu();
    m_scene->addItem(m_mainmenu);
    connect(m_mainmenu, SIGNAL(showBoardConfig()), this, SLOT(showBoardConfig()));
    connect(m_mainmenu, SIGNAL(showTrackLoad()),   this, SLOT(showTrackLoad()));
    connect(m_mainmenu, SIGNAL(startPlay()),       this, SLOT(startPlay()));

    m_trackload = new TrackLoad();
    m_scene->addItem(m_trackload);
    connect(m_trackload, SIGNAL(showMainMenu()),     this,        SLOT(showMainMenu()));
    connect(m_trackload, SIGNAL(loadTrack(QString)), m_trackplan, SLOT(loadTrackFromFile(QString)));
    connect(m_trackload, SIGNAL(loadTrack(QString)), this,        SLOT(showTrackPlan()));

    m_boardcontrol = new BoardControl();
    m_scene->addItem(m_boardcontrol);
    m_trackplan->setBoardControl(m_boardcontrol);
    connect(m_boardcontrol, SIGNAL(showMainMenu()),    this,           SLOT(showMainMenu())           );
    connect(m_mainmenu,     SIGNAL(close()),           m_boardcontrol, SLOT(disconnectBoards()));
    connect(m_mainmenu,     SIGNAL(close()),           this,           SLOT(close()));

/*    m_playcontrols = new PlayControls();
    m_scene->addItem(m_playcontrols);
    connect(m_playcontrols, SIGNAL(showMainMenu()),  this,           SLOT(showMainMenu()));
*/
    m_infopanel = new InfoPanel(QRect(-250,0, 500, 20));
    m_scene->addItem(m_infopanel);

    connect(m_trackload, SIGNAL(sendMsg(QString)),   m_infopanel, SLOT(printMsg(QString))         );

    m_movetrack = false;

    if(QFile::exists(QString(TRACK_DIR)+"recent.xml")) {
        m_trackplan->loadTrackFromFile(QString(TRACK_DIR)+"recent.xml");
    }

    showMainMenu();
}

// zoom
void Railway::zoomIn()
{ scaleView(5); }

void Railway::zoomOut()
{ scaleView(-5); }

// definiert die zoom-stufen
#define MINIMUM_RASTER      10
#define MAXIMUM_RASTER      60

void Railway::scaleView(int curScale) {
    qreal raster = m_scene->raster();
    if(raster+curScale > MAXIMUM_RASTER)
        curScale = MAXIMUM_RASTER-raster;
    if(raster+curScale < MINIMUM_RASTER)
        curScale = MINIMUM_RASTER-raster;

    m_scene->setRaster(raster+curScale);
    m_trackplan->setScale((raster+curScale)/DEFAULT_RASTER);
    m_trackplan->setPos(m_trackplan->pos()*(raster+curScale)/raster);
}
// ende zoom

void Railway::resizeEvent(QResizeEvent* event) {
    QGraphicsView::resizeEvent(event);

    QRect rect = QRect(QPoint(), event->size());
    rect.moveCenter(QPoint());
    setSceneRect(rect);

    m_scene->setSceneRect(rect);
    m_railtracklist->setPos(mapToScene(TRACKLIST_XOFFSET+SPACE, rect.height()-TRACKLIST_YOFFSET-SPACE));
    m_controls->setPos(mapToScene(CONTROLS_XOFFSET+SPACE, CONTROLS_YOFFSET+SPACE));
    m_trackcontrols->setPos(mapToScene(rect.width()-CONTROLS_XOFFSET-SPACE, CONTROLS_YOFFSET+SPACE));
    m_infopanel->setPos(mapToScene(rect.width()/2,0));
//    m_playcontrols->setPos(mapToScene(PLAYCONTROL_XOFFSET+SPACE, PLAYCONTROL_YOFFSET+SPACE));

	if ( m_trackplan->mode() != TrackPlan::PlayMode )
		return;

	m_trackplan->setPos(0,0);
	qreal wscale = (rect.width())/m_trackplan->childrenBoundingRect().width();
        qreal hscale = rect.height()/m_trackplan->childrenBoundingRect().height();
        qreal maxscale = qMin(wscale, hscale);
        m_trackplan->setScale(maxscale);
}

void Railway::wheelEvent(QWheelEvent* event)
{
	if ( m_trackplan->mode() != TrackPlan::BuildMode )
		return;
        if ( event->orientation() != Qt::Vertical )
		return;
	int zoomradempfindlichkeit = 120;
	int dist = event->delta() / zoomradempfindlichkeit;
	if ( dist )
		scaleView(dist);
}

void Railway::mouseDoubleClickEvent(QMouseEvent* event)
{ event->ignore(); }

// das ist in trackplan.cpp drin!!! ???
void Railway::mousePressEvent(QMouseEvent* event)
{
	QGraphicsView::mousePressEvent(event);
	if ( event->isAccepted() )
		return;
	if ( m_trackplan->mode() != TrackPlan::BuildMode )
		return;
	m_movetrack = true;
	m_trackmoved = false;
	m_oldpos = event->pos();
}

void Railway::mouseMoveEvent(QMouseEvent* event)
{
	QGraphicsView::mouseMoveEvent( event );
	if ( m_trackplan->mode() == TrackPlan::BuildMode )
		return;
        if ( !m_movetrack )
		return;
	QPointF curPos = event->pos();
	QPoint dist((curPos.x()-m_oldpos.x())/m_scene->raster(), (curPos.y()-m_oldpos.y())/m_scene->raster());
	if(dist == QPoint(0,0))
		return;
	m_trackplan->setPos(m_trackplan->pos()+dist*m_scene->raster());
	m_oldpos += dist*m_scene->raster();
	m_trackmoved = true;
}

void Railway::mouseReleaseEvent(QMouseEvent* event) {
    QGraphicsView::mouseReleaseEvent(event);
    if(m_trackplan->mode() == TrackPlan::BuildMode) {
        if(m_movetrack){
            m_movetrack = false;
            if(!m_trackmoved)
                m_trackplan->selectionChanged(0);
            m_trackmoved = false;
        }
    }
}

void Railway::hideAll()
{
    m_scene->hideGrid();
    m_trackplan->hide();
    m_railtracklist->hide();
    m_controls->hide();
    m_trackcontrols->hide();
    m_mainmenu->hide();
    m_trackload->hide();
    m_boardcontrol->hide();
    m_infopanel->hide();
//    m_playcontrols->hide();
}

void Railway::showBoardConfig()
{
	hideAll();
	m_boardcontrol->show();
}

void Railway::showTrackLoad()
{
	hideAll();
	m_trackload->show();
}

void Railway::showMainMenu()
{
	hideAll();
	m_mainmenu->show();
}

void Railway::showTrackPlan()
{
	hideAll();
	m_scene->showGrid();
	m_trackplan->show();
	m_trackplan->setMode(TrackPlan::BuildMode);
	m_railtracklist->show();
	m_controls->show();
	m_trackcontrols->show();
}

void Railway::hideMenus()
{
	hideAll();
	m_trackplan->show();
}

void Railway::startPlay()
{
	hideAll();
	m_trackplan->show();
	m_trackplan->centerToView();
//	m_playcontrols->show();

        qreal wscale = (width())/m_trackplan->childrenBoundingRect().width();
        qreal hscale = height()/m_trackplan->childrenBoundingRect().height();
        qreal maxscale = qMin(wscale, hscale);
        m_trackplan->setScale(maxscale);

	m_trackplan->setMode(TrackPlan::PlayMode);
	m_trackplan->initActiveTracks();
}


void Railway::showConnectionStatus(bool connected, QString& msg) {
	qDebug() << "Status: " << msg;
	setWindowTitle(m_baseTitle + (connected?" [ OK: " + msg + " ]":" [ disconnected ]"));
}
