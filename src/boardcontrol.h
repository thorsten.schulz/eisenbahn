#ifndef BOARDCONTROL_H
#define BOARDCONTROL_H

#include <QGraphicsObject>
#include <QGraphicsPixmapItem>
#include <QPainter>
#include <QList>
#include <QDir>
#include <QFile>
#include <QXmlStreamReader>
#include "button.h"
#include "buttons/pixmapbutton.h"
#include "buttons/textbutton.h"
#include "board.h"

#include "kernelwrapper.h"

#include "config.h"

class BoardControl : public QGraphicsObject
{
    Q_OBJECT
public:
    BoardControl(QGraphicsItem* parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);

    QList<Board*> boards() const;

public slots:
    void scanForBoards();
    void probeBoard(Board* board);
    inline void probeAll() { probeBoard(0); }
    void disconnectBoards();
    void changeActivationTime(int pin, int time);
    void activatePin(int pin);

signals:
    void showMainMenu();

private:
    QRectF m_bounds;
    int m_scroll;
    QString m_title;
    QString m_dir;
    QList<Board*> m_boards;
    QMultiMap<QString, char *> m_mediaintfc;
    KernelWrapper m_kernel;

    QList<TextButton*> m_list;
    Button* b_refresh;
    Button* b_scrollup;
    Button* b_scrolldown;

private slots:
    void scrollUp() { m_scroll -=1; setList(); }
    void scrollDown() { m_scroll +=1; setList(); }
    void setList();

};

#endif // BOARDCONTROL_H
