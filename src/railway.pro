#-------------------------------------------------
#
# Project created by QtCreator 2011-09-22T13:11:51
#
# modified by hinki, later by thorsten
#-------------------------------------------------

QT       += core gui svg network

TARGET = railway
TEMPLATE = app
CONFIG += static thread
#CONFIG += debug

#the TRDP headers expect their peer headers to be globally available
INCLUDEPATH = /usr/include/trdp
#libtau will also pull in libtrdp
LIBS += -lrt -ltau

QMAKE_CFLAGS    *= -D_GNU_SOURCE
QMAKE_CXXFLAGS  *= -D_GNU_SOURCE

SOURCES += main.cpp\
    railway.cpp \
    scene.cpp \
    kernel_extension/media_usb.c \
    kernel_extension/media_table.c \
    kernel_extension/crc.c \
    rail_kernel/tools.c \
    rail_kernel/rail-tools.c \
    rail_kernel/msg-tools.c \
    rail_kernel/ipc.c \
    rail_kernel/global.c \
    railtrack.cpp \
    trackplan.cpp \
    tracknode.cpp \
    mainmenu.cpp \
    button.cpp \
    trackload.cpp \
    board.cpp \
    railtracklist.cpp \
    transform.cpp \
    railtracks/switch3.cpp \
    railtracks/straightswitch.cpp \
    railtracks/shortstraight.cpp \
    railtracks/shortcurved.cpp \
    railtracks/shortangled.cpp \
    railtracks/longstraight.cpp \
    railtracks/longcurved.cpp \
    railtracks/halfcurved.cpp \
    railtracks/decouple.cpp \
    railtracks/curvedswitch.cpp \
    buttons/trackbutton.cpp \
    buttons/textbutton.cpp \
    buttons/pixmapbutton.cpp \
    trackcontrols.cpp \
    infopanel.cpp \
    railtracks/activetrack.cpp \
    trackconfig.cpp \
    controls.cpp \
    playcontrols.cpp \
    NetworkPacket.cpp \
    train.cpp \
    boardcontrol.cpp \
    kernelwrapper.cpp \
    geoplan.cpp

HEADERS  += railway.h \
    scene.h \
    kernel_extension/media_usb.h \
    kernel_extension/media_table.h \
    kernel_extension/crc.h \
    rail_kernel/tools.h \
    rail_kernel/rail-tools.h \
    rail_kernel/protocol.h \
    rail_kernel/msg-tools.h \
    rail_kernel/media.h \
    rail_kernel/ipc.h \
    rail_kernel/global.h \
    railtrack.h \
    trackplan.h \
    tracknode.h \
    contype.h \
    mainmenu.h \
    button.h \
    config.h \
    trackload.h \
    board.h \
    railtracklist.h \
    transform.h \
    railtracks/switch3.h \
    railtracks/straightswitch.h \
    railtracks/shortstraight.h \
    railtracks/shortcurved.h \
    railtracks/shortangled.h \
    railtracks/longstraight.h \
    railtracks/longcurved.h \
    railtracks/halfcurved.h \
    railtracks/decouple.h \
    railtracks/curvedswitch.h \
    buttons/trackbutton.h \
    buttons/textbutton.h \
    buttons/pixmapbutton.h \
    controls.h \
    trackcontrols.h \
    infopanel.h \
    railtracks/activetrack.h \
    trackconfig.h \
    playcontrols.h \
    NetworkPacket.h \
    global_includes.h \
    train.h \
    boardcontrol.h \
    kernelwrapper.h \
    geoplan.h \
    /usr/include/trdp/tau_xsession.hpp
#don't know, if the last line is really necessary and done right(tm)

