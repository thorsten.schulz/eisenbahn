#ifndef RAILTRACKLIST_H
#define RAILTRACKLIST_H

#include <QGraphicsObject>
#include "buttons/trackbutton.h"

#include <QParallelAnimationGroup>
#include <QSequentialAnimationGroup>
#include <QPropertyAnimation>

class RailtrackList : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit RailtrackList(QGraphicsItem *parent = 0);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

signals:
    void pressed(Railtrack*);

public slots:
    void updateRailtrackList(QList<Railtrack*> newlist);
    void showDefaultList(bool enabled);
private slots:
    void clearList();

private:
    QList<TrackButton*> m_buttons;
    QList<TrackButton*> m_newbuttons;
	void addButton( Railtrack *track, int i );
    void fadeOutList();
    void fadeInList();
};

#endif // RAILTRACKLIST_H
