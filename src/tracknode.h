#ifndef TRACKNODE_H
#define TRACKNODE_H

#include <QGraphicsEllipseItem>
#include <QPainter>
#include <QPointF>

#include <QDebug>
#include "contype.h"
#include "config.h"

// warum ellipse?
class TrackNode : public QGraphicsEllipseItem
{
public:

    TrackNode(int type, const QPointF& pos = QPointF(0,0), QGraphicsItem* parent = 0);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QPainterPath shape() const;
    bool contains(const QPointF& point ) const;

    int nodeType() const { return m_nodetype; }
    void setNodeType(int type) { m_nodetype = type; }

    QList<ConType*> connType() const { return m_conn; }
    static QList<ConType*> connFromType(int type);

private:
    int m_nodetype;
    QList<ConType*> m_conn;

};

#endif // TRACKNODE_H
