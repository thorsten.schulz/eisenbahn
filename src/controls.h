#ifndef CONTROLS_H
#define CONTROLS_H

#include <QGraphicsObject>
#include "button.h"
#include "buttons/pixmapbutton.h"

class Controls : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit Controls(QGraphicsItem *parent = 0);
    QRectF boundingRect() const { return QRectF(); }
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

signals:
    void showMainMenu();
    void zoomIn();
    void zoomOut();
    void rotateTrack();
    void showTrackLoad();
    //void saveTrack();

public slots:

private:
    PixmapButton* b_menu;
    Button* b_zoomin;
    Button* b_zoomout;
    Button* b_loadtrack;
    Button* b_rotatetrack;
    //Button* b_savetrack;
};

#endif // CONTROLS_H
