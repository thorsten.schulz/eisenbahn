#include "kernelwrapper.h"

#include <QDebug>

KernelWrapper::KernelWrapper() {
    m_retry = 3;
    ipc_init();
}

void KernelWrapper::initBoard(QString board, QString media, QString interface) {
    QString ret("Initializing Board: ");
    ret.append(board);
    qDebug() << "INFO: Connect Board:" << board << "over:" << media << "at:" << interface;
    char* err = ipc_addroute(board.toLatin1().data(),media.toLatin1().data(),interface.toLatin1().data());

    if (err){
        ret.append("\nError: ").append(err);
        qDebug() << "       ERROR:" << err;
    } else
        qDebug() << "       INFO: Success";
    emit sendMsg(ret);
    emit getReply();
}

void KernelWrapper::stopBoard(QString board){
    QString ret("Stopping Board: ");
    ret.append(board);

    qDebug() << "INFO: Disconnect Board:" << board;

    char* err = ipc_delroute(board.toLatin1().data());
    if (err){
        ret.append("\nError: ").append(err);
        qDebug() << "       ERROR:" << err;
    } else
        qDebug() << "       INFO: Success";
    emit sendMsg(ret);
    emit getReply();
}

void KernelWrapper::setSwitch(QString board, int port) {
    QString ret;

    if(m_retry) {
        qDebug() << "INFO: Set Port:" << port << "at Board:" << board;
        if (port >= 0) {
            m_retry--;
            ret.append("Set Switch on Port: ").append(QString::number(port));

            RMSG msg, rsp;
            msg.rm_data.rm_value = port;
            msg.rm_cmd = RC_PSET;
            char* err = rt_execcmd( &msg, board.toLatin1().data(), &rsp );

            if(err) {
                ret.append("\nError: ").append(err);
                qDebug() << "       ERROR:" << err;
                qDebug() << "       INFO: Retry";
                setSwitch(board, port);
            } else {
                qDebug() << "       INFO: Success";
                m_retry = 3;
            }
        } else {
            ret.append("Error Setting: - Port out of Range (");
            ret.append(QString::number(port)).append(")");
            qDebug() << "       ERROR: Port out of range";
        }
        emit sendMsg(ret);
        emit getReply();
    } else
        m_retry = 3;

}

void KernelWrapper::changeTime(QString board, int port, int new_time) {
    QString ret;

    if(m_retry) {
        qDebug() << "INFO: Change activation time for Port:" << port << "to:" << new_time << "at Board:" << board;
        if (port >= 0){
            m_retry--;
            ret.append("Changing Activationtime of Port: ").append(QString::number(port));
            ret.append(" to").append(QString::number(new_time));

            RPVAL port_value;
            port_value.rpv_port = port;
            port_value.rpv_value = new_time;

            RMSG msg, rsp;
            msg.rm_data.rm_pval = port_value;
            msg.rm_cmd = RC_MONOTIME;
            char* err = rt_execcmd( &msg, board.toLatin1().data(), &rsp );

            if(err){
                ret.append("\nError: ").append(err);
                qDebug() << "       ERROR:" << err;
                qDebug() << "       INFO: Retry";
                changeTime(board, port, new_time);
            } else {
                qDebug() << "       INFO: Success";
                m_retry = 3;
            }
        } else {
            ret.append("Error Setting: - Port out of Range (");
            ret.append(QString::number(port)).append(")");
            qDebug() << "       ERROR: Port out of range";
        }
        emit sendMsg(ret);
        emit getReply();
    } else
        m_retry = 3;
}


//Multiswitch wird realisert über num of ports / acht chars. Bit Position 1 oder 0 gibt an ob der Switch gesetzt wird oder nicht.
void KernelWrapper::setMultiSwitch(QString board, QList<int> ports) {
    QString ret;

    int pc = ports.count();
    if (pc) {
        RBYTE temp;
        temp.rb_size = ((pc/8) +2); //we need 6 bytes for 48ports
        temp.rb_port = 0; //value gets ignored anyways

        RMSG msg, rsp;
            msg.rm_data.rm_bytes = temp;
            msg.rm_cmd = RC_MPSET;

        memset(msg.rm_string, 0x0, ((pc/8) + 2) );

        for(int i=0; i<pc; ++i) {
            if ( i >= 0 ) {
                msg.rm_string[ i / 8 ] |= (1 << (i % 8));
                ret.append("Activating Port; ");
                ret.append(QString::number(i)).append("\n");
            } else {
                ret.append("Error Setting: - Port out of Range (");
                ret.append(QString::number(i)).append(")");
            }
        }

        char* err = rt_execcmd( &msg, board.toLatin1().data(), &rsp );

        if(err){
            ret.append("\nError: ").append(err);
        }
    } else
        ret = QString("Error: No Value specified in setMultiSwitch");

    emit sendMsg(ret);
    emit getReply();
}

bool KernelWrapper::probeBoard(QString board) {
    QString ret;
    RMSG msg, rsp;
    msg.rm_cmd = RC_GETNAME;
    qDebug() << "INFO: Search for Board:" << board;
    char* err = rt_execcmd( &msg, board.toLatin1().data(), &rsp );
    if(err) {
        ret.append("Info: No Board found.");
        emit sendMsg(ret);
        qDebug() << "       ERROR:" << err;
    }
    if(QString::compare(QString(rsp.rm_string),board)==0) {
        qDebug() << "       INFO: Found" << board;
        return true;
    } else {
        qDebug() << "       INFO:" << board << "not found";
        stopBoard(board);
    }
    return false;
}


QMultiMap<QString, char *> KernelWrapper::getInterfaceList() {
    QMultiMap<QString, char *> mediaintfc;
    int mc = 0;
    MED_PTR mp;
    qDebug() << "INFO: Search for available interfaces";
    while( (mp = media_getentry(mc)) ) {
        int cnt = 0;
        char* intfc[MAX_INTFC];
        media_getinterfaces(mc++, &cnt, intfc);
        for(int i=0; i<cnt; ++i) {
            mediaintfc.insert(QString(mp->med_name), intfc[i]);
            qDebug() << "       INFO: Found" << intfc[i] << "@" << QString(mp->med_name);
        }
    }

    return mediaintfc;
}

