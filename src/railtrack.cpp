#include "railtrack.h"
#include "trackplan.h"

#include "railtracks/shortstraight.h"
#include "railtracks/longstraight.h"
#include "railtracks/longcurved.h"
#include "railtracks/shortcurved.h"
#include "railtracks/halfcurved.h"
#include "railtracks/straightswitch.h"
#include "railtracks/curvedswitch.h"
#include "railtracks/shortangled.h"
#include "railtracks/decouple.h"
#include "railtracks/switch3.h"

#include <QApplication>

// warum?
Railtrack* Railtrack::fromType(int trackType, QGraphicsItem* parent )
{
    switch(trackType) {
        case Rt_ShortStraight  : return new ShortStraight(parent);  break;
        case Rt_LongStraight   : return new LongStraight(parent);   break;
        case Rt_LongCurved     : return new LongCurved(parent);     break;
        case Rt_ShortCurved    : return new ShortCurved(parent);    break;
        case Rt_HalfCurved     : return new HalfCurved(parent);     break;
        case Rt_StraightSwitch : return new StraightSwitch(parent); break;
        case Rt_CurvedSwitch   : return new CurvedSwitch(parent);   break;
        case Rt_Decouple       : return new Decouple(parent);       break;
        case Rt_ShortAngled    : return new ShortAngled(parent);    break;
        case Rt_Switch3		: return new Switch3(parent);		break;
    }
    return 0;
}

QList<TrackNode*> Railtrack::nodesFromType(int trackType) {
    switch(trackType) {
        case Rt_ShortStraight  : return ShortStraight::nodesFromType();  break;
        case Rt_LongStraight   : return LongStraight::nodesFromType();   break;
        case Rt_LongCurved     : return LongCurved::nodesFromType();     break;
        case Rt_ShortCurved    : return ShortCurved::nodesFromType();    break;
        case Rt_HalfCurved     : return HalfCurved::nodesFromType();     break;
        case Rt_StraightSwitch : return StraightSwitch::nodesFromType(); break;
        case Rt_CurvedSwitch   : return CurvedSwitch::nodesFromType();   break;
        case Rt_Decouple       : return Decouple::nodesFromType();       break;
        case Rt_ShortAngled    : return ShortAngled::nodesFromType();    break;
	case Rt_Switch3		: return Switch3::nodesFromType();		break;
    }
    return QList<TrackNode*>();
}

Railtrack::Railtrack( QSvgRenderer* shared, QGraphicsItem* parent )
	: QGraphicsSvgItem(parent)
{
	m_trackmoved=false; m_movetrack=false;
	setSharedRenderer( shared );
	setElementId( "normal" );

	//GET BOUNDING RECT INFORMATION FROM SVG
	QSize size = renderer()->defaultSize();
	m_bounds = QRectF(QPointF(), size);
	m_bounds.moveCenter(QPointF());

	//REPOSITION TRACKS WITH ODD RASTER SIZE
	/* wenn die Gleisstücke eine ungerade Anzahl von
	 * Rasterquadraten belegen, passt die Platzierung nicht.
	 * TODO: Das muss an einer anderen Stelle korrigiert werden.
         * Aber wo?
	 */
	if( m_bounds.toRect().width() % 100 )
	{
		m_bounds.translate(25,0);
		m_origin.setX(25.0);
	}

	if( m_bounds.toRect().height() % 100 )
	{
		m_bounds.translate(0,25);
		m_origin.setY(25.0);
	}

	//GET SHAPE INFORMATION FROM PIXMAP RENDERER
	QPixmap pixmap(size);
	pixmap.fill(Qt::transparent); ///IMPORTANT TO SUPPRESS RENDER ARTEFACTS!!!!!!
	QPainter painter(&pixmap);
	renderer()->render(&painter, "shape");
	QGraphicsPixmapItem item(pixmap);
	m_shape = item.shape().translated(m_bounds.topLeft());
	m_highlighted = false;
	m_locked = -1;
	m_length = 0;
    m_current = 0;
    m_bluntFoundFirst = true;
}

QList<TrackNode*> Railtrack::nodes() const
{ return m_nodes; }

TrackNode* Railtrack::nextNode()
{
    m_nodes << m_nodes.takeFirst();
    return m_nodes.first();
}

void Railtrack::rotate( qreal angle )
{
    QTransform rot;
    rot.rotate(angle);

    QPointF p1 = transform().map(m_origin);
    setTransform(transform()*rot);

    QPointF p2 = p1-transform().map(m_origin);
    moveBy(p2.x(), p2.y());
}

void Railtrack::mirror(int hscale, int vscale) {
    QTransform scale = QTransform::fromScale(hscale, vscale);

    QPointF p1 = transform().map(m_origin);
    setTransform(transform()*scale);

    QPointF p2 = p1-transform().map(m_origin);
    moveBy(p2.x(), p2.y());
}

int Railtrack::direction() const {
    QTransform trans = transform();
    return toDirection(trans);
}

void Railtrack::setDirection(QTransform direction, bool comb)
{ setTransform( comb ? transform()*direction : direction ); }

QRectF Railtrack::boundingRect( ) const
{ return m_bounds; }

QPainterPath Railtrack::shape( ) const
{ return m_shape; }

void Railtrack::setLocked(int locked) {
    if (m_locked != locked) {
    	m_locked = locked;
    	update();
    }
}

bool Railtrack::highlighted() const
{ return m_highlighted; }

void Railtrack::setHighlighted(bool highlighted)
{
    if (m_highlighted != highlighted) {
    	m_highlighted = highlighted;
    	update();
    }
}

void Railtrack::setTint(int tid, std::vector<PartialTrackTint> updatedSections) {
	if (m_tintSections[tid] != updatedSections) {
		m_tintSections[tid] = updatedSections;
		update();
	}
}

void Railtrack::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    renderer()->render(painter, elementId(),m_bounds);
    TrackNode *tnS = nextNode();
    TrackNode *tnB = nextNode();
    while (tnB->pos().x() >= tnS->pos().x()) {
    	tnS = tnB;
    	tnB = nextNode();
    }
    if (m_current) tnS = nextNode(); /* if set to turn, aim to the third node */

//	int tints = m_tintSections.size();
    int width = 16;
	QFont writer("Helvetica", 32, QFont::Normal);
	painter->setFont(writer);
	//writer.set
    QLineF lfo = m_bluntFoundFirst ? QLineF(tnB->pos(), tnS->pos()) : QLineF(tnS->pos(), tnB->pos());
//    painter->setCompositionMode(QPainter::CompositionMode_SourceAtop);
    for (std::map<int, std::vector<PartialTrackTint>>::const_iterator v_ptt = m_tintSections.cbegin(); v_ptt != m_tintSections.cend(); ++v_ptt) {
    	for (std::vector<PartialTrackTint>::const_iterator ptt = v_ptt->second.begin(); ptt != v_ptt->second.end(); ++ptt) {
//		for (uint8_t i=0; i < ptt->size(); i++){
			char n = ptt->m_id;
			QPen pen(ptt->m_color);
			if (n=='G') {
				pen.setCapStyle(Qt::RoundCap);
				pen.setStyle(Qt::DotLine);
				pen.setWidth(8);
			} else {
				pen.setWidth(width);
			}
			painter->setPen(pen);

			QLineF lf1 = lfo;
			QLineF lf2 = lfo;
	// if (dbg) qDebug() << "Painting" << i << m_tintSections[i].m_id << "start" << lfo.length()*m_tintSections[i].m_start << "end" << lfo.length()*m_tintSections[i].m_end;
			lf1.setLength(lfo.length()*ptt->m_start);
			lf2.setLength(lfo.length()*ptt->m_end);
			painter->drawLine(lf1.p2(), lf2.p2());

			if (n >= '0' && n <= '9'  && (ptt->m_start>0) && (ptt->m_start < 1)) {
				painter->setPen(QPen(Qt::cyan));
				painter->drawText(lf1.p2(), QString(n));
			}
		}
    	width -= width/2;
        lfo.translate( 0, 6);
    }
    if (dbg || redFrame) {
    	painter->setPen(QPen(Qt::red));
    	painter->drawRect(m_bounds);
    }
    if(m_highlighted) {
        painter->setCompositionMode(QPainter::CompositionMode_SourceIn);
        painter->fillRect(m_bounds, Qt::darkRed);
	}
    if (isLocked()) {
        painter->setCompositionMode(QPainter::CompositionMode_DestinationOver);
    	painter->fillRect(m_bounds, Qt::lightGray);
    }
}

Railtrack *Railtrack::closestRailtrack(const QList<Railtrack*> rtl, QPointF &nodeP) {
	QPointF abs = mapToParent(nodeP);
	foreach(Railtrack *rt, rtl) if (rt != this && rt->m_nodes.size()==2) {
		QPointF n0 = rt->m_nodes[0]->pos();
		QPointF n1 = rt->m_nodes[1]->pos();

		if ((rt->mapToParent(n0)-abs).manhattanLength() <= 12) {
			rt->m_bluntFoundFirst = n0.x() < n1.x();
			nodeP = n1;
			return rt;
		}
		if ((rt->mapToParent(n1)-abs).manhattanLength() <= 12) {
			rt->m_bluntFoundFirst = n1.x() < n0.x();
			nodeP = n0;
			return rt;
		}
	}
	return NULL;
}

double Railtrack::find(const QList<Railtrack*> rtl, QVector<Railtrack *> &crumbs, QPointF nodeP, Railtrack *upTo) {
	Railtrack *rt = NULL;
	double length = 0;
	rt = closestRailtrack(rtl, nodeP); // returns the next point if found
	if (rt && rt != upTo) {
		crumbs << rt;
		length = rt->getLength() + rt->find(rtl, crumbs, nodeP, upTo);
	}
	return length;
}
/*
 * m_movetrack soll die Reihenfolge press, move, release bemerken
 */
void Railtrack::mousePressEvent( QGraphicsSceneMouseEvent *event )
{
	if( static_cast<TrackPlan*>(parentItem())->mode() != TrackPlan::BuildMode )
		return;
	m_movetrack = true;
	m_trackmoved = false;
	m_oldpos = event->scenePos().toPoint();
	static_cast<TrackPlan*>(parentItem())->selectionChanged(this);
}

void Railtrack::mouseMoveEvent( QGraphicsSceneMouseEvent *event )
{
	if( static_cast<TrackPlan*>(parentItem())->mode() != TrackPlan::BuildMode )
		return;
	if( !m_movetrack )
		return;

	const int raster = 50;
	QPoint curpos = event->scenePos().toPoint();
	QPoint dist((curpos.x()-m_oldpos.x())/static_cast<Scene*>(scene())->raster(), (curpos.y()-m_oldpos.y())/static_cast<Scene*>(scene())->raster());
	if( dist == QPoint(0,0) )
		return;
	setPos( pos()+dist*raster );

	QList<QGraphicsItem*> list = collidingItems();
	for ( int i = 0; i < list.count(); ++i )
		if( list[i]->parentItem() == parentItem() )
		{
			// revert
			setPos( pos()-dist*raster );
			return;
		}
	// m_oldpos == curpos ???
	m_oldpos += dist*static_cast<Scene*>(scene())->raster();
	m_trackmoved = true;
}

void Railtrack::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
	Q_UNUSED(event);
	if( static_cast<TrackPlan*>(parentItem())->mode() != TrackPlan::BuildMode ) return;
    if( !m_movetrack ) return;
	m_movetrack = false;
    if ( !m_trackmoved ) return;
	// warum?
	static_cast<TrackPlan*>(parentItem())->findOpenNode();
	m_trackmoved = false;
}
