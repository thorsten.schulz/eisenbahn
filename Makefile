bld  := bin
scale = 160
CONF = ../nanostw/config

# this now only changes between debug and relase config
ifeq ($(shell uname -m),armv6l)
	PROJ_FILE := railway_armhf.pro
else
	PROJ_FILE := railway.pro
endif

.PHONY: run network clean distclean all

all: $(bld)/Makefile
	$(MAKE) -C $(bld) $@

# TODO this folder changery should be addressed in the app itself
# you can now set the scale of the predefined model by appending the param
run: all
	$(bld)/railway $(CONF)/gg.xml scale=$(scale)

$(bld):
	mkdir -p $@

$(bld)/Makefile: src/$(PROJ_FILE) | $(bld)
	-$(MAKE) -C $(bld) clean
	qmake -qt=5 -o "$@" "$<"

clean:
	-$(MAKE) -C $(bld) $@

distclean: clean
	-rm -rf $(bld)

# This target only works as an orientation, it does not fit on any use-case anymore.
# Prefer the one given in the other source dir.
network:
	sudo echo
	sudo ip link add wg0 type wireguard
	sudo ip addr add 172.22.0.10/24 dev eth0
	sudo wg setconf wg0 $(CONF)/wg-gg.conf
	sudo ip addr add 172.21.0.10/24 dev wg0
	sudo ip link set wg0 up
